<%@ page isErrorPage="true" %>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.ico">
        <link href="css/bootstrap.css" rel="stylesheet">
        <title>Erro Page </title>

    </head>
    <body>
        <div id="content" class="container">
            <div class="main-container">
                <h1>Error Page</h1>
                <pre>
                    <% exception.printStackTrace(response.getWriter());%>
                </pre>
            </div>

        </div>
    </body>
</html>