<%@page import="org.locusdoc.utilities.ApplicationException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <body>
        <%
            // Create Respotiroy objects first
            RoleRepository roleRepo = new RoleRepository();
            FormRepository formRepo = new FormRepository();
            ActionRepository actionRepo = new ActionRepository();
            PermissionRepository permRepo = new PermissionRepository();
        %>
        <div id="result">
            <h3>Assign permission</h3>
        </div>
        <pre><br>

            <%
                // Define for which Role
                RoleBean admin = roleRepo.getByName("SuperAdmin");
                out.println("\nThe role is: " + admin.getName());
                // Define which Action, e.g VIEW, UPDATE tc.
                ActionBean action = actionRepo.getByAction("VIEW");
                out.println("The action is: " + action.getAction());

                // Then get all Main forms, e.g Documents, Definitions etc.
                ArrayList<FormBean> forms = formRepo.getAll();

                out.println("\n\nForms are: \n");
                // Show which forms we are working with
                for (FormBean form : forms) {
                    out.println(form.getTitle());
                }
                // Now try to insert permission with action for above role
//                for (FormBean form : forms) {
//                    PermissionBean perm = new PermissionBean(form.getId(), admin.getId(), action.getId());
//                    out.println("Try to insert: " + perm);
//                    try {
//                        permRepo.insert(perm);
//                    } catch (ApplicationException ex) {
//                        ex.printStackTrace();
//                        out.println("ERORR: " + ex.getMessage());
//                    }
//                }

            %>
        </pre>
    </body>
</body>
</html>
