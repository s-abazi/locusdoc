<%@page import="org.locusdoc.businessLayer.StateBean"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.SharingBean"%>
<%@page import="org.locusdoc.businessLayer.FileBean"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>

<%@page import="java.util.ArrayList"%>
<jsp:useBean id="rUsrFolder" class="org.locusdoc.businessLayer.UserRepository" scope="page"/>
<jsp:useBean id="docCatRepo" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page" />
<jsp:useBean id="docFileRepo" class="org.locusdoc.businessLayer.FileRepository" scope="page" />
<jsp:useBean id="docStateRepo" class="org.locusdoc.businessLayer.StateRepository" scope="page" />
<jsp:useBean id="docSharingRepo" class="org.locusdoc.businessLayer.SharingRepository" scope="page" />

<h2>Shared Documents</h2>
<div id="result" style="display: none"></div>

<table class="shared-doc-list table table-hover">
    <thead>
        <tr>
            <th>File Name</th>
            <th>Owner</th>
            <th>Created Date</th>
            <th>Size</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <%
            UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
            ArrayList<SharingBean> sharing = docSharingRepo.getByUser(sessUser.getId());            
        %>
        <% for (SharingBean share : sharing) { %>
        <tr> 
            <% FileBean file = docFileRepo.getById(share.getFile());%>
            <td><span class="glyphicon glyphicon-file"></span> <%=file.getFileName()%></td>
            <td><%=rUsrFolder.getById(file.getOwner()).getUsername() %></td>
            <td><%=file.getCreatedDate()%></td>
            <td><%=file.getFileSize() / 1024%> KByte</td>
            <td>
                <button id="download-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-save"></span></button>
                <button id="view-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></button>
            </td>
        <% } %>    
        </tr>
    </tbody>
</table>
<script type="text/javascript" charset="utf-8">
    //DataTable Javascript
    $(document).ready(function() {
        $('.shared-doc-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });
    
    function downloadDoc(fileId) {
        var object = "file=";
        var url = "${pageContext.request.contextPath}/document/download?" + object + fileId;
        window.location.href = url;
    }
</script>