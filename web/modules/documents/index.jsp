<%@page import="org.locusdoc.businessLayer.FormBean" %>
<%@page import="org.locusdoc.businessLayer.FormRepository" %>
<%@include file="/modules/layout/header.jsp"%>

<jsp:useBean id="repoDoc" class="org.locusdoc.businessLayer.FormRepository" scope="request"/>
<% PermissionHandler doc_perms = (PermissionHandler) session.getAttribute("permissions");%>

<%    // Store GET parameter and check if its valid
    FormBean getForm = null;
    String value = request.getParameter("form");
    Integer formId = (value != null) ? Integer.parseInt(value) : 0;
    if (formId > 0) {
        // Instatiate form
        getForm = repoDoc.getById(formId);
        // If form is not null but user has no permission for it, then set it to null
        if (getForm != null && !(doc_perms.hasPermission(getForm, "VIEW"))) {
            getForm = null;
        }

    }
%>

<script>
    $(document).ready(function() {
        var item = document.getElementById('doc-link');
        $(item).attr("class", "active");
    });
</script>
<div id="content" class="container">
    <c:if test="${sessionScope.sessionUser.changedPass == 0}" >
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Hello ${sessionUser.firstName}, this seems to be your first time use. Please click <a href="${pageContext.request.contextPath}/modules/account/" class="alert-link">here</a> to change your generated password.
        </div>  
    </c:if>
    <div class="main-container">
        <div class="sidebar col-md-3">
            <!-- Dynamic left menu generation -->

            <div class="navbar-header">
                <a class="navbar-second-toggle" data-toggle="collapse" data-target=".navbar-second-collapse">
                    Sidebar Menu <b class="caret"></b>
                </a>
            </div>
            <div class="navbar-second-collapse collapse">

                <ul class="navgoco nav-pills nav-stacked " id="navTabs">
                    <%
                        // Get all Parent forms with position set to 1, for Documents
                        ArrayList<FormBean> parentForms = repoDoc.getByPosition(1, null);
                        for (FormBean parent : parentForms) {
                            if (doc_perms.hasPermission(parent, "VIEW")) {
                    %>
                    <li class="single-navbar" id="<%=parent.getNameId()%>"
                        onclick="loadForm('<%=parent.getPath()%>')">
                        <%=parent.getTitle()%>
                        <%}%>
                        <%}%>
                    </li>
                </ul>
            </div>

        </div>
        <div class="right-container col-md-9">
            <c:if test="${ not empty msg}" >
                <div id="upload-result">
                    <p>${requestScope["msg"]}</p>
                </div>
            </c:if>
            <!-- Dynamic Content for the main content (eg: documents,settings,profile etc) -->
            <% if (getForm == null) {%>
            <jsp:include page="/modules/documents/main.jsp"/>
            <% }%>      
            <!-- End of dynamic content for main content(right-container) -->
        </div> 
    </div>  
</div>
<div id="create-new-folder" style="display:none">
    <label for="newFolder">New Folder:</label>
    <input type="text" name="newFolder" id="newFolder" class="form-control" placeholder="Folder Name">
</div>
<%@include file="/modules/layout/footer.jsp" %>
</div>
<script>
//Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    <c:if test="${ not empty success}" >
    $(document).ready(function() {
        var upload_result = document.getElementById("upload-result");
        if (${requestScope["success"]} == 1) {
            $(upload_result).addClass("alert alert-success");
        } else {
            $(upload_result).addClass("alert alert-danger");
        }
    });
    </c:if>



    function loadForm(path) {
        $(".right-container").load("${pageContext.request.contextPath}/" + path);
    }

    <% if (getForm != null) {%>
    loadForm("<%=getForm.getPath()%>");
    <% }%>

</script>
</body> 
</html>