<%@page import="java.io.IOException"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryRepository"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="java.io.File"%>
<%@page import="org.locusdoc.businessLayer.SettingsBean"%>
<%@page import="org.locusdoc.businessLayer.SettingsRepository"%>
<%@page import="org.locusdoc.businessLayer.AlertBean"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.UserRepository"%>
<%@page import="org.locusdoc.businessLayer.AlertRepository"%>
<%@page import="org.locusdoc.businessLayer.StateRepository"%>
<%@page import="org.locusdoc.businessLayer.StateBean"%>
<%@page import="org.locusdoc.businessLayer.UserStateApproverRepository"%>
<%@page import="org.locusdoc.businessLayer.UserStateApproverBean"%>
<%@page import="org.locusdoc.businessLayer.WorkflowHistoryRepository"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="org.locusdoc.utilities.ApplicationException"%>
<%@page import="org.locusdoc.businessLayer.FileRepository"%>
<%@page import="org.locusdoc.businessLayer.FileBean"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="org.locusdoc.businessLayer.WorkflowHistoryBean"%>
<%@page import="java.util.ArrayList"%>
<%!

    private void generateFileNotifications() {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        //Get All Files, which dont have the status approved i.e 100
        ArrayList<FileBean> files = getNotApprovedFiles();
        for (FileBean file : files) {
            StateBean currentState = getStateOfFile(getFlowForFile(file));
            if (currentState != null) {
                notifyApprovers(getApprovers(currentState, file), file); // If this File has a state that is not approved, continue
            }
            
        }
        
    }
     
    // Fetch all the Files that are not Approved (i.e state = 100) from the DB
    private ArrayList<FileBean> getNotApprovedFiles() {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        FileRepository fileRepo = new FileRepository();
        ArrayList<FileBean> files = null;
        try {
            files = fileRepo.getByStateIsNot(100);
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the Files that are not approved.");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        return files;
        
    }
    
    // Fetch Workflow as List for this file
    private ArrayList<WorkflowHistoryBean> getFlowForFile(FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        WorkflowHistoryRepository flowRepo = new WorkflowHistoryRepository();
        ArrayList<WorkflowHistoryBean> fileFlow = null;
        
        try {
            fileFlow = flowRepo.getByFile(file.getId());
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the workflow of the file (id = "+file.getId()+").");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        
        return fileFlow;
    }
    
    // Get current, not approved, state of this file
    private StateBean getStateOfFile(ArrayList<WorkflowHistoryBean> fileFlow) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        Integer stateId = 0;
        StateBean state = null; // State which we will notify approvers for
        for (WorkflowHistoryBean flow : fileFlow) {
            Integer status = flow.getStatus();
            if (status == 0) {
                stateId = flow.getState();
                break; // We've got the first state that isnt approved, so get out!
            }
        }
        StateRepository stateRepo = new StateRepository();
        try {
            state = stateRepo.getById(stateId);
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the state (id = "+state.getId()+").");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        return state;
    }
    
    // Get approvers who can approve state in the category where this File belongs
    private ArrayList<UserStateApproverBean> getApprovers(StateBean state, FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        Integer categoryId = file.getRequestCategory();
        UserStateApproverRepository approversRepo = new UserStateApproverRepository();
        ArrayList<UserStateApproverBean> approvers = null;
        try {
            approvers = approversRepo.getByStateAndCategory(state.getId(), categoryId);
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the approvers "
                    + "(state_id = "+state.getId()+", category_id = "+categoryId+").");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        return approvers;
    }
    
    // Get owner of given file
    private UserBean getOwnerOf(FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        UserRepository userRepo = new UserRepository();
        UserBean user = null;
        try {
            user = userRepo.getById(file.getOwner());
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the owner of the file (id = "+file.getId()+").");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        return user;
    }
    
    // Generate alerts and send them to approvers
    private void notifyApprovers(ArrayList<UserStateApproverBean> approvers, FileBean file) {
        
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        AlertRepository alertRepo = new AlertRepository();
        String title = "File [" + file.getFileName()+ "] needs action.";
        UserBean owner = getOwnerOf(file);
        String content = "File [" + file.getFileName()+ "] that is owned by [" + owner.getId() + "] "
                    + owner.getFirstName() + " " + owner.getLastName() + ", needs status aprovement.";
        
        // Create Alert for every approver
        for (UserStateApproverBean approver: approvers) {
            ArrayList<AlertBean> existentAlerts = getAlertsFor(approver.getUser());
            AlertBean alert = new AlertBean(title, content, approver.getUser());
            
            if (alertExistIn(alert, existentAlerts)){
                continue; // Since this alert already exsists, don't create it
            }
            try {
                alertRepo.insert(alert);
            } catch (ApplicationException ex) {
                Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error inserting the alert for approver (user_id = "+approver.getUser()+").");
                Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        
    }
    
     private ArrayList<AlertBean> getAlertsFor(Integer userId) {
         Logger.getLogger("File-state-changes").log(Level.INFO, "");
         AlertRepository alertRepo = new AlertRepository();
         ArrayList<AlertBean> alerts = null;
         try {
             alerts = alertRepo.getByUser(userId);
         } catch (ApplicationException ex){
             Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the alerts of the user (user_id = "+userId+").");
             Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
         }
         return alerts;
     }
     
     private boolean alertExistIn(AlertBean alert, ArrayList<AlertBean> alerts) {
         for (AlertBean listAlert:alerts) {
             if (alert.getTitle().trim().equals(listAlert.getTitle().trim())) {
                 return true;
             }
         }
         return false;
     }
     
     public void updateFiles() {
         Logger.getLogger("File-state-changes").log(Level.INFO, "");
        //Get All Files
        ArrayList<FileBean> files = getAllFiles();
        for (FileBean file : files) {
            StateBean currentState = getStateOfFile(getFlowForFile(file));
            if (currentState == null){
                currentState = new StateBean(100, "Approved"); // If this File has a state that is not approved, continue
            }
            file.setState(currentState.getId());
            updateFileDetails(file);
            
        }
     }
     
     // Fetch all the Files
    private ArrayList<FileBean> getAllFiles() {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        FileRepository fileRepo = new FileRepository();
        ArrayList<FileBean> files = null;
        try {
            files = fileRepo.getAll();
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching all files.");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        return files;
        
    }
    
    private void updateFileDetails(FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        FileRepository fileRepo = new FileRepository();
        file.setFileSize(getSizeOfFile(file));
        try {
            fileRepo.update(file);
        } catch (ApplicationException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error updating the file (id = "+file.getId()+".");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
    }
    
    private Integer getSizeOfFile(FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        SettingsRepository settingRepo = new SettingsRepository();
        SettingsBean doc_path = null;
        try {
            doc_path = settingRepo.getByKey("doc_path");
        } catch (ApplicationException ex ) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the doc_path setting.");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        // The documents path where all the FileCategory directories are
        String rootPath = doc_path.getValue();
        String directory = getDirectoryName(file);
        File fileRef = new File(rootPath + File.separator + directory +File.separator + file.getId());
        try {
             Logger.getLogger("File-state-changes").log(Level.INFO, "Reference to file is "+fileRef.getCanonicalPath());
        } catch (IOException ex) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
       
        return (int)fileRef.length();
    }
    
    private String getDirectoryName(FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        String directory = null;
        if (file.getRequestCategory() == 1) {
            directory = "users" + File.separator;
            UserBean owner = getOwnerOf(file);
            directory += owner.getUsername();
        } else {
            FileCategoryBean category = getCategoryOf(file);
            String catName = category.getName().toLowerCase().replaceAll("\\s+", "-");
            directory = "file-categories"+File.separator;
            directory += catName;
        }
        
        return directory;
    }
    
    private FileCategoryBean getCategoryOf(FileBean file) {
        Logger.getLogger("File-state-changes").log(Level.INFO, "");
        FileCategoryRepository catRepo = new FileCategoryRepository();
        FileCategoryBean category = null;
        try {
            category = catRepo.getById(file.getRequestCategory());
        } catch (ApplicationException ex ) {
            Logger.getLogger("File-state-changes").log(Level.WARNING, "There was an error fetching the category for file (category_id = "+file.getRequestCategory()+").");
            Logger.getLogger("File-state-changes").log(Level.WARNING, ex.getMessage(), ex);
        }
        
        return category;
    }
    
    
     
     

    
%>
