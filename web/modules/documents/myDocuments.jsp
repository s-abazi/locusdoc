<%@page import="org.locusdoc.businessLayer.SharingBean"%>
<%@page import="org.locusdoc.businessLayer.StateBean"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.FileBean"%>
<%@page import="org.locusdoc.businessLayer.FileBean"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>

<%@page import="java.util.ArrayList"%>
<jsp:useBean id="rUsrFolder" class="org.locusdoc.businessLayer.UserRepository" scope="page"/>
<jsp:useBean id="docCatRepo" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page" />
<jsp:useBean id="docFileRepo" class="org.locusdoc.businessLayer.FileRepository" scope="page" />
<jsp:useBean id="docStateRepo" class="org.locusdoc.businessLayer.StateRepository" scope="page" />
<jsp:useBean id="docShareRepo" class="org.locusdoc.businessLayer.SharingRepository" scope="page" />
<jsp:useBean id="docFormRepo" class="org.locusdoc.businessLayer.FormRepository" scope="page" />
<%@page import="java.util.ArrayList"%>
<h2>My Documents</h2>
<div id="result" style="display: none"></div>
<button type="button" id="add-new" onclick="uploadPersDocument()" class="btn btn-primary">New Document</button>

<table class="myDocuments-doc-list table table-hover">
    <thead>
        <tr>
            <th>File Name</th>
            <th>Shared</th>
            <th>Created Date</th>
            <th>Size</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

        <%
            UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
            ArrayList<FileBean> files = docFileRepo.getByOwner(sessUser.getId());
        %>
        <% for (FileBean file : files) { %>
        <%
            if (file.getRequestCategory() != 1) { // Dont show files that DON'T belong to Default category
                continue;
            }
        %>
        <tr id="row-<%=file.getId()%>">
            <td><span class="glyphicon glyphicon-file"></span> <%=file.getFileName()%></td>
            <% ArrayList<SharingBean> shared = docShareRepo.getByFile(file.getId());%>
            <td><%=(shared.size() > 0) ? "Yes" : "No"%></td>
            <td><%=file.getCreatedDate()%></td>
            <td><%=file.getFileSize() / 1024%> KByte</td>
            <td>
                <button id="download-btn" onclick="downloadDoc(<%=file.getId()%>)"type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-save"></span></button>
                <button id="view-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></button>
                <button id="share-btn"  onclick="askToShare(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-new-window"></span></button>
                <button id="remove-btn" onclick="removeDocDialog(<%=file.getId()%>)"type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-remove"></span></button>
            </td>
        </tr>
        <% } %>  

    </tbody>
</table>
<div id="remove-doc" style="display:none;">
    <p> When you remove a document from the system, no user can access it, download or modify it.</p>
    <p>This operation cannot be undone.</p>
    <br>

    <p>Are you sure you want to remove it?</p>
</div>
<div id="share-file" style="display:none;">
    <form id="share-file-form" method="post" enctype="multipart/form-data">
        <label for="user-shared">Share With:</label>
        <select id="user-shared" name="user-shared" class="form-control">
            <% ArrayList<UserBean> users = rUsrFolder.getActive(); %>
            <% for (UserBean user : users) {%>
            <%
                if (user.getId() == ((UserBean) session.getAttribute("sessionUser")).getId()) {
                    continue;
                }
            %>
            <option value="<%=user.getId()%>"><%=user.getFirstName() + " " + user.getLastName()%></option>
            <% }%>
        </select>
    </form>
</div>

<div id="upload-doc" style="display:none;">
    <form id="create-doc-form" action="${pageContext.request.contextPath}/documents/create" method="post" enctype="multipart/form-data">
        <label for="doc-name">Name:</label>
        <input type="text" id="doc-name" name="doc-name" class="form-control"><br>
        <label for="upload-document">Upload:</label>
        <input type="file" id="upload-document" name="upload-document" class="form-control">
        <input type="number" name="user" id="user" hidden="true" value="${sessionScope.sessionUser.getId()}">
        <input type="number" name="category" id="category" hidden="true" value="1">
        <input type="number" name="srcFormId" id="srcFormId" hidden="true" value="<%=docFormRepo.getByNameId("mydocuments-doc").getId()%>" >
    </form>
</div>    

<script type="text/javascript" charset="utf-8">
    //DataTable Javascript
    $(document).ready(function() {
        $('.myDocuments-doc-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });

    function downloadDoc(fileId) {
        var object = "file=";
        var url = "${pageContext.request.contextPath}/document/download?" + object + fileId;
        window.location.href = url;
    }

    function shareFile(fileId) {
        /* get some values from elements on the page: */
        var $form = $('#share-file-form');
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/share-file",
            // the data to send (will be converted to a query string)
            data: $form.serialize() + "&fileId=" + fileId,
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function removeDoc(fileId) {

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/remove",
            // the data to send (will be converted to a query string)
            data: {fileId: fileId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $('#row-' + fileId).toggle("fadeOut", function() {
                        $('#row-' + fileId).remove();
                    });
                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    // Render message in result div
    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }

    // Create dialog that asks if sure
    function removeDocDialog(fileId) {
        $(function() {

            $("#remove-doc").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Yes": function() {
                        console.log("Clicked YES");
                        removeDoc(fileId);
                        $(this).dialog("destroy");
                    },
                    No: function() {
                        $(this).dialog("destroy");
                    }
                }
            });
            $("span.ui-dialog-title").text('Upload Document');
        });
    }

    //From Dialog ask to share a file
    function askToShare(fileId) {
        $(function() {

            $("#share-file").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Share": function() {
                        $(shareFile(fileId)),
                                $(this).dialog("close");

                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Share File');
        });
    }

    //From Dialog upload document
    function uploadPersDocument() {
        $(function() {

            $("#upload-doc").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Upload": function() {
                        $(this).dialog().find('form').submit();
                        $(this).dialog("destroy");

                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Upload Document');
        });
    }
</script>
