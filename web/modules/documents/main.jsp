<%@page import="org.locusdoc.utilities.ApplicationException"%>
<%@page import="org.locusdoc.businessLayer.UserStateApproverBean"%>
<%@page import="org.locusdoc.businessLayer.StateBean"%>
<%@page import="org.locusdoc.businessLayer.FileBean"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<jsp:useBean id="docCatRepo" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page" />
<jsp:useBean id="docFileRepo" class="org.locusdoc.businessLayer.FileRepository" scope="page" />
<jsp:useBean id="docStateRepo" class="org.locusdoc.businessLayer.StateRepository" scope="page" />
<jsp:useBean id="docApproverRepo" class="org.locusdoc.businessLayer.UserStateApproverRepository" scope="page" />
<jsp:useBean id="docFormRepo" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<%@page import="java.util.ArrayList"%>
<h2>Main documents</h2>
<div id="result" style="display: none"></div>
<button type="button" id="add-new" onclick="uploadMainDocument()" class="btn btn-primary">New Document</button>
<table class="main-doc-list table table-hover">
    <thead>
        <tr>
            <th>File Name</th>
            <th>Category</th>
            <th>State</th>
            <th>Created Date</th>
            <th>Size</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>   
        <%
            UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
            ArrayList<FileBean> files = docFileRepo.getByOwner(sessUser.getId());
        %>
        <% for (FileBean file : files) { %>
        <% if (file.getRequestCategory() == 1) {
                continue;
            }// Dont show files that belong to Default category %>
        <tr id="row-main-<%=file.getId()%>">
            <td><span class="glyphicon glyphicon-file"></span> <%=file.getFileName()%></td>
            <% FileCategoryBean category = docCatRepo.getById(file.getRequestCategory());%>
            <td><%=category.getName()%></td>
            <% StateBean state = docStateRepo.getById(file.getState());%>
            <td><%=state.getDescription()%></td>
            <td><%=file.getCreatedDate()%></td>
            <td><%=file.getFileSize() / 1024%> KByte</td>
            <td>
                <button id="download-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-save"></span></button>
                <button id="view-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></button>
                <button id="remove-btn" onclick="removeDocDialog(<%=file.getId()%>)"type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-remove"></span></button>
            </td>
        </tr>
        <% } %>
    </tbody>
</table>
<br><br>        
<h2>Documents for approval</h2>
<table id="approve-docs-list" class="table table-hover">
    <thead>
        <tr>
            <th>File Name</th>
            <th>Category</th>
            <th>State</th>
            <th>Created Date</th>
            <th>Size</th>
            <th>Actions</th>
        </tr>
    </thead>       
    <%
        ArrayList<UserStateApproverBean> myApproves = null;%>
    <%
        try {
            myApproves = docApproverRepo.getByUser(sessUser.getId());
        } catch (ApplicationException ex) {
            out.print("Error retrieving files you can approve.<br>");
            out.print(ex.getMessage());
        }

        for (UserStateApproverBean appr : myApproves) {

            ArrayList<FileBean> approveFiles = docFileRepo.getByCategoryAndState(appr.getFileCategory(), appr.getState());
    %>
    <% for (FileBean file : approveFiles) {%>
    <%
        if (file.getOwner() == ((UserBean) session.getAttribute("sessionUser")).getId()) {
            continue;
        }
    %>

    <tr id="row-<%=file.getId()%>-<%=file.getState()%>">
        <td><span class="glyphicon glyphicon-file"></span> <%=file.getFileName()%></td>
        <% FileCategoryBean category = docCatRepo.getById(file.getRequestCategory());%>
        <td><%=category.getName()%></td>
        <% StateBean state = docStateRepo.getById(file.getState());%>
        <td><%=state.getDescription()%></td>
        <td><%=file.getCreatedDate()%></td>
        <td><%=file.getFileSize() / 1024%> KByte</td>
        <td>
            <button id="approve-btn" onclick="approveDocDialog(<%=file.getId()%>, <%=state.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-ok"></span></button>
            <button id="download-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-save"></span></button>
            <button id="view-btn" onclick="downloadDoc(<%=file.getId()%>)" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></button>
            <button id="remove-btn" type="button" class="action-buttons btn btn-primary"><span class="glyphicon glyphicon-remove"></span></button>
        </td>
    </tr>
    <% } %>
    <% } %>

</tbody>
</table>
<div id="approve-doc" style="display:none;">
    <p>Depending on this documents workflow after you approve it,<br> it's state will be changed to Approved, or sent to the next approver.</p>
    <p>Refer to your companies policies for more details.</p>
    <br>
    <p>Do you still want to approve this document?</p>
</div>
<div id="remove-doc" style="display:none;">
    <p> When you remove a document from the system, no user can access it, download or modify it.</p>
    <p>This operation cannot be undone.</p>
    <br>

    <p>Are you sure you want to remove it?</p>
</div>
<div id="upload-doc" style="display:none;">
    <form id="create-doc-form" action="${pageContext.request.contextPath}/documents/create" method="post" enctype="multipart/form-data">
        <label for="doc-name">Name:</label>
        <input type="text" id="doc-name" name="doc-name" class="form-control">
        <label for="doc-category">Category:</label>
        <select id="doc-category" name="category" class="form-control">
            <% ArrayList<FileCategoryBean> categorylist = docCatRepo.getAll(); %>
            <% for (FileCategoryBean filecategory : categorylist) {%>
            <%
                if (filecategory.getId() == 1) { //Dont show the default category
                    continue;
                }
            %>
            <option value="<%=filecategory.getId()%>"><%=filecategory.getName()%></option>
            <% }%>
        </select>
        <label for="upload-document">Upload:</label>
        <input type="file" id="upload-document" name="upload-document" class="form-control">
        <input type="number" name="user" id="userId" hidden="true" value="${sessionScope.sessionUser.getId()}">
        <input type="number" name="srcFormId" id="srcFormId" hidden="true" value="<%=docFormRepo.getByNameId("main-doc").getId()%>" >
    </form>
</div>    

<script type="text/javascript" charset="utf-8">
    //DataTable Javascript
    $(document).ready(function() {
        $('.main-doc-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });
    //DataTable Javascript
    $(document).ready(function() {
        $('#approve-docs-list').dataTable({
            bFilter: false,
            bInfo: false,
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });

    function downloadDoc(fileId) {
        console.log("Called downloadDoc");
        var object = "file=";
        var url = "${pageContext.request.contextPath}/document/download?" + object + fileId;
        window.location.href = url;
    }

    //From Dialog upload document
    function uploadMainDocument() {
        $(function() {

            $("#upload-doc").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Upload": function() {
                        $(this).dialog().find('form').submit();
                        $(this).dialog("destroy");

                    },
                    Cancel: function() {
                        $(this).dialog("destroy");
                    }
                }
            });
            $("span.ui-dialog-title").text('Upload Document');
        });
    }
    // Create dialog that asks if sure
    function approveDocDialog(fileId, stateId) {
        $(function() {

            $("#approve-doc").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Yes": function() {
                        console.log("Clicked YES");
                        approveDoc(fileId, stateId);
                        $(this).dialog("destroy");
                    },
                    No: function() {
                        $(this).dialog("destroy");
                    }
                }
            });
            $("span.ui-dialog-title").text('Upload Document');
        });
    }

    // Create dialog that asks if sure
    function removeDocDialog(fileId) {
        $(function() {

            $("#remove-doc").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Yes": function() {
                        console.log("Clicked YES");
                        removeDoc(fileId);
                        $(this).dialog("destroy");
                    },
                    No: function() {
                        $(this).dialog("destroy");
                    }
                }
            });
            $("span.ui-dialog-title").text('Upload Document');
        });
    }

    var genMsg; // Generated message from backend
    var success; // Return value generated from backend, 1 or 0
    function approveDoc(fileId, stateId) {

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/approve",
            // the data to send (will be converted to a query string)
            data: {fileId: fileId, stateId: stateId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                $('#row-' + fileId + '-' + stateId).toggle("fadeOut", function() {
                    $('#row-' + fileId + '-' + stateId).remove();
                });
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function removeDoc(fileId) {

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/remove",
            // the data to send (will be converted to a query string)
            data: {fileId: fileId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                $('#row-main-' + fileId).toggle("fadeOut", function() {
                    $('#row-main-' + fileId).remove();
                });
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }
</script>
