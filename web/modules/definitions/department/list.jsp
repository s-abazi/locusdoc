<%@page import="org.locusdoc.businessLayer.DepartmentBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.DepartmentRepository"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="depList" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<jsp:useBean id="frmList" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler dep_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (dep_list_perms.hasPermission(frmList.getByTitle("Department List"), "VIEW")) { %>
<h2>Department List</h2>
<div id="result" style="display: none"></div>
<% if (dep_list_perms.hasPermission(frmList.getByTitle("Department List"), "INSERT")) { %>
<button type="button" id="add-new" onclick="addNewDepartment(-1)" class="btn btn-primary">Add New</button>
<% } %>

<div id="depts-table" >

</div>

<div id="register-form" style="display: none">  
    <!-- Dialog content is shown here -->
</div>

<script type="text/javascript">

    //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    // When document is loaded
    $(document).ready(function() {
        loadData();
    });

    // Construct datable on the table-list
    function loadData() {
        // Load table-list.jsp inside table element
       $.get('${pageContext.request.contextPath}/modules/definitions/department/table-list.jsp', '', function(d) {
            $('#depts-table').html(d);
            initTable();
        });
    }

    // Create Datatable
    function initTable() {
        $('#department-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>",
            "bDestroy": true // Destroy any old Datables
        });
    }

    var genMsg;
    var success;

    //Register a new Department function
    function registerDepartment() {

        var $form = $('#dept-form');
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/department/repository",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text(); // Get value as text of child success
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
            loadData(); // Redraw table if the request finished successfully
            console.log("Reloading datatable of Departmens");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }

    //Add or Edit a Department
    function addNewDepartment(id) {
        httpGet = "";
        if (id > 0) {
            httpGet = "?form=" + id;
        }
        $(document).ready(function() {
            var form = $('#dept-form');
            $('#register-form').load(
                    "${pageContext.request.contextPath}/modules/definitions/department/register.jsp" +
                    httpGet,
                    function() {
                        $('#dept-form').dialog({
                            width: "auto",
                            dialogClass: 'dialog-custom', // custom css class to add css style without changing the original
                            show: {
                                effect: "fadeIn",
                                duration: 250
                            },
                            hide: {
                                effect: "fadeOut",
                                duration: 250
                            },
                            buttons: {
                                "Save": function() {
                                    if ($('#dept-form').valid()) { // validate form inside dialog
                                        registerDepartment();
                                        $('#dept-form').dialog("destroy");
                                    }
                                },
                                Cancel: function() {
                                    $(this).dialog("close");
                                    $('#dept-form').dialog("destroy");
                                }
                            }
                        });
                        if (id > 0) {
                            $("span.ui-dialog-title").text('Modify Department');
                        } else {
                            $("span.ui-dialog-title").text(
                                    'Register a new Department');
                        }
                    });
        });
    }
</script>

<% }%>