<%@page import="org.locusdoc.businessLayer.DepartmentBean"%>
<%@page import="org.locusdoc.businessLayer.DepartmentRepository"%>

<jsp:useBean id="depDlg" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<%
    String value = request.getParameter("form");
    Integer depId = (value != null) ? Integer.parseInt(value) : 0;
    DepartmentBean dep = null;
    boolean isUpdate = false;
    if (depId > 0) {
        dep = depDlg.getById(depId);
        isUpdate = true;
    }
%>

<form id="dept-form" class="register-form" role="form" method="post" style="display: none">
    <h2></h2>
    <table>
        <tr>
            <td> 
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Name"
                       value="<%=(isUpdate) ? dep.getName() : ""%>"><br>
            </td>
        </tr>

        <input type="number" name="isUpdate" id="isUpdate" hidden="true" value="<%=(isUpdate) ? 1 : 0%>">  
        <input type="number" name="depId" id="depId" hidden="true" value="<%=(isUpdate) ? dep.getId() : 0%>">
    </table>
</form>
<script>
    $(document).ready(function() {
        $('#dept-form').validate({
            rules: {
                name: "required"
            },
            messages: {
                name: "*This field is required"
            },
            submitHandler: function(form) {
                registerDepartment();
            }
        });
    });
</script>