<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.DepartmentBean"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="depList" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<jsp:useBean id="frmList" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler dep_table_perms = (PermissionHandler) session.getAttribute("permissions");%>

<table id="department-list" class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        <%
            ArrayList<DepartmentBean> departmentList = depList.getAll();
            for (DepartmentBean department : departmentList) {
        %>

        <tr id="row-<%=department.getId()%>">
            <td><%=department.getId()%></td>
            <td><%=department.getName()%></td>
            <td>
                <% if (dep_table_perms.hasPermission(frmList.getByTitle("Department List"), "UPDATE")) {%>
                <button id="edit-btn" onclick="addNewDepartment(<%=department.getId()%>)" type="button" class="action-buttons btn btn-primary">Edit</button>
                <% }%>
            </td>
        </tr>
        <% }%>
    </tbody>
</table>

