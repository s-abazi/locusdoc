<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryRepository"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="fileCat" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page" />
<jsp:useBean id="dFrmRepo" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler category_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (category_list_perms.hasPermission(dFrmRepo.getByTitle("File Category"), "VIEW")) { %>
<h2>Category List</h2>
<div id="result" tabindex="-1" style="display: none"></div>
<% if (category_list_perms.hasPermission(dFrmRepo.getByTitle("File Category"), "INSERT")) { %>
<button type="button" id="add-new" onclick="addNewCategory(-1)" class="btn btn-primary">Add New</button>
<% } %>
<div id="category-table" >

</div>

<div id="register-form" style="display: none">  
    <!-- Dialog content is shown here -->
</div>
<script type="text/javascript">

    //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    // When document is loaded
    $(document).ready(function() {
        loadData();
    });

    // Construct datable on the table-list
    function loadData() {
        // Load table-list.jsp inside table element
        $.get('${pageContext.request.contextPath}/modules/definitions/document/category-table.jsp', '', function(d) {
            $('#category-table').html(d);
            initTable();
        });
    }

    // Create Datatable
    function initTable() {
        $('#filecateogry-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>",
            "bDestroy": true // Destroy any old Datables
        });
    }

    var genMsg;
    var success;
    //Register a new Role function
    function registerCategory() {

        var $form = $('#filecategory-form');
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/category-add",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child success
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
            loadData(); // Redraw table if the request finished successfully
            console.log("Reloading datatable of Departmens");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }

    function addNewCategory(id) {
        //Add or Edit a FileCategory
        httpGet = "";
        if (id > 0) {
            httpGet = "?form=" + id;
        }
        $(document).ready(function() {
            var form = $('#filecategory-form');
            $('#register-form').load("${pageContext.request.contextPath}/modules/definitions/document/register.jsp" + httpGet,
                    function() {
                        $('#filecategory-form').dialog({
                            width: "auto",
                            dialogClass: 'dialog-custom', // custom css class to add css style without changing the original
                            show: {
                                effect: "fadeIn",
                                duration: 250
                            },
                            hide: {
                                effect: "fadeOut",
                                duration: 250
                            },
                            buttons: {
                                "Save": function() {
                                    if ($('#filecategory-form').valid()) { // validate form inside dialog
                                        registerCategory();
                                        $('#filecategory-form').dialog("destroy");
                                    }
                                },
                                Cancel: function() {
                                    $(this).dialog("close");
                                    $('#filecategory-form').dialog("destroy");
                                }
                            }
                        });
                        if (id > 0) {
                            $("span.ui-dialog-title").text('Modify Category');
                        }
                        else {
                            $("span.ui-dialog-title").text('Register a new Category');
                        }
                    });
        });
    }
</script>

<% }%>