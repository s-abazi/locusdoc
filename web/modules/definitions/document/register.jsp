<%@page import="org.locusdoc.businessLayer.WorkflowBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryRepository"%>

<jsp:useBean id="repFc" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page" />
<jsp:useBean id="repWfl" class="org.locusdoc.businessLayer.WorkflowRepository" scope="page" />
<%
    String value = request.getParameter("form");
    Integer catId = (value != null) ? Integer.parseInt(value) : 0;
    FileCategoryBean category = null;
    boolean isUpdate = false;
    if (catId > 0) {
        category = repFc.getById(catId);
        isUpdate = true;
    }
%>
<%
    ArrayList<WorkflowBean> workflow0 = repWfl.getByFileCategory(1);
    ArrayList<WorkflowBean> workflow1 = repWfl.getByFileCategory(3);
    ArrayList<WorkflowBean> workflow2 = repWfl.getByFileCategory(2);
    ArrayList[] workflows = {workflow0, workflow1, workflow2};
%>
<form id="filecategory-form" class="register-form" role="form" method="post" style="display: none">
    <table>
        <tr>
            <td> 
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Name"
                       value="<%=(isUpdate) ? category.getName() : ""%>"><br>
            </td>
            <td>
                <label for="name">Workflow:</label>
                <select id="wokflow-select" name="workflow" class="form-control">
                    <%
                        ArrayList<WorkflowBean> catFlow = null;
                        if (isUpdate) {
                            catFlow = repWfl.getByFileCategory(category.getId());
                        }
                    %>
                    <%
                        for (int i = 0; i < workflows.length; i++) {
                    %>
                    <option value="<%=i%>" name="workflow"
                            <%
                                if (isUpdate) {
                                    if (catFlow.size() == workflows[i].size()) {
                                        out.print(" selected");
                                    } else {
                                        out.print(" disabled");
                                    }
                                }
                            %>
                            > Workflow <%=i%> </option>
                    <% }%>


                </select><br>
            </td>
        </tr>


        <input type="number" name="isUpdate" id="isUpdate" hidden="true" value="<%=(isUpdate) ? 1 : 0%>">
        <% if (isUpdate) {%>
        <input type="number" name="catId" id="catId" hidden="true" value="<%=category.getId()%>">
        <% }%>
    </table>
    <h4 id="workflow-diagram-title">Workflow 1 Diagram</h4>
    <img id="workflow-diagram" src="" alt="workflow 1"/>
</form>

<script>
    $(document).ready(function() {
        $('#filecategory-form').validate({
            rules: {
                name: "required"
            },
            messages: {
                name: "*This field is required"
            },
            submitHandler: function(form) {
                registerCategory();
            }
        });
    });

    $(document).ready(function() {
        loadImage();
    });

    // Load diagram of selected workflow inside #workflow-diagram
    function loadImage() {
        var diagram = $("#workflow-diagram");
        var diagram_title = $('#workflow-diagram-title');
        var wf = $('#wokflow-select').children(':selected').val();
        var url = "${pageContext.request.contextPath}/images/workflows/" + wf + ".png";
        console.log("Url is " + wf);
        diagram.attr("src", url);
        diagram_title.text("Workflow " + wf + " Diagram");
    }

    // When the select input is changed, call loadImage()
    $("#wokflow-select").change(function() {
        loadImage();
    });
</script>