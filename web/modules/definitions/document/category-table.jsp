<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryRepository"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="fileCat" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page" />
<jsp:useBean id="dFrmRepo" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler category_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<table id="filecateogry-list" class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Editable</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <%
            ArrayList<FileCategoryBean> categorylist = fileCat.getAll();
            for (FileCategoryBean filecategory : categorylist) {
        %>
        <tr>
            <td><%=filecategory.getId()%></td>
            <td><%=filecategory.getName()%></td>
            <td><%=filecategory.getEditable()%></td>
            <td>
                <%if (category_list_perms.hasPermission(dFrmRepo.getByTitle("File Category"), "UPDATE")) {%>
                <button id="edit-btn" onclick="addNewCategory(<%=filecategory.getId()%>)" type="button" class="action-buttons btn btn-primary">Edit</button>
                <% } %>
            </td>
        </tr>
        <% }%>
    </tbody>
</table>