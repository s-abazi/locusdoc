<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<jsp:useBean id="aFrmRep" class="org.locusdoc.businessLayer.FormRepository" scope="page"/>
<jsp:useBean id="aFileCat" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page"/>

<% PermissionHandler approver_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (approver_perms.hasPermission(aFrmRep.getByTitle("Approvers"), "VIEW")) { %>

<h2>Approvers List</h2>
<!-- AJAX result will be displayed here -->
<div id="result" style="display: none"></div>

<select id="cat-select" class="role-select form-control">
    <option disabled selected>Select Category</option>
    <% for (FileCategoryBean filecategory : aFileCat.getAll()) {%>
    <option value="<%=filecategory.getId()%>"><%=filecategory.getName()%></option>
    <% }%>
</select>

<% if (approver_perms.hasPermission(aFrmRep.getByTitle("Approvers"), "INSERT")) { %>
<button type="button" id="add-new" onclick="newApproverDialog()" class="btn btn-primary" disabled="disabled">
    New Approver</button>
    <% } %>

<div id="approver-list-container">
    <!-- Load Approvers for selected Category -->
</div>

<script type="text/javascript" charset="utf-8">


    $("#cat-select").change(
            function() {
                var idOfCat = "";
                idOfCat = $('#cat-select').children(':selected').val();
                // When any option is selected, load the permissions for it
                loadApprovers(idOfCat);
            });


    function loadApprovers(catId) {
        var url = "/modules/definitions/document/approver-list.jsp";
        url += "?cat=" + catId;
        // Load permissions for given role in #permission-list-container
        $("#approver-list-container").load("${pageContext.request.contextPath}" + url);
        // Now enable the Add New button since the userhas selected a role
        $("#add-new").prop("disabled", false);
    }

    //Create dialog for new user state registration
    function newApproverDialog() {
        $(function() {
 
            $("#new-approver").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Save": function() {
                        addNewApprover();
                        $('#new-approver').dialog("destroy");
                        var idOfCat = "";
                        idOfCat = $('#cat-select').children(':selected').val();
                        // Load the permissions for the selected category after adding the approver
                        loadApprovers(idOfCat);
                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            // Title of dialog
            $("span.ui-dialog-title").text('New Approver');
        });
    }
</script>

<% }%>