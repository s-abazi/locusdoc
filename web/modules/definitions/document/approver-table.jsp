<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.StateBean"%>
<%@page import="org.locusdoc.businessLayer.UserStateApproverBean"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="aUsrRep" class="org.locusdoc.businessLayer.UserRepository" scope="page"/>
<jsp:useBean id="aStateRep" class="org.locusdoc.businessLayer.StateRepository" scope="page"/>
<jsp:useBean id="aFileCatRep" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page"/>
<jsp:useBean id="aUsrStRep" class="org.locusdoc.businessLayer.UserStateApproverRepository" scope="page"/>
<jsp:useBean id="formPermList" class="org.locusdoc.businessLayer.FormRepository" scope="page"/>

<% PermissionHandler a_list_perms = (PermissionHandler) session.getAttribute("permissions");%>
<%    // Store GET parameter and check if its valid
    FileCategoryBean cat = null;
    String value = request.getParameter("cat");
    Integer catId = (value != null) ? Integer.parseInt(value) : 0;
    if (catId > 0) {
        // Instatiate category
        cat = aFileCatRep.getById(catId);
    }
    ArrayList<UserStateApproverBean> userStateApprover = null;
    if (cat != null) {
        // Get all states for this file category
        userStateApprover = aUsrStRep.getByFileCategory(cat.getId());
    }
%>

<table class="approver-list table table-hover">
    <thead>
        <tr>
            <th>User</th>
            <th>State</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <% for (UserStateApproverBean userState : userStateApprover) { %>
        <% UserBean user = aUsrRep.getById(userState.getUser());%>
        <tr id="row-<%=user.getId()%>">     
            <td><%="[" + user.getId() + "] " + user.getFirstName() + " " + user.getLastName()%></td>
            <% StateBean state = aStateRep.getById(userState.getState());%>
            <td><%=state.getDescription()%></td>
            <td>
                <% if (a_list_perms.hasPermission(formPermList.getByTitle("Approvers"), "DELETE")) {%>
                <button id="remove-btn" onclick="askToRemove(<%=userState.getUser()%>,<%=userState.getFileCategory()%>,<%=userState.getState()%>)" 
                        type="button" class="action-buttons btn btn-disable">Remove</button>
                <% } %>        
            </td>
        </tr>
        <% }%>
    </tbody>
</table>

<div id="remove-confirm" style="display:none">
    <p>Are you sure you wanna remove this user from "<%=cat.getName()%>" category?</p>
</div>

<div id="new-approver" style="display: none">
    <form id="new-approver-form" class="register-form" role="form" method="post">
        <table>
            <tr>
                <td>
                    <label for="user">User:</label>
                    <select id="user" name="user" class="form-control">

                        <% ArrayList<UserBean> users = aUsrRep.getAll(); %>
                        <% for (UserBean user : users) {%>
                            <option value="<%=user.getId()%>"> <%="[" + user.getId() + "] " + user.getFirstName() + " "
                                + user.getLastName()%> </option>
                            <% }%>
                    </select><br>
                </td>

                <td>
                    <label for="state">State:</label>
                    <select id="state" name="state" class="form-control">

                        <% ArrayList<StateBean> states = aStateRep.getAll(); %>
                        <% for (StateBean state : states) {%>
                        <option value="<%=state.getId()%>"> <%=state.getDescription()%> </option>
                        <% }%>
                    </select><br>
                </td>
            <input type="number" value="<%=cat.getId()%>" name="category" hidden="true">
            </tr>
        </table>
    </form>
</div>