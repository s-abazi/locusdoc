<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.FileCategoryBean"%>
<%@page import="org.locusdoc.businessLayer.StateBean"%>
<%@page import="org.locusdoc.businessLayer.UserStateApproverBean"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="aUsrRep" class="org.locusdoc.businessLayer.UserRepository" scope="page"/>
<jsp:useBean id="aStateRep" class="org.locusdoc.businessLayer.StateRepository" scope="page"/>
<jsp:useBean id="aFileCatRep" class="org.locusdoc.businessLayer.FileCategoryRepository" scope="page"/>
<jsp:useBean id="aUsrStRep" class="org.locusdoc.businessLayer.UserStateApproverRepository" scope="page"/>
<jsp:useBean id="formPermList" class="org.locusdoc.businessLayer.FormRepository" scope="page"/>

<% PermissionHandler a_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<%    // Store GET parameter and check if its valid
    FileCategoryBean cat = null;
    String value = request.getParameter("cat");
    Integer catId = (value != null) ? Integer.parseInt(value) : 0;
    if (catId > 0) {
        // Instatiate category
        cat = aFileCatRep.getById(catId);
    }
    ArrayList<UserStateApproverBean> userStateApprover = null;
    if (cat != null) {
        // Get all states for this file category
        userStateApprover = aUsrStRep.getByFileCategory(cat.getId());
    }
%>

<table class="approver-list table table-hover">
    <thead>
        <tr>
            <th>User</th>
            <th>State</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <% for (UserStateApproverBean userState : userStateApprover) { %>
        <tr id="row-<%=userState.getFileCategory()%>-<%=userState.getState()%>-<%=userState.getUser()%>">    
            <% UserBean user = aUsrRep.getById(userState.getUser());%>
            <td><%="[" + user.getId() + "] " + user.getFirstName() + " " + user.getLastName()%></td>
            <% StateBean state = aStateRep.getById(userState.getState());%>
            <td><%=state.getDescription()%></td>
            <td>
                <% if (a_list_perms.hasPermission(formPermList.getByTitle("Approvers"), "DELETE")) {%>
                <button id="remove-btn" onclick="askToRemove(<%=userState.getUser()%>,<%=userState.getFileCategory()%>,<%=userState.getState()%>)" 
                        type="button" class="action-buttons btn btn-disable">Remove</button>
                <% } %>        
            </td>
        </tr>
        <% }%>
    </tbody>
</table>

<div id="remove-confirm" style="display:none">
    <p>Are you sure you wanna remove this user from "<%=cat.getName()%>" category?</p>
</div>

<div id="new-approver" style="display: none">
    <form id="new-approver-form" class="register-form" role="form" method="post">
        <table>
            <tr>
                <td>
                    <label for="user">User:</label>
                    <select id="user" name="user" class="form-control">

                        <% ArrayList<UserBean> users = aUsrRep.getAll(); %>
                        <% for (UserBean user : users) {%>
                            <option value="<%=user.getId()%>"> <%="[" + user.getId() + "] " + user.getFirstName() + " "
                                + user.getLastName()%> </option>
                            <% }%>
                    </select><br>
                </td>

                <td>
                    <label for="state">State:</label>
                    <select id="state" name="state" class="form-control">

                        <% ArrayList<StateBean> states = aStateRep.getAll(); %>
                        <% for (StateBean state : states) {%>
                        <option value="<%=state.getId()%>"> <%=state.getDescription()%> </option>
                        <% }%>
                    </select><br>
                </td>
            <input type="number" value="<%=cat.getId()%>" name="category" hidden="true">
            </tr>
        </table>
    </form>
</div>
<script>
    //DataTable Javascript
    $(document).ready(function() {
        $('.approver-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });

    function removeApprover(user, fileCategory, state) {

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/delete-approver",
            // the data to send (will be converted to a query string)
            data: {user: user, fileCategory: fileCategory, state: state},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                $('#row-' + fileCategory+'-'+state+'-'+user).toggle("fadeOut", function() {
                $('#row-'+fileCategory+'-'+state+'-'+user).remove();
                });
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    // Render message in result div
    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");        
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }

    //Create Dialog to for confirmation
    function askToRemove(user, fileCategory, state) {
        $(function() {

            $("#remove-confirm").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Remove": function() {
                        $(removeApprover(user, fileCategory, state)),
                                $(this).dialog("destroy");
                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Remove Approver');
        });
    }

    function addNewApprover() {
        var $form = $("#new-approver-form");

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/document/register-approver",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
            }
        });
    }


</script>