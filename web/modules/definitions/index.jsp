<%@page import="org.locusdoc.businessLayer.FormBean" %>
<%@page import="org.locusdoc.businessLayer.FormRepository" %>
<%@include file="/modules/layout/header.jsp"%>
<jsp:useBean id="repoDef" class="org.locusdoc.businessLayer.FormRepository" scope="request"/>
<% PermissionHandler def_perms = (PermissionHandler) session.getAttribute("permissions");%>


<%    // Store GET parameter and check if its valid
    FormBean getForm = null;
    String value = request.getParameter("form");
    Integer formId = (value != null) ? Integer.parseInt(value) : 0;
    if (formId > 0) {
        // Instatiate form
        getForm = repoDef.getById(formId);
    }
%>
<script>
    $(document).ready(function() {
        var item = document.getElementById('def-link');
        $(item).attr("class", "active");
    });
</script>

<div id="content" class="container">
    <c:if test="${sessionScope.sessionUser.changedPass == 0}" >
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Hello ${sessionUser.firstName}, this seems to be your first time use. Please click <a href="${pageContext.request.contextPath}/modules/account/" class="alert-link">here</a> to change your generated password.
        </div>  
    </c:if>
    <div class="main-container">
        <div class="sidebar col-md-3">
            <!-- Dynamic left menu generation -->

            <div class="navbar-header">
                <a class="navbar-second-toggle" data-toggle="collapse" data-target=".navbar-second-collapse">
                    Sidebar Menu <b class="caret"></b>
                </a>
            </div>
            <div class="navbar-second-collapse collapse">

                <ul class="navgoco nav-pills nav-stacked " id="navTabs">
                    <%
                        ArrayList<FormBean> parentForms = repoDef.getByPosition(3, null);
                        for (FormBean parent : parentForms) {
                            if(def_perms.hasPermission(parent, "VIEW")){
                    %>
                    <li>
                        <a href="#"><%=parent.getTitle()%> <b class="caret"></b></a>
                        <ul class="dropdown-settings">
                            <%
                                ArrayList<FormBean> subForms = repoDef.getByParent(parent.getId());
                                for (FormBean sub : subForms) {
                                    if(def_perms.hasPermission(sub, "VIEW")){
                            %>
                            <li class="settings-navbar" id="<%=sub.getNameId()%>" 
                                onclick="loadForm('<%=sub.getPath()%>')">
                                <%=sub.getTitle()%>
                            </li>
                                <% } %>
                            <% } %>
                        </ul>
                        <% } %>
                    </li>
                    <% }%>


                </ul>
            </div>

        </div>
        <div class="right-container col-md-9">
            <!-- Dynamic Content for the main content (e.g: documents,settings,profile etc) -->
            <% if (getForm == null) {%>
            <jsp:include page="/modules/definitions/department/list.jsp"/>
            <% }%>  
            <!-- End of dynamic content for main content(right-container) -->
        </div> 
    </div>  
</div> 
<script type="text/javascript">
    //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    function loadForm(path) {
        $(".right-container").load("${pageContext.request.contextPath}/" + path);
    }
    
    <% if (getForm != null) {%>
    loadForm("<%=getForm.getPath()%>");
    <% }%>
</script>
<%@include file="/modules/layout/footer.jsp" %>
</div>
</body>
</html>