<%@page import="org.locusdoc.businessLayer.ActionBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.FormBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.RoleBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionBean"%>
<jsp:useBean id="rolePermList" class="org.locusdoc.businessLayer.RoleRepository" scope="page"/>
<jsp:useBean id="permListRepo" class="org.locusdoc.businessLayer.PermissionRepository" scope="page"/>
<jsp:useBean id="actionPermList" class="org.locusdoc.businessLayer.ActionRepository" scope="page"/>
<jsp:useBean id="formPermList" class="org.locusdoc.businessLayer.FormRepository" scope="page"/>

<% PermissionHandler p_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<%    // Store GET parameter and check if its valid
    RoleBean role = null;
    String value = request.getParameter("role");
    Integer roleId = (value != null) ? Integer.parseInt(value) : 0;
    if (roleId > 0) {
        // Instatiate form
        role = rolePermList.getById(roleId);
    }
    ArrayList<PermissionBean> permissions = null;
    if (role != null) {
        // Get all permissions for this role
        permissions = permListRepo.getByRole(role.getId());
    }
%>
<table class="permission-list table table-hover">
    <thead>
        <tr>
            <th>Forms</th>
            <th>Action</th>
            <th>Modify</th>
        </tr>
    </thead>
    <tbody>
        <% for (PermissionBean perm : permissions) {%>
        <tr id="row-<%=perm.getForm()%>-<%=perm.getRole()%>-<%=perm.getAction()%>">    
            <% FormBean formTr = formPermList.getById(perm.getForm());%>
            <td><%="[" + formTr.getId() + "] " + formTr.getTitle()%></td>
            <td><%=actionPermList.getById(perm.getAction()).getAction()%></td>
            <td>
                <% if (p_list_perms.hasPermission(formPermList.getByTitle("Permissions"), "DELETE")) {%>
                <button id="remove-btn" onclick="askToRemove(<%=perm.getForm()%>, <%=perm.getAction()%>, <%=perm.getRole()%>)" 
                        type="button" class="action-buttons btn btn-disable">Remove</button>
                <% } %>        
            </td>
        </tr>
        <% }%>
    </tbody>
</table>

<div id="remove-confirm" style="display:none">
    <p>Are you sure you wanna remove this permission from <%=role.getName()%> role?</p>
</div>

<div id="new-permission" style="display: none">
    <form id="new-permission-form" class="register-form" role="form" method="post">
        <table>
            <tr>
                <td>
                    <label for="form">Form:</label>
                    <select id="form" name="form" class="form-control">

                        <% ArrayList<FormBean> forms = formPermList.getAll(); %>
                        <% for (FormBean form : forms) {%>
                        <option value="<%=form.getId()%>"> <%="[" + form.getId() + "] " + form.getTitle()%> </option>
                        <% }%>
                    </select><br>
                </td>

                <td>
                    <label for="action">Action:</label>
                    <select id="action" name="action" class="form-control">

                        <% ArrayList<ActionBean> actions = actionPermList.getAll(); %>
                        <% for (ActionBean action : actions) {%>
                        <option value="<%=action.getId()%>"> <%=action.getAction()%> </option>
                        <% }%>
                    </select><br>
                </td>
            <input type="number" value="<%=role.getId()%>" name="role" hidden="true">
            </tr>
        </table>
    </form>
</div>
<script>
    //DataTable Javascript
    $(document).ready(function() {
        $('.permission-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });

    function removePerm(form, action, role) {
        //console.log(form, action, role);

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/role/delete-permission",
            // the data to send (will be converted to a query string)
            data: {form: form, action: action, role: role},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                $('#row-' +form+'-'+role+'-'+action).toggle("fadeOut", function() {
                $('#row-' +form+'-'+role+'-'+action).remove();
                });
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    // Render message in result div
    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }

    //Create Dialog to for confirmation
    function askToRemove(form, action, role) {
        $(function() {

            $("#remove-confirm").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Remove": function() {
                        $(removePerm(form, action, role)),
                                $(this).dialog("destroy");
                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Remove Permission');
        });
    }

    function addNewPerm() {
        var $form = $("#new-permission-form");

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/role/register-permission",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }


</script>
