<%@page import="org.locusdoc.businessLayer.RoleBean"%>
<%@page import="org.locusdoc.businessLayer.RoleRepository"%>

<jsp:useBean id="repRl" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<%
    String value = request.getParameter("role");
    Integer roleId = (value != null) ? Integer.parseInt(value) : 0;
    RoleBean role = null;
    boolean isUpdate = false;
    if (roleId > 0) {
        role = repRl.getById(roleId);
        isUpdate = true;
    }
%>

<form id="role-form" class="register-form" role="form" method="post" style=" display: none">
    <h2></h2>
    <table>
        <tr>
            <td> 
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Name"
                       value="<%=(isUpdate) ? role.getName() : ""%>"><br>
            </td>
            <td> 
                <label for="description">Description:</label>
                <input type="text" name="description" id="description" class="form-control" placeholder="Description:"
                       value="<%=(isUpdate) ? role.getDescription() : ""%>"><br>
            </td>
        </tr>

        <input type="number" name="isUpdate" id="isUpdate" hidden value="<%=(isUpdate) ? 1 : 0%>">  
        <input type="number" name="roleId" id="roleId" hidden value="<%=(isUpdate) ? role.getId() : 0%>">
    </table>
</form>
<script>
    $(document).ready(function() {
        $('#role-form').validate({
            rules: {
                name: "required"
            },
            messages: {
                name: "*This field is required"
            },
            submitHandler: function(form) {
                registerDepartment();
            }
        });
    });
</script>