<%@page import="org.locusdoc.businessLayer.RoleBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.RoleRepository"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="repoRol" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<jsp:useBean id="rListFrm" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler role_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<table id="list-role" class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <%
            ArrayList<RoleBean> rolelist = repoRol.getAll();
            for (RoleBean rolelst : rolelist) {
        %>
        <tr>
            <td><%=rolelst.getId()%></td>
            <td><%=rolelst.getName()%></td>
            <td><%=rolelst.getDescription()%></td>
            <td>
                <% if (role_list_perms.hasPermission(rListFrm.getByTitle("Roles"), "UPDATE")) { %>
                    <button id="edit-btn" onclick="addNewRole(<%=rolelst.getId()%>)" type="button" class="action-buttons btn btn-primary">Edit</button>
                <% } %>
            </td>
        </tr>
        <% }%>
    </tbody>
</table>