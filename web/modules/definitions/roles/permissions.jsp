<%@page import="org.locusdoc.businessLayer.RoleBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<jsp:useBean id="pFrmRep" class="org.locusdoc.businessLayer.FormRepository" scope="page"/>

<% PermissionHandler perm_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (perm_list_perms.hasPermission(pFrmRep.getByTitle("Permissions"), "VIEW")) { %>

<h2>Permission List</h2>
<!-- AJAX result will be displayed here -->
<div id="result" style="display: none"></div>
<jsp:useBean id="rolesPerm" class="org.locusdoc.businessLayer.RoleRepository" scope="page"/>
<select id="role-select" class="role-select form-control">
    <option disabled selected>Select role</option>
    <% for (RoleBean role : rolesPerm.getAll()) {%>
    <option value="<%=role.getId()%>"><%=role.getName()%></option>
    <% }%>
</select>

<% if (perm_list_perms.hasPermission(pFrmRep.getByTitle("Permissions"), "INSERT")) { %>
<button type="button" id="add-new" onclick="newPermissionDialog()" class="btn btn-primary" disabled="disabled">
    New Permission</button>
    <% }%>
<div id="permission-list-container">
    <!-- Load permissions for selected role -->
</div>

<script type="text/javascript" charset="utf-8">


    $("#role-select").change(
            function() {
                var idOfRole = "";
                idOfRole = $('#role-select').children(':selected').val();
                // Whne any option is selected, load the permissions for it
                loadRolePermissions(idOfRole);
            });


    function loadRolePermissions(roleId) {
        var url = "/modules/definitions/roles/perm-list.jsp";
        url += "?role=" + roleId;
        // Load permissions for given role in #permission-list-container
        $("#permission-list-container").load("${pageContext.request.contextPath}" + url);
        // Now enable the Add New button since the userhas selected a role
        $("#add-new").prop("disabled", false);
    }
    
    //Create dialog for new permission registration
    function newPermissionDialog() {
        $(function() {

            $("#new-permission").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Save": function() {
                        //console.log("Clicked Save on newPermissionDialog");
                        addNewPerm();
                        $('#new-permission').dialog("destroy");
                        var idOfRole = "";
                        idOfRole = $('#role-select').children(':selected').val();
                        // Whne any option is selected, load the permissions for it
                        loadRolePermissions(idOfRole);
                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            // Title of dialog
            $("span.ui-dialog-title").text('New permission');
        });
    }
</script>

<% }%>