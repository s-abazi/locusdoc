<%@page import="org.locusdoc.businessLayer.RoleBean"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.RoleRepository"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="repoRol" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<jsp:useBean id="rListFrm" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler role_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (role_list_perms.hasPermission(rListFrm.getByTitle("Roles"), "VIEW")) { %>
<h2>Role List</h2>
<div id="result" tabindex="-1" style="display: none"></div>
<% if (role_list_perms.hasPermission(rListFrm.getByTitle("Roles"), "INSERT")) { %>
<button type="button" id="add-new" onclick="addNewRole(-1)" class="btn btn-primary">Add New</button>
<% } %>
<div id="role-tr">
    
</div>

<div id="register-form" style="display: none">  
    <!-- Dialog content is shown here -->
</div>

<script type="text/javascript">
    
    //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    // When document is loaded
    $(document).ready(function() {
        loadData();
    });

    // Construct datable on the table-list
    function loadData() {
        // Load table-list.jsp inside table element
       $.get('${pageContext.request.contextPath}/modules/definitions/roles/role-table.jsp', '', function(d) {
            $('#role-tr').html(d);
            initTable();
        });
    }

    // Create Datatable
    function initTable() {
        $('#list-role').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>",
            "bDestroy": true // Destroy any old Datables
        });
    }
    
    //Register a new Role function
    function registerRole() {
            var $form = $('#role-form');
            $.ajax({
                // the URL for the request
                url: "${pageContext.request.contextPath}/role/repository",
                // the data to send (will be converted to a query string)
                data: $form.serialize(),
                // whether this is a POST or GET request
                type: "POST",
                // the type of data we expect back
                dataType: "xml",
                // code to run if the request succeeds;
                // the response is passed to the function
                success: function(xml) {

                    $(xml).find("result").each(function() //To traverese result tree
                    {
                        genMsg = $(this).find("message").text(); // Get value as text of child message
                        success = $(this).find("success").text();// Get value as text of child affectedRows
                    });
                    renderHtml(genMsg, success);
                },
                // code to run if the request fails; the raw request and
                // status codes are passed to the function
                error: function(xhr, status, errorThrown) {
                    genMsg = "Sorry, there was a problem! Please, try again.";
                    console.log("Error: " + errorThrown);
                    console.log("Status: " + status);
                    console.dir(xhr);
                    renderHtml(genMsg, 0);
                },
                // code to run regardless of success or failure
                complete: function(xhr, status) {
                    //console.log("The request is complete!");
                }
            });
        }
     function renderHtml(msg, success) {
            var result = document.getElementById('result');
            result.innerHTML = msg;
            $(result).css("display", "block");
            if (success == 1) {
            $(result).attr("class", "alert alert-success");
            loadData(); // Redraw table if the request finished successfully
            console.log("Reloading datatable of Departmens");
            } else {
                $(result).attr("class", "alert alert-danger");
            }
        }
    function addNewRole(id) {
       

        //Add or Edit a Role
        httpGet = "";
        if (id > 0) {
            httpGet = "?role=" + id;
        }
        $(document).ready(function() {

            $('#register-form').load("${pageContext.request.contextPath}/modules/definitions/roles/register.jsp" + httpGet,
                    function() {
                        $('#role-form').dialog({
                            width: "auto",
                            dialogClass: 'dialog-custom', // custom css class to add css style without changing the original
                            show: {
                                effect: "fadeIn",
                                duration: 250
                            },
                            hide: {
                                effect: "fadeOut",
                                duration: 250
                            },
                            buttons: {
                                "Save": function() {
                                    if ($('#role-form').valid()) { // validate form inside dialog
                                        registerRole();
                                        $('#role-form').dialog("destroy");
                                    }
                                },
                                Cancel: function() {
                                    $(this).dialog("close");
                                    $('#role-form').dialog("destroy");
                                }
                            }
                        });
                        if (id > 0) {
                            $("span.ui-dialog-title").text('Modify Role');
                        }
                        else {
                            $("span.ui-dialog-title").text('Register a new Role');
                        }
                    });
        });
    }

</script>
                    
<% } %>