<%@page import="org.locusdoc.businessLayer.DepartmentBean"%>
<%@page import="org.locusdoc.businessLayer.EmployeeBean"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.UserRepository"%>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentRepository"%>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentBean"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="rUsrED" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<jsp:useBean id="rEdAssig" class="org.locusdoc.businessLayer.EmployeeDepartmentRepository" scope="page" />
<jsp:useBean id="rEmpAss" class="org.locusdoc.businessLayer.EmployeeRepository" scope="page" />
<jsp:useBean id="rDepAss" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<%
    String value = request.getParameter("assignment");
    Integer edId = (value != null) ? Integer.parseInt(value) : 0;
    EmployeeDepartmentBean edb = null;
    EmployeeBean empED = null;
    UserBean userED = null;
    DepartmentBean depED = null;
    boolean isUpdate = false;
    if (edId > 0) {
        edb = rEdAssig.getById(edId);
        if (edb != null) {
            empED = rEmpAss.getById(edb.getEmployee());
            userED = rUsrED.getByEmployee(empED.getId());
            depED = rDepAss.getById(edb.getDepartment());
            if (userED != null && depED != null) {
                isUpdate = true;
            }
        }
    }
%>

<form class="new-assignment-form" id="new-assignment-id" role="form" method="post">
    <table class="table" >
        <tr>
            <td>
                <label for="user">User:</label>
                <select id="user" name="user" class="form-control">

                    <% ArrayList<UserBean> users = rUsrED.getActive(); %>
                    <% for (UserBean user : users) {%>
                    <option <%= (isUpdate && userED.equals(user)) ? "selected" : ""%>
                        value="<%=user.getId()%>"><%=user.getFirstName() + " " + user.getLastName()%></option>
                    <% }%>
                </select><br>
            </td> 
            <td>
                <label for="department">Department:</label>
                <select id="department" name="department" class="form-control">

                    <% ArrayList<DepartmentBean> depts = rDepAss.getAll(); %>
                    <% for (DepartmentBean dept : depts) {%>
                    <option <%= (isUpdate && depED.equals(dept)) ? "selected" : ""%>
                        value="<%=dept.getId()%>"><%=dept.getName()%></option>
                    <% }%>
                </select><br>
            </td>
        <input type="number" name="isUpdate" id="isUpdate" hidden="true" value="<%=(isUpdate) ? 1 : 0%>">  
        <input type="number" name="edbId" id="edbId" hidden="true" value="<%=(isUpdate) ? edb.getId() : 0%>">
        </tr>

    </table>
</form>
<script>
    $(document).ready(function() {
        $('#dept-form').validate({
            rules: {
                name: "required"
            },
            messages: {
                name: "*This field is required"
            },
            submitHandler: function(form) {
                registerDepartment();
            }
        });
    });
</script>