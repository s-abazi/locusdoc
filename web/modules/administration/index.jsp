<%@page import="org.locusdoc.businessLayer.FormBean" %>
<%@page import="org.locusdoc.businessLayer.FormRepository" %>
<%@include file="/modules/layout/header.jsp"%>
<jsp:useBean id="repoAdm" class="org.locusdoc.businessLayer.FormRepository" scope="request"/>
<% PermissionHandler admin_perms = (PermissionHandler) session.getAttribute("permissions");%>

<%    // Store GET parameter and check if its valid
    FormBean getForm = null;
    String value = request.getParameter("form");
    Integer formId = (value != null) ? Integer.parseInt(value) : 0;
    if (formId > 0) {
        // Instatiate form
        getForm = repoAdm.getById(formId);
        // If form is not null but user has no permission for it, then set it to null
        if ( getForm != null && !(admin_perms.hasPermission(getForm, "VIEW")) ) {
            getForm = null;
        }
        
    }
%>
<script>
    $(document).ready(function() {
        var item = document.getElementById('admin-link');
        $(item).attr("class", "active");
    });
</script>
<div id="content" class="container">
    <c:if test="${sessionScope.sessionUser.changedPass == 0}" >
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Hello ${sessionUser.firstName}, this seems to be your first time use. Please click <a href="${pageContext.request.contextPath}/modules/account/" class="alert-link">here</a> to change your generated password.
        </div>  
    </c:if>
    <div class="main-container">
        <div class="sidebar col-md-3">
            <!-- Dynamic left menu generation -->

            <div class="navbar-header">
                <a class="navbar-second-toggle" data-toggle="collapse" data-target=".navbar-second-collapse">
                    Sidebar Menu <b class="caret"></b>
                </a>
            </div>
            <div class="navbar-second-collapse collapse">
                <ul class="navgoco nav-pills nav-stacked " id="navTabs">
                    <%
                        // Get all Parent forms with position set to 2, for Administration
                        ArrayList<FormBean> parentForms = repoAdm.getByPosition(2, null);
                        for (FormBean parent : parentForms) {
                            if (admin_perms.hasPermission(parent, "VIEW")){
                    %>
                    <li>
                        <a href="#"><%=parent.getTitle()%> <b class="caret"></b></a>
                        <ul class="dropdown-settings">
                            <%
                                // Now get all child forms for this parent
                                ArrayList<FormBean> subForms = repoAdm.getByParent(parent.getId());
                                for (FormBean sub : subForms) {
                                    if (admin_perms.hasPermission(sub, "VIEW")){
                            %>
                            <li class="settings-navbar" id="<%=sub.getNameId()%>" 
                                onclick="loadForm('<%=sub.getPath()%>')">
                                <%=sub.getTitle()%>
                            </li>
                            <% } %>
                            <% } %>
                        </ul>
                        <% } %>
                    </li>
                    <% } %>
                    <% FormBean logsForm = repoAdm.getByTitle("Logs");%>
                    <% if (admin_perms.hasPermission(repoAdm.getByTitle("Logs"), "VIEW")){ %>
                    <li class="single-navbar" id="<%=logsForm.getNameId()%>"
                        onclick="loadForm('<%=logsForm.getPath()%>')">
                        <%=logsForm.getTitle()%>
                        <%}%>
                    </li>

                </ul>
            </div>

        </div>
        <div class="right-container col-md-9">
            <!-- Dynamic Content for the main content (eg: documents,settings,profile etc) -->
            <% if (getForm == null) {%>
            <jsp:include page="/modules/administration/user/enabled-list.jsp"/>
            <% }%>            
            <!-- End of dynamic content for main content(right-container) -->
        </div> 
    </div>  
</div> 
<script type="text/javascript">
//Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    function loadForm(path) {
        $(".right-container").load("${pageContext.request.contextPath}/" + path);
    }
    <% if (getForm != null) {%>
    loadForm("<%=getForm.getPath()%>");
    <% }%>
</script>
<%@include file="/modules/layout/footer.jsp" %>
</div>
</body>
</html>