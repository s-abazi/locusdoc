<%@page import="org.locusdoc.businessLayer.ActionRepository"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentBean" %>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentRepository" %>
<%@page import="org.locusdoc.businessLayer.DepartmentBean" %>
<%@page import="org.locusdoc.businessLayer.DepartmentRepository" %>
<%@page import="org.locusdoc.businessLayer.UserBean" %>
<%@page import="org.locusdoc.businessLayer.UserRepository" %>

<jsp:useBean id="depEmAs" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<jsp:useBean id="usrEmAs" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<jsp:useBean id="edEmAs" class="org.locusdoc.businessLayer.EmployeeDepartmentRepository" scope="page" />
<jsp:useBean id="formEmAs" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler emp_assig_lperms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (emp_assig_lperms.hasPermission(formEmAs.getByTitle("Employee Assignment"), "VIEW")) { %>
        
<h2>Employee Assignment</h2>
<div id="result" style="display: none"></div>
<% if (emp_assig_lperms.hasPermission(formEmAs.getByTitle("Employee Assignment"), "INSERT")) { %>
    <button type="button" id="add-new" onclick="newAssigment(0)" class="btn btn-primary">Add New</button>
<% } %>

<div id="employee-assignment" >

</div>

<div id="new-assigment"  style="display: none">

</div>

<script type="text/javascript" charset="utf-8">
     //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    // When document is loaded
    $(document).ready(function() {
        loadData();
    });

    // Construct datable on the table-list
    function loadData() {
        // Load table-list.jsp inside table element
       $.get('${pageContext.request.contextPath}/modules/administration/employee_assignment_table.jsp', '', function(d) {
            $('#employee-assignment').html(d);
            initTable();
        });
    }

    // Create Datatable
    function initTable() {
        $('.assignment-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>",
            "bDestroy": true // Destroy any old Datables
        }).yadcf([{
                column_number: 0,
                filter_default_label: "All"
            }, {
                column_number: 1,
                filter_default_label: "All"
            }

        ]);
    }

</script>
    
<script>
    function loadAssignmentForm(assignment) {
        httpGet = "";
        if (assignment > 0) {
            httpGet = "?assignment=" + assignment;
        }
        $('#new-assigment').load("${pageContext.request.contextPath}"
                + "/modules/administration/employee_assignment_register.jsp" + httpGet);
    }

    function newAssigment(assignment) {
        loadAssignmentForm(assignment);
        $(function() {
            $("#new-assigment").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Assign": function() {
                        assignDepartment();
                        $(this).dialog("close");

                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('New Assigment');
        });
    }

    function assignDepartment() {
        /* get some values from elements on the page: */
        var $form = $('#new-assignment-id');

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/department/assignment",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
            loadData(); // Redraw table if the request finished successfully
            console.log("Reloading datatable of Departmens");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }
</script>

<% } %>
