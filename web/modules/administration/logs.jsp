<%@page import="org.locusdoc.businessLayer.UserBean" %>
<%@page import="org.locusdoc.businessLayer.UserRepository" %>
<%@page import="org.locusdoc.businessLayer.LogsBean" %>
<%@page import="org.locusdoc.businessLayer.LogsRepository" %>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="usrLg" class="org.locusdoc.businessLayer.UserRepository" scope="request"/>
<jsp:useBean id="repoLg" class="org.locusdoc.businessLayer.LogsRepository" scope="request"/>
<h2>Logs</h2>
<table class="logs table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Action</th>
            <th>Description</th>
            <th>Timestamp</th>
            <th>User</th>
        </tr>
    </thead>
    <tbody>
        <%
            ArrayList<LogsBean> logsList = repoLg.getAll();
            for (LogsBean log : logsList) {
        %>
        <tr>
            <td><%=log.getId()%></td>
            <td><%=log.getAction()%></td>
            <td><%=log.getDescription()%></td>
            <td><%=log.getTimestamp()%></td>
            <td><%=usrLg.getById(log.getUser()).getUsername()%></td>
        </tr>
        <% }%>
    </tbody>
</table>
<!-- Editable,datatable javascript -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.logs').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });
</script>
