<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="javax.servlet.http.HttpServlet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.UserRepository"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.RoleRepository" %>
<%@page import="org.locusdoc.businessLayer.RoleBean" %>
<%@page import="org.locusdoc.businessLayer.EmployeeRepository" %>
<%@page import="org.locusdoc.businessLayer.EmployeeBean" %>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentRepository" %>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentBean" %>
<%@page import="org.locusdoc.businessLayer.DepartmentRepository" %>
<%@page import="org.locusdoc.businessLayer.DepartmentBean" %>
<%@page import="org.locusdoc.businessLayer.RoleRepository" %>
<%@page import="org.locusdoc.businessLayer.RoleBean" %>

<c:if test="${sessionScope.sessionUser == null || sessionScope.sessionUser.getId() == 0}" >
    <c:set var="message" scope="request" value="You are not logged in. You have to log in to view this page."/>
    <c:redirect url="/index.jsp"/>
</c:if>
<jsp:useBean id="rRoleRg" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<jsp:useBean id="rUsrRg" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<jsp:useBean id="rEmpRg" class="org.locusdoc.businessLayer.EmployeeRepository" scope="page" />
<jsp:useBean id="rEDRg" class="org.locusdoc.businessLayer.EmployeeDepartmentRepository" scope="page" />
<jsp:useBean id="rDepRg" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<jsp:useBean id="rFormRg" class="org.locusdoc.businessLayer.FormRepository" scope="page" />
<%
    PermissionHandler regPermissions = (PermissionHandler) session.getAttribute("permissions");
    // Store GET parameter and check if its valid
    // If no valid id via HTTP GET was given, the form isempty
    UserBean user = null;
    String value = request.getParameter("userId");
    Integer userId = (value != null) ? Integer.parseInt(value) : 0;
    EmployeeBean emp = null;
    boolean isUpdate = false;
    if (userId > 0) {
        // Instatiate user
        user = rUsrRg.getById(userId);
        emp = rEmpRg.getById(user.getEmployee());
        // Now if both returned valid records, set isUpdate to TRUE
        isUpdate = (user != null && emp != null) ? true : false;
    }
    if (isUpdate) {
        UserBean thisSessionUsr = (UserBean) session.getAttribute("sessionUser");
        if (!thisSessionUsr.equals(user)) {
            if (!regPermissions.hasPermission(rFormRg.getByTitle("Register User"), "UPDATE")) {
                out.print("<h3>You don't have permission to update this user.</h3>");
                out.close();
            }
        }
    } else {
        if (!regPermissions.hasPermission(rFormRg.getByTitle("Register User"), "INSERT")) {
            out.print("<h3>You don't have permission to create a new user account.</h3>");
            out.close();
        }
    }


%>
<script>
    $(function() {
        $("#birthdate").datepicker();
    });

    $(document).ready(function() {
        $('#firstName').focus();
    });
</script>
<div id="result" style="display: none"></div>
<form id="user-register" class="register-form" role="form" method="post">
    <h2><%= (isUpdate) ? "Update User Details" : "Add New User"%></h2>
    <table>
        <tr>
            <td> 
                <label for="firstName">First Name:</label>
                <input type="text" name="firstName" id="firstName" class="form-control" placeholder="First Name"
                       value="<%=(isUpdate) ? user.getFirstName() : ""%>"><br>
            </td>
            <td>
                <label for="middleName">Middle Name:</label>
                <input type="text" name="middleName" id="middleName" class="form-control" placeholder="Middle Name"
                       value="<%=(isUpdate) ? user.getMiddleName() : ""%>"><br>    
            </td>
            <td>
                <label for="lastName">Last Name:</label>
                <input type="text" name="lastName" id="lastName" class="form-control" placeholder="Last Name"
                       value="<%=(isUpdate) ? user.getLastName() : ""%>"><br>
            </td>
        </tr>
        <tr>
            <td> 
                <label for="username">Username:</label>
                <input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus=""
                       value="<%=(isUpdate) ? user.getUsername() : ""%>"
                       <% if (isUpdate) {
                               out.print("readonly=\"readonly\"");
                           }%> ><br>
            </td>
            <td>
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Email Adress"
                       value="<%=(isUpdate) ? emp.getEmail() : ""%>"><br>
            </td>

            <%
                String pwdPlaceHolder = "*******";
                if (!isUpdate) {
                    pwdPlaceHolder = "Generated automatically.";
            %>
            <td>
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" class="form-control" autocomplete="off"  
                       placeholder="<%=pwdPlaceHolder%>" readonly="readonly" ><br>
            </td>
            <% } %>
        </tr>
        <tr>
            <td>
                <label for="role">Role:</label>
                <select id="role" name="role" class="form-control">
                    <% ArrayList<RoleBean> roles = rRoleRg.getAll(); %>
                    <% for (RoleBean role : roles) { %>
                    <option 
                        <% if (isUpdate && role.getId().equals(user.getRole())) {
                                out.print("selected=\"true\"");
                            }
                        %>
                        value="<%=role.getId()%>"><%=role.getName()%></option>
                    <% }%>
                </select><br>
            </td>
            <td>
                <label for="address">Job Title:</label>
                <input type="text" name="jobTitle" id="jobTitle" class="form-control" placeholder="jobTitle"
                       value="<%=(isUpdate) ? emp.getJobTitle() : ""%>"><br>
            </td>
            <td>
                <label for="department">Department:</label>
                <select id="department" name="department" class="form-control">
                    <%
                        EmployeeDepartmentBean empDep = null;
                        if (isUpdate) {
                            empDep = rEDRg.getActiveDeptOf(emp.getId());
                        }
                    %>
                    <% ArrayList<DepartmentBean> depts = rDepRg.getAll(); %>
                    <% for (DepartmentBean dept : depts) { %>
                    <option 
                        <% if (isUpdate && empDep != null && empDep.getDepartment().equals(dept.getId())) {
                                out.print("selected=\"true\"");
                            }
                        %>
                        value="<%=dept.getId()%>"><%=dept.getName()%></option>
                    <% }%>
                </select><br>
            </td>
        </tr>
        <tr>
            <td> 
                <label for="homeTel">Home Number:</label>
                <input type="text" name="homeTel" id="homeTel" class="form-control" placeholder="Home Number"
                       value="<%=(isUpdate) ? emp.getHomeTel() : ""%>"><br>
            </td>
            <td>
                <label for="mobileTel">Mobile Number:</label>
                <input type="text" name="mobileTel" id="mobileTel" class="form-control" placeholder="Mobile Number"
                       value="<%=(isUpdate) ? emp.getMobileTel() : ""%>"><br>
            </td>
            <td>
                <label for="address">Address:</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Address"
                       value="<%=(isUpdate) ? emp.getAddress() : ""%>"><br>
            </td>
        </tr>
        <tr>
            <td>
                <label for="zipCode">Zip Code:</label>
                <input type="text" name="zipCode" id="zipCode" class="form-control" placeholder="Zip Code"
                       value="<%=(isUpdate) ? emp.getZipCode() : ""%>"><br>
            </td>
            <td> 
                <label for="city">City:</label>
                <input type="text" name="city" id="city" class="form-control" placeholder="City"
                       value="<%=(isUpdate) ? emp.getCity() : ""%>"><br>
            </td>
            <td>
                <label for="country">Country:</label>
                <input type="text" name="country" id="country" class="form-control" placeholder="Country"
                       value="<%=(isUpdate) ? emp.getCountry() : ""%>"><br>
            </td>
        </tr>
        <tr>
            <td>
                <label for="birthdate">Birthday</label>
                <input type="text" name="birthdate" id="birthdate" class="form-control" placeholder="Birthday"
                       value="<%=(isUpdate) ? emp.birthDateToStr("MM/dd/yyyy") : ""%>"><br>
            </td>
            <td> 
                <label for="gender">Gender:</label>
                <select id="gender" name="gender" class="form-control">
                    <%
                        ArrayList<String> genders = new ArrayList<String>();
                        genders.add("Other");
                        genders.add("Male");
                        genders.add("Female");
                        for (int i = 0; i < genders.size(); i++) {
                    %>
                    <option
                        <%
                            if (isUpdate && emp.getGender().equals(i)) {
                                out.print("selected=\"true\"");
                            }
                        %>
                        value="<%= i%>"><%= genders.get(i)%></option>
                    <% }%>
                </select><br>
            </td>

        </tr>
    </table>
    <input type="number" name="isUpdate" id="isUpdate" hidden="true" value="<%=(isUpdate) ? 1 : 0%>">  
    <input type="number" name="userId" id="userId" hidden="true" value="<%=(isUpdate) ? user.getId() : 0%>">
    <button class="btn btn-primary" type="submit" ><%=(isUpdate) ? "Save" : "Register"%></button>
    <input class="reset-button" type="button" onclick="reset()" value="Reset">
</form>
<script>
    $(document).ready(function() {
        $('.register-form').validate({
            rules: {
                username: "required",
                firstName: "required",
                lastName: "required",
                email: {
                    required: true,
                    email: true
                },
                birthdate: {
                    required: true
                },
                homeTel: {
                    required: true,
                    number: true
                },
                mobileTel: {
                    number: true
                },
                department: "required",
                role: "required",
                jobTitle: "required",
                address: "required",
                city: "required",
                zipCode: {
                    required: true,
                    number: true
                },
                country: "required",
                gender: "required"
            },
            messages: {
                username: "*This field is required",
                firstName: "*This field is required",
                lastName: "*This field is required",
                email: {
                    required: "*This field is required",
                    email: "*Only valid emails are allowed"
                },
                birthdate: {
                    required: "*This field is required"
                },
                homeTel: {
                    required: "*This field is required",
                    number: "*Only numbers are allowed"
                },
                mobileTel: {
                    number: "*Only numbers are allowed"
                },
                role: "*This field is required",
                jobTitle: "*This field is required",
                address: "*This field is required",
                city: "*This field is required",
                zipCode: {
                    required: "*This field is required",
                    number: "*Only numbers are allowed"
                },
                country: "*This field is required",
                gender: "*This field is required"
            },
            submitHandler: function(form) {
                updateUser();
            }
        });
    });

    var genMsg; // Generated message from backend
    var success; // Return value generated from backend, 1 or 0
    function updateUser() {
        /* get some values from elements on the page: */
        var $form = $('#user-register');

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/user/repository",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }
</script>
