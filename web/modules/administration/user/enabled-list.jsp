<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.*" %>

<jsp:useBean id="roleEnLs" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<jsp:useBean id="userEnLs" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<jsp:useBean id="formEnLs" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler enabled_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (enabled_list_perms.hasPermission(formEnLs.getByTitle("User List"), "VIEW")) { %>

<h2>User List</h2>
<div id="result" style="display: none"></div>
<button id="show-disabled-btn" onclick="showDisabledUsers()" type="button" class="action-buttons btn btn-primary">Show disabled Users</button>
<table id="user-list" class="userlist table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
            <th>Role</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <%
            ArrayList<UserBean> userList = userEnLs.getActive();
            for (UserBean user : userList) {
        %>
        <tr id="row-<%=user.getId()%>">
            <td><%=user.getId()%></td>
            <td><%=user.getFirstName()%></td>
            <td><%=user.getLastName()%></td>
            <td><%=user.getUsername()%></td>
            <td><%=roleEnLs.getById(user.getRole()).getName()%></td>
            <td>
                <button id="view-btn" onclick="loadUserProfile(<%=user.getId()%>)" type="button" class="action-buttons btn btn-primary">View</button>
                <% if (enabled_list_perms.hasPermission(formEnLs.getByTitle("Register User"), "UPDATE")) { %>
                    <button id="edit-btn" onclick="editUserProfile(<%=user.getId()%>)"type="button" class="action-buttons btn btn-primary">Edit</button>
                <% } %>
                <% if (enabled_list_perms.hasPermission(formEnLs.getByTitle("User List"), "DISABLE")) { %>
                    <button id="disable-btn" onclick="<%=(user.getId().equals(((UserBean) session.getAttribute("sessionUser")).getId())) ? "disabled=true" : "askToDisable(" + user.getId() + ")"%>" type="button" class="action-buttons btn btn-disable">Disable</button>
                <% } %>     
            </td>
            
        </tr>
        <% }%>
    </tbody>
</table>  
<div id="disable-confirm" style="display:none">
    <p>When you disable an account, the employee using it won't be able to login anymore.</p>
    <p>Are you sure you wanna disable it?</p>
</div>

<script type="text/javascript" charset="utf-8">
                    //DataTable Javascript
                    $(document).ready(function() {
                        $('#user-list').dataTable({
                            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
                        });
                    });
</script>
    
<script>
    /** Loads the profile view into .right-container div, with specified id */
    function loadUserProfile(userId) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/profile.jsp?userId=" + userId);
    }
    
    /** Loads the register view into .right-container div, with specified id */
    function editUserProfile(userId) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/administration/user/register.jsp?userId=" + userId, {},
                function() {
                    $('#firstName').focus();
                });
    }

    //From Dialog ask to Disable a User.
    function askToDisable(userId) {
        $(function() {

            $("#disable-confirm").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Disable User": function() {
                        $(disableUser(userId)),
                                $(this).dialog("close");

                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Disable User');
        });
    }
    function disableUser(userId) {
        var genMsg; // Message that will be generated from the backend
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/user/change-status",
            // the data to send (will be converted to a query string)
            data: {id: userId, toStatus: 0},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $('#row-' + userId).toggle("fadeOut", function() {
                        $('#row-' + userId).remove();
                    });
                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).addClass("alert alert-success");
        } else {
            $(result).addClass("alert alert-danger");
        }
    }

    function showDisabledUsers() {
        console.log("showDisabledUsers()");
        $(".right-container").load("${pageContext.request.contextPath}/modules/administration/user/disabled-list.jsp");
    }
</script>

<% } %>
