<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.*" %>

<jsp:useBean id="roleDsLs" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<jsp:useBean id="usrDsLs" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<jsp:useBean id="formDnLs" class="org.locusdoc.businessLayer.FormRepository" scope="page" />


<% PermissionHandler disabled_list_perms = (PermissionHandler) session.getAttribute("permissions");%>

<% if (disabled_list_perms.hasPermission(formDnLs.getByTitle("Disabled Users"), "VIEW")) { %>
<script>
    /** Loads the profile view into div containing .right-container class */
    function loadUserProfile(userId) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/profile.jsp?userId=" + userId);
    }

    function editUserProfile(userId) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/administration/user/register.jsp?userId=" + userId, {},
                function() {
                    $('#firstName').focus();
                });
    }

    function enableUser(userId) {
        var genMsg; // Message that will be generated from the backend
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/user/change-status",
            // the data to send (will be converted to a query string)
            data: {id: userId, toStatus: 1},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $('#row-' + userId).toggle("highlight", function() {
                        $('#row-' + userId).remove();
                    });
                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).addClass("alert alert-success");
        } else {
            $(result).addClass("alert alert-danger");
        }
    }

    function showEnabledUsers() {
        console.log("showEnabledUsers()");
        $(".right-container").load("${pageContext.request.contextPath}/modules/administration/user/enabled-list.jsp");
    }
</script>
<h2>List of inactive Users</h2>
<div id="result" style="display: none"></div>
<button id="show-enabled-btn" onclick="showEnabledUsers()" type="button" class="action-buttons btn btn-primary">Show enabled Users</button>
<table id="user-list" class="userlist table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
            <th>Role</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <%
            ArrayList<UserBean> userList = usrDsLs.getInactive();
            for (UserBean user : userList) {
        %>
        <tr id="row-<%=user.getId()%>">
            <td><%=user.getId()%></td>
            <td><%=user.getFirstName()%></td>
            <td><%=user.getLastName()%></td>
            <td><%=user.getUsername()%></td>
            <td><%=roleDsLs.getById(user.getRole()).getName()%></td>
            <td>
                <button id="view-btn" onclick="loadUserProfile(<%=user.getId()%>)" type="button" class="action-buttons btn btn-primary">View</button>
                <% if (disabled_list_perms.hasPermission(formDnLs.getByTitle("Disabled Users"), "UPDATE")) { %>
                    <button id="edit-btn" onclick="editUserProfile(<%=user.getId()%>)"type="button" class="action-buttons btn btn-primary">Edit</button>
                <% } %>
                <% if (disabled_list_perms.hasPermission(formDnLs.getByTitle("Disabled Users"), "ENABLE")) { %>
                    <button id="disable-btn" onclick="enableUser(<%=user.getId()%>)" type="button" class="action-buttons btn btn-success">Enable</button>
                <% } %>    
            </td>
        </tr>
        <% }%>
    </tbody>
</table>
<!-- Editable,datatable javascript -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#user-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });
</script>

<% } else { 
    out.print("<h3>You don't have the right permissions to access this page.</h3>");
    out.close();
    } %>