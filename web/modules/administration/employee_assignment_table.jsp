<%@page import="org.locusdoc.businessLayer.ActionRepository"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentBean" %>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentRepository" %>
<%@page import="org.locusdoc.businessLayer.DepartmentBean" %>
<%@page import="org.locusdoc.businessLayer.DepartmentRepository" %>
<%@page import="org.locusdoc.businessLayer.UserBean" %>
<%@page import="org.locusdoc.businessLayer.UserRepository" %>

<jsp:useBean id="depEmAs" class="org.locusdoc.businessLayer.DepartmentRepository" scope="page" />
<jsp:useBean id="usrEmAs" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<jsp:useBean id="edEmAs" class="org.locusdoc.businessLayer.EmployeeDepartmentRepository" scope="page" />
<jsp:useBean id="formEmAs" class="org.locusdoc.businessLayer.FormRepository" scope="page" />

<% PermissionHandler emp_assig_lperms = (PermissionHandler) session.getAttribute("permissions");%>

<table class="assignment-list table table-hover">
    <thead>
        <tr>
            <th>Employee</th>
            <th>Department</th>
            <th>Begin Date</th>
            <th>End Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <% ArrayList<EmployeeDepartmentBean> empDepts = edEmAs.getAllNotDisabled(); %>
    <tbody>
        <% for (EmployeeDepartmentBean empDept : empDepts) {%>
        <tr>
            <%
                UserBean user = usrEmAs.getByEmployee(empDept.getEmployee());
            %>
            <td><%= user.getFirstName() + " " + user.getLastName()%></td>
            <td><%= depEmAs.getById(empDept.getDepartment()).getName()%></td>
            <td><%= empDept.getBeginDate()%></td>
            <td><%= (empDept.getEndDate() == null) ? "N/A" : empDept.getEndDate()%></td>
            <td>
                <% if (emp_assig_lperms.hasPermission(formEmAs.getByTitle("Employee Assignment"), "UPDATE")) { %>
                    <button id="edit-btn" onclick="newAssigment(<%=empDept.getId()%>)"type="button" class="action-buttons btn btn-primary">Edit</button>
                <% } %>
            </td>
        </tr>
        <% }%>
    </tbody>
</table>
