<%@include file="/modules/layout/header.jsp"%>
<script>
    $(document).ready(function() {
        var item = document.getElementById('help-link');
        $(item).attr("class", "active");
    });
</script>
<div id="content" class="container">
    <c:if test="${sessionScope.sessionUser.changedPass == 0}" >
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Hello ${sessionUser.firstName}, this seems to be your first time use. Please click <a href="#" class="alert-link">here</a> to change your generated password.
        </div>  
    </c:if>
    <div class="main-container">
        <div class="sidebar col-md-3">
            <!-- Dynamic left menu generation -->
        </div>
        <div class="right-container col-md-9">
            <!-- Dynamic Content for the main content (eg: documents,settings,profile etc) -->

            <!-- End of dynamic content for main content(right-container) -->
        </div> 
    </div>  
</div> 
<%@include file="/modules/layout/footer.jsp" %>
</div>
</body>
</html>