<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setHeader("Cache-Control", "no-cache");

//Forces caches to obtain a new copy of the page from the origin server
    response.setHeader("Cache-Control", "no-store");

//Directs caches not to store the page under any circumstance
    response.setDateHeader("Expires", 0);

//Causes the proxy cache to see the page as "stale"
    response.setHeader("Pragma", "no-cache");
%>
<c:if test="${sessionScope.sessionUser == null || sessionScope.sessionUser.getId() == 0}" >
    <c:set var="message" scope="request" value="You are not logged in. You have to log in to view this page."/>
    <c:redirect url="/index.jsp"/>
</c:if>
<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.ico">
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">        

        <title>Dashboard - Locus Document Management System</title>

        <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/js/jquery.dataTables.yadcf.js"></script>
        <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
        <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/js/jquery.navgoco.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <%@include file="/modules/layout/navigation/nav.jsp"%>