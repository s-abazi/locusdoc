<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>
<%@page import="org.locusdoc.businessLayer.PermissionBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.FormBean" %>
<%@page import="org.locusdoc.businessLayer.FormRepository" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${sessionScope.sessionUser == null || sessionScope.sessionUser.getId() == 0}" >
    <c:redirect url="/index.jsp"/>
</c:if>
<jsp:useBean id="repoFrm" class="org.locusdoc.businessLayer.FormRepository" scope="request"/>
<%
    ArrayList<FormBean> parentForm = null;
    ArrayList<FormBean> subForm = null;
%>
<ul class="nav navbar-nav">

    <% PermissionHandler left_nav_perms = (PermissionHandler) session.getAttribute("permissions");%>

    <!-- Documents main link -->
    <% if (left_nav_perms.hasPermission(repoFrm.getByNameId("doc-link"), "VIEW")) { %>
    <li class="menu-item dropdown" id="def-link">
        <a href="${pageContext.request.contextPath}/modules/documents/index.jsp" class="dropdown-toggle" data-toggle="dropdown"> Documents <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <%
                // Get all Parent forms with position set to 1, for Document
                parentForm = repoFrm.getByPosition(1, null);
                for (FormBean parent : parentForm) {
                    if (left_nav_perms.hasPermission(parent, "VIEW")) {
            %>
            <li>
                <a href="${pageContext.request.contextPath}/modules/documents/index.jsp?form=<%=parent.getId()%>">
                    <%=parent.getTitle()%></a>
            </li>
            <%}%>
            <%}%>
        </ul>        
    </li>
    <% } %>

    <!-- Administration main link -->
    <% if (left_nav_perms.hasPermission(repoFrm.getByNameId("admin-link"), "VIEW")) { %>
    <li class="menu-item dropdown" id="admin-link">
        <a href="${pageContext.request.contextPath}/modules/administration/index.jsp" class="dropdown-toggle" data-toggle="dropdown"> Administration <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <%
                // Get all Parent forms with position set to 2, for Administration
                parentForm = repoFrm.getByPosition(2, null);
                for (FormBean parent : parentForm) {
                    //Check if this user has permission for this parent form
                    if (left_nav_perms.hasPermission(parent, "VIEW")) {
            %>
            <li class="menu-item dropdown dropdown-submenu"><a href="#"><%=parent.getTitle()%></a>
                <ul class="dropdown-menu">
                    <%
                        // Get all sub forms for Definition parent forms
                        subForm = repoFrm.getByParent(parent.getId());
                        for (FormBean sub : subForm) {
                            // Check if this user has permission for this subform
                            if (left_nav_perms.hasPermission(sub, "VIEW")) {
                    %>
                    <li class="menu-item settings-navbar" id="<%=sub.getNameId()%>">
                        <a href="${pageContext.request.contextPath}/modules/administration/index.jsp?form=<%=repoFrm.getByTitle(sub.getTitle()).getId()%>"><%=sub.getTitle()%></a>   
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </li>
            <% } %>
            <% }%>

            <!-- Log drop down link -->
            <% FormBean logsFrm = repoFrm.getByTitle("Logs");%>
            <% if (left_nav_perms.hasPermission(logsFrm.getId(), "VIEW")) {%>
            <li class="menu-item" id="<%=logsFrm.getNameId()%>">
                <a href="${pageContext.request.contextPath}/modules/administration/index.jsp?form=<%=repoFrm.getByTitle(logsFrm.getTitle()).getId()%>"><%=logsFrm.getTitle()%></a>
            </li>
            <% } %>

        </ul>
    </li>
    <% } %>

    <!-- Definitions main link -->
    <% if (left_nav_perms.hasPermission(repoFrm.getByNameId("def-link"), "VIEW")) { %>
    <li class="menu-item dropdown" id="def-link">
        <a href="${pageContext.request.contextPath}/modules/definitions/index.jsp" class="dropdown-toggle" data-toggle="dropdown"> Definitions <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <%
                // Get all Parent forms with position set to 3, for Definitions
                parentForm = repoFrm.getByPosition(3, null);
                for (FormBean parent : parentForm) {
                    if (left_nav_perms.hasPermission(parent.getId(), 1)) {
            %>
            <li class="menu-item dropdown dropdown-submenu"><a href="#"><%=parent.getTitle()%></a>
                <ul class="dropdown-menu">
                    <%
                        // Get all child forms for Definition parent forms
                        subForm = repoFrm.getByParent(parent.getId());
                        for (FormBean sub : subForm) {
                            //  Check first if this user has permission
                            if (left_nav_perms.hasPermission(sub.getId(), 1)) {
                    %>
                    <li class="menu-item settings-navbar" id="<%=sub.getNameId()%>">
                        <a href="${pageContext.request.contextPath}/modules/definitions/index.jsp?form=<%=repoFrm.getByTitle(sub.getTitle()).getId()%>"><%=sub.getTitle()%></a>   
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </li>
            <%
                    }
                }
            %>
        </ul>
    </li>
    <% } %>

    <!-- Help main link -->
    <% if (left_nav_perms.hasPermission(repoFrm.getByNameId("help-link").getId(), 1)) { %>
    <li id="help-link"><a href="${pageContext.request.contextPath}/modules/help/index.jsp">Help</a></li>
        <% }%>

</ul>

