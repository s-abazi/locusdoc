<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="${pageContext.request.contextPath}/main.jsp"><img class="logo" src="${pageContext.request.contextPath}/images/logo-main.png" alt="LocusDOC logo"></a>
        </div>
        <div class="navbar-collapse collapse">
            <%@include file="left-nav.jsp" %>
            <%@include file="right-nav.jsp" %>
        </div>
    </div>
</div>