<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.*"%>

<c:if test="${sessionScope.sessionUser == null || sessionScope.sessionUser.getId() == 0}" >
    <c:redirect url="/index.jsp"/>
</c:if>

<jsp:useBean id="repoRl" class="org.locusdoc.businessLayer.RoleRepository" scope="request"/>
<jsp:useBean id="repoMsg" class="org.locusdoc.businessLayer.MessageRepository" scope="request"/>
<jsp:useBean id="rEmpRight" class="org.locusdoc.businessLayer.EmployeeRepository" scope="request"/>
<jsp:useBean id="rAlRight" class="org.locusdoc.businessLayer.AlertRepository" scope="request"/>
<jsp:useBean id="rFormRight" class="org.locusdoc.businessLayer.FormRepository" scope="request"/>
<jsp:useBean id="rUsrRight" class="org.locusdoc.businessLayer.UserRepository" scope="request"/>
<%@include file="/modules/documents/file-state-changes.jsp"%>
<% UserBean sessionUser = (UserBean) session.getAttribute("sessionUser"); %>
<ul class="nav navbar-nav navbar-right">

    <!-- Alerts -->
    <% generateFileNotifications(); %> <!-- Check existent Files and generate Alerts -->
    <% updateFiles(); %>
    <li class="dropdown bell">
        <a href="#1" class="dropdown-toggle" data-toggle="dropdown">
            <% ArrayList<AlertBean> alerts = rAlRight.getByUser(sessionUser.getId());%>
            <i class="glyphicon glyphicon-warning-sign"></i><span class="label label-menu label-message"><%=(alerts.size() != 0) ? alerts.size() : ""%></span>
        </a>  
        <ul class="right-nav dropdown-menu">
            <!-- Alert List -->
            <li class="header-notify">You have <%=alerts.size()%> alerts</li>
                <% if (alerts.size() > 0) {%>
                <% int countAlerts = 0; %>
                <% for (AlertBean alert : alerts) {%>
                <% if (countAlerts == 3) {
                        break;
                    }%>
            <li class="title"><!-- start alert -->
                <a href="${pageContext.request.contextPath}/modules/account/?form=<%=rFormRight.getByTitle("Alerts").getId()%>">
                    <p><b><%=alert.getTitle()%></b></p>
                </a>
            </li><!-- end alert -->
            <% countAlerts++; %>
            <% } %>
            <% }%>
            <li class="footer"><a href="${pageContext.request.contextPath}/modules/account/?form=<%=rFormRight.getByTitle("Alerts").getId()%>">
                    See All Alerts</a></li>
        </ul>
    </li>

    <!-- Messages -->
    <li class="dropdown massage">
        <% ArrayList<MessageBean> unreadMsgs = repoMsg.getByReceiver(sessionUser.getId(), 0);%>
        <a href="#2" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-envelope"></i><span class="label label-menu label-message"><%=(unreadMsgs.size() != 0) ? unreadMsgs.size() : ""%></span>
        </a>  
        <ul class="right-nav dropdown-menu">

            <!-- Message List -->
            <li class="header-notify">You have <%=unreadMsgs.size()%> messages</li>
                <% if (unreadMsgs.size() > 0) {%>
                <% int countMsgs = 0;%>
                <% for (MessageBean msg : unreadMsgs) {%>
                <% if (countMsgs == 5) {
                        break;
                    }%>
            <li class="title"><!-- start message -->
                <a href="${pageContext.request.contextPath}/modules/account/?form=<%=rFormRight.getByTitle("Inbox").getId()%>">
                    <% UserBean sender = rUsrRight.getById(msg.getSender());%>
                    <p><b><%=sender.getFirstName()%> <%=sender.getLastName()%></b><br />
                        <i><%=msg.getSubject()%></i></p>
                </a>
            </li><!-- end message -->
            <% countMsgs++; %>
            <% } %>
            <% }%>
            <li class="footer">
                <a href="${pageContext.request.contextPath}/modules/account/?form=<%=rFormRight.getByTitle("Inbox").getId()%>">
                    See All Messages</a></li>
        </ul>
    </li>

    <!-- User Profile -->
    <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i>
            <span>${sessionUser.firstName} ${sessionUser.lastName} <i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header bg-light-blue">

                <% // Get Employee using the EmployeeRepository stored in the application context
                    EmployeeBean empPrfPic = rEmpRight.getById(sessionUser.getEmployee());%>
                <img src="${pageContext.request.contextPath}/images/<%=empPrfPic.getProfilePic()%>"
                     class="img-circle" alt="User Image">
                <p>
                    ${sessionUser.firstName} ${sessionUser.lastName}
                    <% RoleBean sessUserRole = repoRl.getById(sessionUser.getRole());%>
                    <small><%=sessUserRole.getName()%> 2013-14</small>
                </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
                <div class="pull-left">
                    <a href="${pageContext.request.contextPath}/modules/account/" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                    <a href="${pageContext.request.contextPath}/logout.jsp" class="btn btn-default btn-flat">Sign out</a>
                </div>
            </li>
        </ul>
    </li>
</ul>
