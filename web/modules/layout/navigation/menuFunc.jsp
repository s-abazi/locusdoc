<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.FormBean"%>
<%@page import="org.locusdoc.utilities.ApplicationException"%>
<%@page import="org.locusdoc.businessLayer.FormRepository"%>
<%!
    FormRepository repo = new FormRepository();

    public String renderSubMenu(FormBean parent) throws ApplicationException {
        ArrayList<FormBean> subForm = null;
        subForm = repo.getByParent(parent.getId());
        StringBuilder sb = new StringBuilder("<li class=\"menu-item dropdown dropdown-submenu\"><a href=\"#\">");
        sb.append(parent.getTitle() + "</a>");
        sb.append("\n<ul class=\"dropdown-menu\">");
        for (FormBean sub : subForm) {
            sb.append("\n<li class=\"menu-item settings-navbar\"");
            // id="<%=sub.getNameId()
            sb.append("id=\"" + sub.getNameId() + "\" >");
            sb.append("\n<a href=\"" + getServletContext().getContextPath());
            sb.append("/" + parent.getPath() + "/index.jsp?form=");
            sb.append(repo.getByTitle(sub.getTitle()).getId() + "\">");
            sb.append(sub.getTitle());
            sb.append("</a>");
            sb.append("\n</li>");
        }
        sb.append("\n</ul>\n</li>");
        
        return sb.toString();
    }

    public String renderMenu(Integer formPos) {
        StringBuilder sb = new StringBuilder();
        ArrayList<FormBean> parentForms = null;

        try {
            parentForms = repo.getByPosition(formPos, null);
        } catch (ApplicationException ex) {
            ex.printStackTrace();
            return "Cannot generate form.";
        }
        for (FormBean parent : parentForms) {
            try {
                sb.append(renderSubMenu(parent));
            } catch (ApplicationException ex) {
                ex.printStackTrace();
                return "Cannot generate form.";
            }

        }
        return sb.toString();
    }

    public String createHeaderMenu() {
        StringBuilder sb = new StringBuilder();
        ArrayList<FormBean> mainForms = null;
        try {
            mainForms = repo.getByPosition(0, null);
        } catch (ApplicationException ex) {
            ex.printStackTrace();
            return "Cannot generate form.";
        }
        for(FormBean main : mainForms) {
            sb.append("\n\n<li class=\"menu-item dropdown\" ");
            sb.append(" id=\""+main.getNameId()+"\">");
            sb.append("\n<a href=\""+getServletContext().getContextPath());
            sb.append("/" + main.getPath()+"\"");
            sb.append(" class=\"dropdown-toggle\" data-toggle=\"dropdown\">");
            sb.append(main.getTitle());
            sb.append("<b class=\"caret\"></b></a>");
            sb.append("\n<ul class=\"dropdown-menu\">");
            if (main.getTitle().equals("Documents")) {
                sb.append(renderMenu(FormRepository.DOCUMENTS));
            } else if (main.getTitle().equals("Administration")) {
                sb.append(renderMenu(FormRepository.ADMINISTRATION));
            } else if (main.getTitle().equals("Definitions")) {
                sb.append(renderMenu(FormRepository.DEFINITIONS));
            }else if (main.getTitle().equals("Help")) {
            }
            sb.append("\n</ul>\n</li>"); 
        }
        return sb.toString();
    }
%>