<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.AlertBean"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="repoAlr" class="org.locusdoc.businessLayer.AlertRepository" scope="page" />

<h2>Alert List</h2>
<div id="result" style="display: none"></div>
<button type="button" id="delete-all-btn" onclick="askToDeleteAll('${sessionScope.sessionUser.id}')"  class="action-buttons btn btn-disable">Delete All</button>

<div id="alert-table">

</div>

<div id="register-form" style="display: none">  
    <!-- Dialog content is shown here -->
</div>
<div id="delete-confirm" style="display:none">
    <p>You are about to delete all your alerts!</p>
    <p>Are you sure you wanna delete them?</p>
</div>

<script>

    //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });

    // When document is loaded
    $(document).ready(function() {
        loadData();
    });

    // Construct datable on the table-list
    function loadData() {
        // Load table-list.jsp inside table element
        $.get('${pageContext.request.contextPath}/modules/account/alert-table.jsp', '', function(d) {
            $('#alert-table').html(d);
            initTable();
        });
    }

    // Create Datatable
    function initTable() {
        $('#alert-list').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>",
            "bDestroy": true // Destroy any old Datables
        });
    }

    //Ask to delete all alerts
    function askToDeleteAll(userId) {
        $(function() {

            $("#delete-confirm").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Delete All": function() {
                        $(deleteAll(userId)),
                                $(this).dialog("close");

                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Delete All Alerts');
        });
    }

    //Delete all alerts
    function deleteAll(userId) {
        var genMsg; // Message that will be generated from the backend
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/alert/delete-all",
            // the data to send (will be converted to a query string)
            data: {id: userId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $("#alert-list > tbody").toggle("fadeOut", function() {
                        $("#alert-list > tbody").html("");
                    });
                    loadData(); // Redraw table if the request finished successfully

                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    //Delete only one alert
    function deleteAlert(userId) {
        var genMsg; // Message that will be generated from the backend
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/alert/delete",
            // the data to send (will be converted to a query string)
            data: {id: userId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $('#row-' + userId).toggle("fadeOut", function() {
                        $('#row-' + userId).remove();
                    });
                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).addClass("alert alert-success");
        } else {
            $(result).addClass("alert alert-danger");
        }
    }

    function viewAlert(alertId) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/view-alert.jsp?alert=" + alertId);
    }
</script>
