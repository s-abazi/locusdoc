<%@page import="org.locusdoc.businessLayer.AlertBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<jsp:useBean id="rAlertView" class="org.locusdoc.businessLayer.AlertRepository" scope="page" />
<jsp:useBean id="rUsr" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<%
    // Store GET parameter and check if its valid
    String value = request.getParameter("alert");
    AlertBean alert = rAlertView.getById(Integer.parseInt(value));
%>
<div id="view-alert">
    <h2>View Alert</h2>
    <div class="alert-atributes">
        <p><b>Title:<%=alert.getTitle()%></b></p>
    </div>        
    <div class="msg-content">
        <p><b>Content:</b></p>
        <p class="well"><%=alert.getContent()%></p>
    </div>
</div>
<button type="button" onclick="alert()" class="action-buttons btn btn-primary">Back to Alerts</button>

<script>
    function alert() {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/alerts.jsp");
    }
</script>
