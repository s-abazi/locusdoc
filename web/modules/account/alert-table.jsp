<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.AlertBean"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="repoAlr" class="org.locusdoc.businessLayer.AlertRepository" scope="page" />

<table id="alert-list" class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Content</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <%
            UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
            ArrayList<AlertBean> alertList = repoAlr.getByUser(sessUser.getId());
            for (AlertBean alert : alertList) {
        %>
        <tr id="row-<%=alert.getId()%>">
            <td><%=alert.getId()%></td>
            <%
                String title = alert.getTitle().trim(); // Show only 30 characters
                if (title.length() > 30) {
                    title = title.substring(0, 30);
                    title += "...";
                }

            %>
            <td><%=title%></td>
            <%
                String content = alert.getContent().trim(); // Show only 30 characters
                if (content.length() > 60) {
                    content = content.substring(0, 60);
                    content += "...";
                }

            %>
            <td><%=content%></td>
            <td>
                <button type="button" id="view-btn" onclick="viewAlert(<%=alert.getId()%>)"  class="action-buttons btn btn-primary">View</button>
                <button type="button" id="delete-btn" onclick="deleteAlert(<%=alert.getId()%>)"  class="action-buttons btn btn-disable">Delete</button>
            </td>
        </tr>
        <% }%>
    </tbody>
</table>