<%@page import="org.locusdoc.businessLayer.MessageBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<jsp:useBean id="rMsgInbox" class="org.locusdoc.businessLayer.MessageRepository" scope="page" />
<jsp:useBean id="rUsrInbox" class="org.locusdoc.businessLayer.UserRepository" scope="page" />

<div id="msg-content">
    <h2>Inbox</h2>
    <button id="compose-new-msg" onclick="composeNewMessage()" type="button" class="action-buttons btn btn-primary">New Message</button>
    <table id="msg-inbox" class="msg-inbox-form table table-hover">
        <thead>
            <tr>
                <th>From</th>
                <th>Subject</th>
                <th>Date</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <%
                UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
                ArrayList<MessageBean> msgList = rMsgInbox.getByReceiver(sessUser.getId());
                for (MessageBean msg : msgList) {
            %>
            <tr class=<%=(msg.getRead() == 1)? "hasRead":"read"%>>
                <% UserBean sender = rUsrInbox.getById(msg.getSender());%>
                <td><%= sender.getFirstName() + " " + sender.getLastName()%></td>
                <td><%=msg.getSubject()%></td>
                <td><%=msg.getTimestamp()%></td>
                <td>
                    <button type="button" onclick="viewMsg(<%=msg.getId()%>, 'inbox')" class="action-buttons btn btn-primary">View</button>
                </td>
            </tr>
            <% }%>
        </tbody>
    </table>
</div>

<!-- Editable,datatable javascript -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.msg-inbox-form').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });

    function composeNewMessage() {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/compose.jsp");
    }

    function viewMsg(msgId, type) {
        msgId = encodeURIComponent(msgId);
        type = encodeURIComponent(type);
        params = "msgId=" + msgId + "&" + "type=" + type;
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/view.jsp?"+params); 
    }
</script>
