<%@page import="org.locusdoc.businessLayer.MessageBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<jsp:useBean id="rMsgInbox" class="org.locusdoc.businessLayer.MessageRepository" scope="page" />
<jsp:useBean id="rUsrInbox" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<%
    // Store GET parameter and check if its valid
    String value = request.getParameter("msgId");
    String type = request.getParameter("type");

    MessageBean msg = rMsgInbox.getById(Integer.parseInt(value));
    UserBean sender = rUsrInbox.getById(msg.getSender());
    msg.setRead(1);
    rMsgInbox.update(msg);
%>
<div id="view-msg">
    <h2>View Message</h2>
    <div class="msg-atributes">
        <p><b><%=(type.equals("sent")) ? "To:" : "From:"%></b></p>
        <p><b>Date:</b></p>
        <p><b>Subject:</b></p>
    </div>        
    <div class="msg-details">
        <p> <%= sender.getFirstName() + " " + sender.getLastName()%></p>
        <p><%=msg.getTimestamp()%></p>
        <p><%=msg.getSubject()%></p>
    </div>
    <div class="msg-content">
        <p><b>Content:</b></p>
        <p class="well"><%=msg.getContent()%></p>
    </div>
</div>
<button type="button" onclick="replyMessage(<%=sender.getId()%>, '<%=msg.getSubject()%>')" class="action-buttons btn btn-primary">Reply Message</button>
<button type="button" onclick="inbox()" class="action-buttons btn btn-primary">Back to Inbox</button>

<script>
    function replyMessage(userId, subject) {
        userId = encodeURIComponent(userId);
        subject = encodeURIComponent(subject);
        params = "userId=" + userId + "&" + "subject=" + subject;
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/compose.jsp?" + params);
    }

    function inbox() {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/inbox.jsp");
    }
</script>
