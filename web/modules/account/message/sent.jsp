<%@page import="org.locusdoc.businessLayer.MessageBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<jsp:useBean id="rMsgInbox" class="org.locusdoc.businessLayer.MessageRepository" scope="page" />
<jsp:useBean id="rUsrInbox" class="org.locusdoc.businessLayer.UserRepository" scope="page" />
<div id="msg-content">
    <h2>Sent Messages</h2>
    <table id="msg-sent" class="msg-sent-form table table-hover">
        <thead>
            <tr>
                <th>To</th>
                <th>Subject</th>
                <th>Date</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <%
                UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
                ArrayList<MessageBean> msgList = rMsgInbox.getBySender(sessUser.getId());
                for (MessageBean msg : msgList) {
            %>
            <tr <%=(msg.getRead() == 1)? "hasRead":"read"%>>
                <% UserBean receiver = rUsrInbox.getById(msg.getReceiver());%>
                <td><%= receiver.getFirstName() + " " + receiver.getLastName()%></td>
                <td><%=msg.getSubject()%></td>
                <td><%=msg.getTimestamp()%></td>
                <td>
                    <button type="button" onclick="viewMsg(<%=msg.getId()%>,'sent')" class="action-buttons btn btn-primary">View</button>
                </td>
            </tr>
            <% }%>

        </tbody>
    </table>
</div>
<!-- Editable,datatable javascript -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.msg-sent-form').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });
       function viewMsg(msgId,type) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/view.jsp?msgId=" + msgId+"&type="+type);
    }
</script>
