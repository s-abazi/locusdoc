<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="rUsrMsg" class="org.locusdoc.businessLayer.UserRepository" scope="request"/>
<%
String userIdStr = request.getParameter("userId");
String subject = request.getParameter("subject");

boolean isReply = false;
Integer userId = null;
if (userIdStr != null) {
    userId = Integer.parseInt(userIdStr);   
    if (userId > 0) {
        isReply = true;
    }
}
%>

<div id="result" style="display: none"></div>
<div id="msg-compose-form">
    <h2><%=(isReply)? "Reply Message" : "New Message"%></h2>   
    <form class="message-form" id="compose-msg-form" role="form" method="post">
        <label for="receiver">To:</label>
        <select id="receiver" name="receiver" class="msg-control" size="1" >

            <% ArrayList<UserBean> users = rUsrMsg.getActive(); %>
            <% for (UserBean user : users) {%>
            <option <%=(isReply && userId.equals(user.getId()))? "selected": ""%>
                value="<%=user.getId()%>"><%=user.getFirstName() + " " + user.getLastName()%>
            </option>
            <% }%>
        </select><br>
        <label for="subject">Subject:</label>
        <input type="text" name="subject" id="subject" class="<%=(isReply)? "msg-control reply-msg" : "msg-control"%>" autofocus="true" placeholder="Subject" value="<%=(isReply)? "RE: "+subject : ""%>"><br>
        <label for="content">Message:</label>
        <textarea type="text" id="content" rows="7" cols="50" class="msg-box form-control"  name="content" form="compose-msg-form" placeholder="Enter your message here"></textarea>
        </br>
        <button class="send-button btn btn-primary" type="submit">Send</button>
    </form> 
</div>
<script>
    
    $(document).ready(function() {
        $('#compose-msg-form').validate({
            rules: {
                subject: "required",
                content: "required"
            },
            messages: {
                subject: "*This field is required",
                content: "*This field is required"
            },
            submitHandler: function(form) {
                composeMsg();
            }
        });
        console.log("Validate");
    });

    var genMsg; // Generated message from backend
    var success; // Return value generated from backend, 1 or 0
    function composeMsg() {
        /* get some values from elements on the page: */
        var $form = $('#compose-msg-form');

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/message/compose",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).attr("class", "alert alert-success");
        } else {
            $(result).attr("class", "alert alert-danger");
        }
    }
</script>