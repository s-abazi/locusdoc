<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${sessionScope.sessionUser == null || sessionScope.sessionUser.getId() == 0}" >
    <c:set var="message" scope="request" value="You are not logged in. You have to log in to view this page."/>
    <c:redirect url="/index.jsp"/>
</c:if>
<!-- Navigation Buttons -->
<div class="sidebar col-md-3">
    <ul class="navgoco nav-pills nav-stacked" id="navTabs">
        <li class="message-navbar" id="compose">New Message</li>
        <li class="message-navbar" id="inbox">Inbox <span class="badge">94</span></li>
        <li class="message-navbar" id="sent">Sent Mail</li>      
    </ul>
</div>

<!-- Content -->
<div class="right-container col-md-9">  
    <%@include file="../../modules/message/compose.jsp"%>
</div>
<script type="text/javascript">
    //Navbar for settings
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });
</script>

<script>
    $(document).ready(function() {
        $("#compose").click(function() {
            $(".right-container").load("modules/message/compose.jsp");
        });
    });
    $(document).ready(function() {
        $("#sent").click(function() {
            $(".right-container").load("modules/message/sent.jsp");
        });
    });
    $(document).ready(function() {
        $("#inbox").click(function() {
            $(".right-container").load("modules/message/inbox.jsp");
        });
    });
</script>

