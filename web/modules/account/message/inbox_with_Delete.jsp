<%@page import="org.locusdoc.businessLayer.MessageBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<jsp:useBean id="rMsgInbox" class="org.locusdoc.businessLayer.MessageRepository" scope="page" />
<jsp:useBean id="rUsrInbox" class="org.locusdoc.businessLayer.UserRepository" scope="page" />

<script>
    //Ask to delete all messages
    function askToDeleteAll(userId) {
        $(function() {

            $("#delete-confirm").dialog({
                width: "auto",
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Delete All": function() {
                        $(deleteAll(userId)),
                                $(this).dialog("close");

                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Delete All Messages');
        });
    }
    //Delete all messages
    function deleteAll(userId) {
        var genMsg; // Message that will be generated from the backend
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/message/delete-all",
            // the data to send (will be converted to a query string)
            data: {id: userId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $('#row-' + userId).toggle("fadeOut", function() {
                        $('#row-' + userId).remove();
                    });
                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }
    
    //Delete only one message
    function deleteMsg(userId) {
        var genMsg; // Message that will be generated from the backend
        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/message/delete",
            // the data to send (will be converted to a query string)
            data: {id: userId},
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
                if (success == 1) {
                    $('#row-' + userId).toggle("fadeOut", function() {
                        $('#row-' + userId).remove();
                    });
                }
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).addClass("alert alert-success");
        } else {
            $(result).addClass("alert alert-danger");
        }
    }
</script>
<div id="msg-content">
    <h2>Inbox</h2>
    <div id="result" style="display: none"></div>
    <button id="compose-new-msg" onclick="composeNewMessage()" type="button" class="action-buttons btn btn-primary">New Message</button>
    <button type="button" id="delete-all-btn" onclick="askToDeleteAll('${sessionScope.sessionUser.id}')"  class="action-buttons btn btn-disable">Delete All</button>
    <table id="msg-inbox" class="msg-inbox-form table table-hover">
        <thead>
            <tr>
                <th>From</th>
                <th>Subject</th>
                <th>Date</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <%
                UserBean sessUser = (UserBean) session.getAttribute("sessionUser");
                ArrayList<MessageBean> msgList = rMsgInbox.getByReceiver(sessUser.getId());
                for (MessageBean msg : msgList) {
            %>
            <tr>
                <% UserBean sender = rUsrInbox.getById(msg.getSender());%>
                <td><%= sender.getFirstName() + " " + sender.getLastName()%></td>
                <td><%=msg.getSubject()%></td>
                <td><%=msg.getTimestamp()%></td>
                <td>
                    <button type="button" onclick="viewMsg(<%=msg.getId()%>, 'inbox')" class="action-buttons btn btn-primary">View</button>
                    <button type="button" id="delete-btn" onclick="deleteMsg(<%=msg.getId()%>)"  class="action-buttons btn btn-disable">Delete</button>
                </td>
            </tr>
            <% }%>
        </tbody>
    </table>
</div>
<div id="delete-confirm" style="display:none">
    <p>You are about to delete all your messages!</p>
    <p>Are you sure you wanna delete them?</p>
</div>
<!-- Editable,datatable javascript -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.msg-inbox-form').dataTable({
            "sDom": "<'row'<'span6'f>r>t<'row'<'span1'i><'span2'p>>"
        });
    });

    function composeNewMessage() {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/compose.jsp");
    }

    function viewMsg(msgId, type) {
        $(".right-container").load("${pageContext.request.contextPath}/modules/account/message/view.jsp?msgId=" + msgId + "&type=" + type);
    }
</script>
