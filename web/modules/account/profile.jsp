<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@page import="javax.servlet.http.HttpServletResponse"%>
<%@page import="org.locusdoc.businessLayer.DepartmentRepository"%>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentBean"%>
<%@page import="org.locusdoc.businessLayer.EmployeeDepartmentRepository"%>
<%@page import="org.locusdoc.businessLayer.EmployeeBean"%>
<%@page import="org.locusdoc.businessLayer.EmployeeRepository"%>
<%@page import="org.locusdoc.businessLayer.RoleBean"%>
<%@page import="org.locusdoc.businessLayer.RoleRepository"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page import="org.locusdoc.businessLayer.UserRepository"%>
<%@page import="org.locusdoc.businessLayer.PermissionHandler"%>


<jsp:useBean id="rRolePfl" class="org.locusdoc.businessLayer.RoleRepository" scope="page" />
<jsp:useBean id="rDepPfl" class="org.locusdoc.businessLayer.DepartmentRepository" scope="request"/>
<jsp:useBean id="rUsrPfl" class="org.locusdoc.businessLayer.UserRepository" scope="request"/>
<jsp:useBean id="rEmpPfl" class="org.locusdoc.businessLayer.EmployeeRepository" scope="request"/>
<jsp:useBean id="rEDPfl" class="org.locusdoc.businessLayer.EmployeeDepartmentRepository" scope="request"/>
<jsp:useBean id="rFormfl" class="org.locusdoc.businessLayer.FormRepository" scope="page" />


<%
    // If no valid id via HTTP GET was given, show the sessionUser profile
    UserBean user = (UserBean) (session.getAttribute("sessionUser"));
    try {
        if (request.getParameter("userId") != null) {
            // Try to fetch userfrom Database
            user = rUsrPfl.getById(Integer.parseInt(request.getParameter("userId")));
        }
        if (user == null) {
            // Use with specified Id does not exist
            response.sendRedirect("/LocusDMS/main.jsp");
            return;
        }
    } catch (Exception ex) {
        response.sendRedirect("/LocusDMS/main.jsp");
        return;
    }

    EmployeeBean emp = rEmpPfl.getById(user.getEmployee());
%>
<div id="result" style="display: none"></div>
<div class="profile">
    <div class="main-info">
        <img src="${pageContext.request.contextPath}/images/<%=emp.getProfilePic()%>" alt="Profile Pic">    
        <div class="usr-info">
            <h3><%=user.getFirstName() + " " + user.getLastName()%></h3>
            <p><b>Department:</b>
                <% EmployeeDepartmentBean edb = rEDPfl.getActiveDeptOf(emp.getId());%>
                <% edb.getDepartment();%>
                <%=rDepPfl.getById(edb.getDepartment()).getName()%>
            </p>
            <p><b>Job Title:</b> <%=emp.getJobTitle()%></p>
            <p><b>Role:</b> <%=rRolePfl.getById(user.getRole()).getName()%></p>
            <p><b>Username:</b> <%=user.getUsername()%></p>
        </div>             
    </div>
    <div class="personal-info">
        <h4>Personal Info</h4>
        <table>
            <tr>
                <td><b>Address:</b> <%=emp.getAddress()%></td>
                <td><b>City:</b> <%=emp.getCity()%></td> 
                <td><b>Country:</b> <%=emp.getCountry()%></td>
            </tr>
            <tr>
                <td><b>Zip Code:</b> <%=emp.getZipCode()%></td>
                <td><b>Gender:</b> <%=emp.genderToStr()%></td>
                <td><b>Birthdate:</b> <%=emp.getBirthdate()%></td>
            </tr>
        </table>
    </div>
    <div class="contact-info">
        <h4>Contact Info</h4>
        <table>
            <tr>
                <td><b>Email:</b> <%=emp.getEmail()%></td>
                <td><b>Tel Number:</b> <%=emp.getHomeTel()%></td> 
                <% String mobileTel = emp.getMobileTel();%>
                <td><b>Mobile Number:</b> <%=(mobileTel == null) ? "N/A" : mobileTel%></td> 
            </tr>
        </table>        
    </div>
    <div class="security-info">
        <h4>Security</h4>
        <table>
            <tr>
                <td><p id="edit-pw" onclick="showPasswordEditDialog()"> Change Password</p></td>  
            </tr>
        </table>
    </div>
            <button type="button" id="edit-btn" class="btn btn-primary" onclick="loadProfileEdit()" ><i  class="glyphicon glyphicon-edit"></i> Edit User Details</button>
    <div id="edit-pwd"  style="display: none">
        <form class="change-password-form" action="${pageContext.request.contextPath}/user/password-change" id="edit-passwd-form" role="form" method="post">
            <table class="table" >
                <tr>
                    <td>
                        <label for="currentPassword">Current Password:</label>
                        <input type="password" name="currentPassword" id="currentPassword" class="form-control" autocomplete="off"><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="newPassword">New password:</label>
                        <input type="password" name="newPassword" id="newPassword" class="form-control" autocomplete="off"><br>
                    </td>
                <tr>
                <tr>
                    <td>
                        <label for="conformNewPassword">Confirm new password:</label>
                        <input type="password" name="confirmNewPassword" id="confirmNewPassword" class="form-control" autocomplete="off"><br>
                    </td>
                <tr> 
                <input type="hidden" name="id" value="<%=user.getId()%>">
            </table>
        </form>
    </div>
</div>
<script>

</script>
<script>
    function showPasswordEditDialog() {
        var form = $("#edit-passwd-form");
        $(function() {
            // Open dialog for password change
            $("#edit-pwd").dialog({
                dialogClass: 'dialog-custom',
                show: {
                    effect: "fadeIn",
                    duration: 250
                },
                hide: {
                    effect: "fadeOut",
                    duration: 250
                },
                buttons: {
                    "Change Password": function() {

                        if (form.valid()) {
                            changePass();
                            $(this).dialog("destroy")
                        }
                    },
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("span.ui-dialog-title").text('Change Password');
        });
    }

    $(document).ready(function() {
        $('#edit-passwd-form').validate({
            rules: {
                currentPassword: "required",
                newPassword: "required",
                confirmNewPassword: {
                    equalTo: "#newPassword"
                }
            },
            messages: {
                currentPassword: "*This field is required",
                newPassword: "*This field is required",
                confirmNewPassword: "*Enter the same new password for confirmation"
            },
            submitHandler: function(form) {
                changePass();
            }
        });
    });

    function changePass() {
        /* get some values from elements on the page: */
        var $form = $('#edit-passwd-form');

        $.ajax({
            // the URL for the request
            url: "${pageContext.request.contextPath}/user/password-change",
            // the data to send (will be converted to a query string)
            data: $form.serialize(),
            // whether this is a POST or GET request
            type: "POST",
            // the type of data we expect back
            dataType: "xml",
            // code to run if the request succeeds;
            // the response is passed to the function
            success: function(xml) {

                $(xml).find("result").each(function() //To traverese result tree
                {
                    genMsg = $(this).find("message").text(); // Get value as text of child message
                    success = $(this).find("success").text();// Get value as text of child affectedRows
                });
                renderHtml(genMsg, success);
            },
            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function(xhr, status, errorThrown) {
                genMsg = "Sorry, there was a problem! Please, try again.";
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
                renderHtml(genMsg, 0);
            },
            // code to run regardless of success or failure
            complete: function(xhr, status) {
                //console.log("The request is complete!");
            }
        });
    }

    function renderHtml(msg, success) {
        var result = document.getElementById('result');
        result.innerHTML = msg;
        $(result).css("display", "block");
        if (success == 1) {
            $(result).addClass("alert alert-success");
        } else {
            $(result).addClass("alert alert-danger");
        }
    }

    function loadProfileEdit() {
        var container = $(".right-container");
        container.load("${pageContext.request.contextPath}/modules/administration/user/register.jsp"
                + "?userId=<%=user.getId()%>");
        $('#firstName').focus();
    }
</script>