<%@page import="org.locusdoc.businessLayer.FormBean" %>
<%@page import="org.locusdoc.businessLayer.FormRepository" %>
<%@include file="/modules/layout/header.jsp"%>
<jsp:useBean id="repoForm" class="org.locusdoc.businessLayer.FormRepository" scope="request"/>

<%    // Store GET parameter and check if its valid
    FormBean getForm = null;
    String value = request.getParameter("form");
    Integer formId = (value != null) ? Integer.parseInt(value) : 0;
    if (formId > 0) {
        // Instatiate form by id
        getForm = repoForm.getById(formId);
    }
%>

<div id="content" class="container">
    <div class="main-container">
        <div class="sidebar col-md-3">
            <!-- Dynamic left menu generation -->

            <div class="navbar-header">
                <a class="navbar-second-toggle" data-toggle="collapse" data-target=".navbar-second-collapse">
                    Sidebar Menu <b class="caret"></b>
                </a>
            </div>
            <div class="navbar-second-collapse collapse">
                <ul class="navgoco nav-pills nav-stacked " id="navTabs">
                    <%
                        ArrayList<FormBean> parentForms = repoForm.getByPosition(4, null);
                        for (FormBean parent : parentForms) {
                    %>
                    <li>
                        <a href="#"><%=parent.getTitle()%> <b class="caret"></b></a>
                        <ul class="dropdown-settings">
                            <%
                                ArrayList<FormBean> subForms = repoForm.getByParent(parent.getId());
                                for (FormBean sub : subForms) {
                            %>
                            <li class="settings-navbar" id="<%=sub.getNameId()%>" 
                                onclick="loadForm('<%=sub.getPath()%>')">
                                <%=sub.getTitle()%>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                    <%}%>
                    <% FormBean alertForm = repoForm.getByTitle("Alerts");%>
                    <li class="single-navbar" id="<%=alertForm.getNameId()%>" 
                        onclick="loadForm('<%=alertForm.getPath()%>')">
                        <%=alertForm.getTitle()%>
                    </li>

                </ul>
            </div>

        </div>
        <div class="right-container col-md-9">
            <!-- Dynamic Content for the main content (e.g: documents,settings,profile etc) -->
            <!-- If we have a form from GET don't load profile for performance -->
            <% if (getForm == null) {%>
            <jsp:include page="/modules/account/profile.jsp"/>
            <% }%>
            <!-- End of dynamic content for main content(right-container) -->
        </div> 
    </div>  
</div>
<%@include file="/modules/layout/footer.jsp" %>
</div>
<script type="text/javascript">
    //Navbar in the sidebar
    $(document).ready(function() {
        $('.navgoco').navgoco();
    });
    
    // Loads given path into right-container class
    function loadForm(path) {
        if (path === "modules/administration/user/register.jsp") {
            path = "modules/administration/user/register.jsp" + "?userId=${sessionScope.sessionUser.id}";
        }
        $(".right-container").load("${pageContext.request.contextPath}/" + path);
    }
    // If we have a form in GET, load it into right container class using loadForm(path)
    <% if (getForm != null) {%>
    loadForm("<%=getForm.getPath()%>");
    <% }%>
</script>
</body>
</html>