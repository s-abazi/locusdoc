<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.locusdoc.businessLayer.UserBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.ico">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/login.css" rel="stylesheet">

        <title>Login - LocusDoc Document Management System</title>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.validate.js"></script>
        <script src="js/additional-methods.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css"></style></head>

    <body>

        <c:if test="${sessionScope.sessionUser != null && sessionScope.sessionUser.getId() > 0}" >
            <c:redirect url="/main.jsp"/>
        </c:if>
        <div class="container">
            <form class="login-form" role="form" action="login" method="post">
                <img class="main-logo" alt="LocusDoc Logo" src="images/logo.png"> 
                <input type="text" name="username" class="form-control" placeholder="Username" autofocus=""><br>
                <input type="password" name="password" class="form-control" placeholder="Password" >
                <label class="checkbox">
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
                <c:if test="${not empty msg}">
                    <div class="login-alert alert-danger">
                        <p>${msg}</p>
                    </div>
                </c:if>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

        </div> 

        <script>
            $(document).ready(function() {
                $('.login-form').validate({
                    rules: {
                        username: "required",
                        password: "required"
                    },
                    messages: {
                        username: "*Please enter your username",
                        password: "*Please enter your password"
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </body>

</html>