<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
    </head>
    <body>
        <c:if test="${sessionScope.sessionUser != null }" >
            <c:remove var="sessionUser"/>
            <% session.invalidate(); %>
        </c:if>
        <c:redirect url="/index.jsp"/>
    </body>
</html>
