package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.FileBean;
import org.locusdoc.businessLayer.FileCategoryBean;
import org.locusdoc.businessLayer.FileCategoryRepository;
import org.locusdoc.businessLayer.FileRepository;
import org.locusdoc.businessLayer.StateBean;
import org.locusdoc.businessLayer.StateRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.UserRepository;
import org.locusdoc.businessLayer.UserStateApproverBean;
import org.locusdoc.businessLayer.UserStateApproverRepository;
import org.locusdoc.businessLayer.WorkflowBean;
import org.locusdoc.businessLayer.WorkflowHistoryBean;
import org.locusdoc.businessLayer.WorkflowHistoryRepository;
import org.locusdoc.utilities.ApplicationException;

@WebServlet(name = "ApproveDocument", urlPatterns = {"/document/approve"})
public class ApproveDocument extends HttpServlet {
    private static final Logger logger = Logger.getLogger(LoginServlet.class.getName());
    private UserBean sessionUser;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ApproveDocument</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ApproveDocument at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            this.sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser != null && sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        String fileIdStr = request.getParameter("fileId");
        String stateIdStr = request.getParameter("stateId");
        if (fileIdStr == null || stateIdStr == null) {
            renderXmlResponse(response, "To compelte this request you must send the file id and state id as POST parameters.", 0);
        }
        Integer fileId = null;
        Integer stateId = null;
        try {
            fileId = Integer.parseInt(fileIdStr);
            stateId = Integer.parseInt(stateIdStr);
        } catch (NumberFormatException ex) {
            logger.log(Level.WARNING, String.format("Value of fileId or stateId is NAN (fileId=%s, stateId=%s)", fileIdStr, stateIdStr));
            renderXmlResponse(response, "POST parameters need to be numbers only.", 0);
        }
        
        // Fetch State and File from DB
        StateRepository stateRepo = new StateRepository();
        StateBean oldState = null;
        FileRepository fileRepo = new FileRepository();
        FileBean file = null;
        try {
            oldState = stateRepo.getById(stateId);
            file = fileRepo.getById(fileId);
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when trying to fetch old state and file."));
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        
        FileCategoryRepository categoryrepo = new FileCategoryRepository();
        FileCategoryBean category = null;
        try {
            category = categoryrepo.getById(file.getRequestCategory());
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when trying to fetch fileCategory of file, id ofcategory is [%s].", file.getRequestCategory()));
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        try {
           if (!userCanApprove(category, oldState)) {
               renderXmlResponse(response, "You cannot approve the document in its current state.", 0);
            } 
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when trying to check if user can approve ths document."));
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        
        
        WorkflowHistoryRepository fileFlowRepo = new WorkflowHistoryRepository();
        WorkflowHistoryBean fileFlow = null;
        try { // Get Workflow
            fileFlow = fileFlowRepo.getByFileAndState(file.getId(), oldState.getId());
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when trying to fetch specific flow [%s].", file.getRequestCategory()));
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        // Upadte workflow
        try {
            fileFlow.setStatus(1);
            fileFlowRepo.update(fileFlow);
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when trying to update specific flow [%s].", fileFlow.getId()));
            renderXmlResponse(response, ex.getMessage(), 0);
        }

        renderXmlResponse(response, "The current state of the file was approved successfully. "
                + "The file is now fully approved or forwarded to the next state, depending on it's workflow.",1);
    }
    
    private boolean userCanApprove(FileCategoryBean category, StateBean state) throws ApplicationException {
        UserStateApproverRepository approversRepo = new UserStateApproverRepository();
        UserStateApproverBean approver = null;
        
        try {
            approver = approversRepo.getByPK(sessionUser.getId(), category.getId(), state.getId());
        } catch (ApplicationException ex) {
            throw ex;
        }
        return approver != null;
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet to approve a state in the specific flow for the document";
    }

}
