package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.SharingBean;
import org.locusdoc.businessLayer.SharingRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.utilities.ApplicationException;

@WebServlet(name = "ShareFile", urlPatterns = {"/document/share-file"})
public class ShareFile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShareFile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShareFile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        SharingRepository sharingRepo = new SharingRepository();
        SharingBean sharing = null;
        

        String msg = null; // Message that will be generated for the Front End
        Integer userId = Integer.parseInt(request.getParameter("user-shared"));
        Integer fileId = Integer.parseInt(request.getParameter("fileId"));
        sharing = new SharingBean(userId,fileId,1,0,0);
        int affRows = 0;
        try {
            affRows = sharingRepo.insert(sharing);
        } catch (ApplicationException ex) {
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        if (affRows == 0) {
            renderXmlResponse(response, "There was a problem sharing a file with selected User ", affRows);
        }
        
        renderXmlResponse(response, "File was shared successfully to User.",1);
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
