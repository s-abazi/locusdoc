package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.RoleBean;
import org.locusdoc.businessLayer.RoleRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
@WebServlet(name = "RoleServlet", urlPatterns = {"/role/repository"})
public class RoleServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RoleServlet</title>");
            out.println("</head>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        return;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        RoleRepository roleRep = new RoleRepository();
        RoleBean role = null;

        String msg = null; // Message that will be generated for the Front End
        int upd = Integer.parseInt(request.getParameter("isUpdate"));
        boolean isUpdate = (upd == 1);

        if (isUpdate) {
            try {
                role = roleRep.getById(Integer.parseInt(request.getParameter("roleId")));
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (role == null) {
                renderXmlResponse(response, "Role with the specified id does not exist.", 0);
            }
            role.setName(request.getParameter("name"));
            role.setDescription(request.getParameter("description"));
            try {
                roleRep.update(role);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
        } else {
            role = new RoleBean(request.getParameter("name"), request.getParameter("description"));
            int genId = 0;
            try {
                genId = roleRep.insert(role);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (genId == 0) {
                renderXmlResponse(response, "There was an error inserting the new role.", 0);
            }
        }

        if (isUpdate) {
            msg = "Role was updated successfully.";
        } else {
            msg = "New role was created successfully.";
        }
        renderXmlResponse(response, msg, 1);
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Department registration servlet.";
    }

}
