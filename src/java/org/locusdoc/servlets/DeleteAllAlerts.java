package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.AlertRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.utilities.ApplicationException;

@WebServlet(name = "deleteAllAlerts", urlPatterns = {"/alert/delete-all"})
public class DeleteAllAlerts extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteAllAlerts</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteAllAlerts at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        processRequest(request, response);
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        int affRows = 0;
        String msg = null;
        Integer userId = Integer.parseInt(request.getParameter("id"));
        AlertRepository repoAlert = new AlertRepository();
        response.setHeader("Cache-Control", "no-cache");

        try {
            affRows = repoAlert.deleteAll(userId);
        } catch (ApplicationException ex) {
            msg = ex.getMessage();
            renderXmlResponse(response, msg, 0);
        }

        if (affRows == 0) {
            msg = "No alerts were deleted.";
        } else {
            if (affRows == 1) {
                msg = affRows + " alert was deleted.";
            } else if (affRows > 1) {
                msg = affRows + " alerts were deleted.";
            }
            affRows = 1;
        }
        renderXmlResponse(response, msg, affRows);

    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
