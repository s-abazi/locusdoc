/*
 * Servlet for Document uploading and storage
 */
package org.locusdoc.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.locusdoc.businessLayer.AlertBean;
import org.locusdoc.businessLayer.AlertRepository;
import org.locusdoc.businessLayer.FileBean;
import org.locusdoc.businessLayer.FileCategoryBean;
import org.locusdoc.businessLayer.FileCategoryRepository;
import org.locusdoc.businessLayer.FileRepository;
import org.locusdoc.businessLayer.StateBean;
import org.locusdoc.businessLayer.StateRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.UserRepository;
import org.locusdoc.businessLayer.UserStateApproverBean;
import org.locusdoc.businessLayer.UserStateApproverRepository;
import org.locusdoc.businessLayer.WorkflowBean;
import org.locusdoc.businessLayer.WorkflowHistoryBean;
import org.locusdoc.businessLayer.WorkflowHistoryRepository;
import org.locusdoc.businessLayer.WorkflowRepository;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 * Servlet for Document uploading and storage
 */
@WebServlet(name = "UploadHandler", urlPatterns = {"/documents/create"})
public class UploadHandler extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LoginServlet.class.getName());
    private HttpServletRequest request;
    private HttpServletResponse response;
    private Integer userId;
    private Integer categoryId;
    private String fileName;
    private Integer formId;
    private File file;
    private UserBean sessionUser;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UploadHandler</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UploadHandler at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private File uploadDocument(HttpServletRequest request, String fullPath) throws ApplicationException {
        logger.log(Level.INFO, String.format("Entered uploadDocument method."));
        final String DESTINATION = fullPath;
        File file = null;
        //process only if its multipart content
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);

                for (FileItem item : multiparts) {
                    // Process the normal form fields
                    if (item.isFormField()) {
                        String name = item.getFieldName();
                        if (name.equals("user")) {
                            this.userId = Integer.parseInt(item.getString());
                            logger.log(Level.INFO, String.format("Parsing value of 'user' field."));
                        } else if (name.equals("category")) {
                            this.categoryId = Integer.parseInt(item.getString());
                            logger.log(Level.INFO, String.format("Parsing value of 'category' field."));
                        } else if (name.equals("doc-name")) {
                            this.fileName = item.getString();
                            logger.log(Level.INFO, String.format("Parsing value of 'doc-name' field."));
                        } else if (name.equals("srcFormId")) {
                            this.formId = Integer.parseInt(item.getString());
                            logger.log(Level.INFO, String.format("Parsing value of 'srcFormId' field."));
                        }
                    }
                    // Process the File
                    if (!item.isFormField()) {
                        // Replace whitespace with one underscore
                        this.fileName = this.fileName.replaceAll("\\s+", "_");
                        int dot = item.getName().lastIndexOf(".");
                        String ext = item.getName().substring(dot);
                        this.fileName = this.fileName + ext;
                        // Create File reference
                        file = new File(DESTINATION + File.separator + this.fileName);
                        item.write(file);
                    }
                }

                //File uploaded successfully
                String msg = " \nFilename:\t\t" + file.getName();
                msg += " \nFullPath:\t\t" + file.getCanonicalPath();
                double sizeInBytes = file.length();
                msg += " \nFile Size:\t\t" + Math.round(sizeInBytes / 1024) + " KBytes.";
                logger.log(Level.INFO, String.format("\nUploaded file details:\n %s", msg));
            } catch (Exception ex) {
                throw new ApplicationException("File upload failed due to: " + ex.getMessage());
            }

        } else {
            throw new ApplicationException("Sorry this Servlet only handles file upload request.");
        }
        return file; // Return the reference to the newly uploaded file
    }

    private UserBean fetchUser() throws IOException {
        logger.log(Level.INFO, String.format("Entered fetchUser method."));
        UserBean user = null;
        UserRepository usrRepo = new UserRepository();
        try {
            user = usrRepo.getById(this.userId);
        } catch (ApplicationException ex) {
            sendResponse(ex.getMessage(), 0);
        }
        if (user == null) {
            sendResponse("User doesn't exist.", 0);
        }
        return user;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Check if user is logged in first 
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }

        this.response = response;
        this.request = request;

        Map settings = (HashMap) request.getSession(false).getAttribute("settings");
        // The documents path where all the FileCategory directories are
        String rootPath = (String) settings.get("doc_path");
        logger.log(Level.INFO, String.format("The root documents directory fetched from the Settings table is \"%s\".", rootPath));
        String tempPath = rootPath + File.separator + "temp"; // Temporary directory
        File fileRef = null;
        try {
            fileRef = uploadDocument(request, tempPath); // Upload file and return reference to that File
        } catch (ApplicationException ex) {
            logger.log(Level.SEVERE, String.format("File upload failed. Error thrown is %s.", ex.getMessage()), ex);
            sendResponse(ex.getMessage(), 0);
        }
        this.sessionUser = fetchUser();

        FileCategoryRepository catRepo = new FileCategoryRepository();
        Integer catId = this.categoryId;
        FileCategoryBean category = null;
        try {
            category = catRepo.getById(catId);
        } catch (ApplicationException ex) {
            sendResponse(ex.getMessage(), 0);
        }
        if (category == null) { // When category doesn't exist, throw an error because we cannot continue
            logger.log(Level.INFO, String.format("The returned FileCategory is null. Specified id is \"%s\".", catId));
            sendResponse("FileCategory with specified name does not exist.", 0);
        }

        File directory = null; // The directory where the File will be uplaoded, depends on the FileCategory

        // If its Pernsonal file i.e. FileCategory is "Default"
        if (category.getName().equals("Default")) {
            // User directory will be created inside ../users
            String pathToUserDirs = rootPath + File.separator + "users";
            logger.log(Level.INFO, String.format("Path of directory with the user directory is \"%s\".", pathToUserDirs));
            directory = createPersonalDir(sessionUser, pathToUserDirs); // Creates directory, but if it exists nothing changes

        } else { // If file is with Workflow
            String pathToCategories = rootPath + File.separator + "file-categories";
            logger.log(Level.INFO, String.format("Path of directory with the FielCategories is \"%s\".", pathToCategories));
            directory = createCategoryDir(category, pathToCategories);
        }
        //Create FileBean for uploaded file
        FileBean file = new FileBean(fileName, category.getId(), sessionUser.getId(), fileRef.getCanonicalPath(), "text", (int) fileRef.length(), null, 0, 1);
        file = insertFile(file); // Insert File into database
        logger.log(Level.INFO, String.format("\nGenerated FileBean is %s.\n", file));
        // Move file from/temp to the right directory
        if (fileRef.renameTo(new File(directory.getCanonicalPath() + File.separator + file.getId()))) {
            logger.log(Level.INFO, String.format("File was moved successfully to \"%s\".", directory.getCanonicalPath()));
        } else {
            logger.log(Level.INFO, String.format("Failed to move file to\"%s\".", directory.getCanonicalPath()));
        }
        this.file = fileRef;
        file.setFilePath(directory.getCanonicalPath() + File.separator + file.getId());
        file = updateFile(file);

        // Workflow of this specific file, based on the category workflow
        ArrayList<WorkflowHistoryBean> fileFlow = fetchFileFlow(file.getId(), category.getId());
        fileFlow = insertFlowIntoDB(fileFlow); // Insert Worflow of File into DB and return the list with ids set
        logger.log(Level.INFO, String.format("\nGenerated Workflow for File is: %s.", fileFlow));
        //createAlerts(fileFlow, category);
        sendResponse("File was uploaded successfully.", 1);
    }

    private FileBean insertFile(FileBean file) throws IOException {
        logger.log(Level.INFO, String.format(""));
        FileRepository fileRepo = new FileRepository();
        int id = 0;
        try {
            id = fileRepo.insert(file);
        } catch (ApplicationException ex) {
            sendResponse(ex.getMessage(), 0);
        }
        file.setId(id);
        return file;
    }

    private FileBean updateFile(FileBean file) throws IOException {
        logger.log(Level.INFO, String.format(""));
        FileRepository fileRepo = new FileRepository();
        try {
            fileRepo.update(file);
        } catch (ApplicationException ex) {
            sendResponse(ex.getMessage(), 0);
        }
        return file;
    }

    private ArrayList<WorkflowHistoryBean> fetchFileFlow(Integer fileId, Integer categoryId) throws IOException {
        logger.log(Level.INFO, String.format(""));
        ArrayList<WorkflowHistoryBean> fileFlow = new ArrayList<WorkflowHistoryBean>(); // Workflow of this specific file, based on the category workflow
        ArrayList<WorkflowBean> categoryFlow = null; // Workflow of the fileCategory
        WorkflowRepository catFlowRepo = new WorkflowRepository();
        try {
            categoryFlow = catFlowRepo.getByFileCategory(categoryId);
        } catch (ApplicationException ex) {
            logger.log(Level.SEVERE, String.format("Workflow of FileCategory coudn't be fetched from database."), ex);
            sendResponse(ex.getMessage(), 0);
        }
        for (WorkflowBean flow : categoryFlow) {
            fileFlow.add(new WorkflowHistoryBean(fileId, flow.getState(), flow.getNextState(), flow.getStatus()));
        }
        return fileFlow;
    }

    private ArrayList<WorkflowHistoryBean> insertFlowIntoDB(ArrayList<WorkflowHistoryBean> fileFlow) throws IOException {
        logger.log(Level.INFO, String.format(""));
        WorkflowHistoryRepository fileFlowRepo = new WorkflowHistoryRepository();
        for (WorkflowHistoryBean flow : fileFlow) {
            Integer flowId = 0;
            try {
                flowId = fileFlowRepo.insert(flow);
            } catch (ApplicationException ex) {
                sendResponse(ex.getMessage(), 0);
            }
            flow.setId(flowId); // Set generated Id 
        }
        return fileFlow;

    }

    private void createAlerts(ArrayList<WorkflowHistoryBean> fileFlow, FileCategoryBean category) throws IOException {
        logger.log(Level.INFO, String.format(""));
        Integer stateId = 0;
        StateBean state = null; // State which we will notify approvers for
        for (WorkflowHistoryBean flow : fileFlow) {
            Integer status = flow.getStatus();
            if (status == 0) {
                stateId = flow.getState();
                break; // We've got the first state that isnt approved, so get out!
            }
        }
        if (stateId == 0) {
            return; // No unapproved state, so get out!
        }
        StateRepository stateRepo = new StateRepository();
        try {
            state = stateRepo.getById(stateId);
        } catch (ApplicationException ex) {
            sendResponse(ex.getMessage(), 0);
        }
        if (state == null) {
            sendResponse("State doesnt exist.", 0);
        }
        logger.log(Level.INFO, String.format("First state that is not passed is: %s", state));

        UserStateApproverRepository approverRepo = new UserStateApproverRepository();
        ArrayList<UserStateApproverBean> approvers = null;
        try {
            approvers = approverRepo.getByStateAndCategory(state.getId(), category.getId());
        } catch (ApplicationException ex) {
            sendResponse(ex.getMessage(), 0);
        }
        logger.log(Level.INFO, String.format("Number approvers in '%s' for state '%s' is %s.", category.getName(), state.getDescription(), approvers.size()));
        // Now create all the alerts
        ArrayList<AlertBean> alerts = new ArrayList<AlertBean>(approvers.size());
        for (UserStateApproverBean approver : approvers) {
            String title = "File [" + file.getName() + "] needs status change";
            String content = "File [" + file.getName() + "] that is owned by [" + this.sessionUser.getId() + "] "
                    + this.sessionUser.getFirstName() + " " + this.sessionUser.getLastName() + ", needs your attention.";
            alerts.add(new AlertBean(title, content, approver.getUser()));
        }
        logger.log(Level.INFO, String.format("Created alerts. Nubmer of alerts is %s.", alerts.size()));

        // Add all alerts into database
        AlertRepository alRepo = new AlertRepository();
        for (AlertBean alert : alerts) {
            try {
                alRepo.insert(alert);
            } catch (ApplicationException ex) {
                sendResponse(ex.getMessage(), 0);
            }
        }
        logger.log(Level.INFO, String.format("Alerts added to database.", state));

    }

    private File createPersonalDir(UserBean sessionUser, String pathToUserDirs) throws IOException {
        logger.log(Level.INFO, String.format(""));
        String username = sessionUser.getUsername();
        String fullPath = pathToUserDirs + File.separator + username; // Add <username> to ../users/
        File dirRef = new File(fullPath);
        boolean result = dirRef.mkdir();  // Create directory
        if (result) {
            logger.log(Level.INFO, String.format("Path of directory where file will be stored is %s.", dirRef.getCanonicalPath()));
            return dirRef;
        } else {
            logger.log(Level.WARNING, String.format("Directory where file should be uploaded was not created."));
            return dirRef;
        }
    }

    private File createCategoryDir(FileCategoryBean category, String pathToUserDirs) throws IOException {
        logger.log(Level.INFO, String.format(""));
        String catName = category.getName().toLowerCase().replaceAll("\\s+", "-");
        String fullPath = pathToUserDirs + File.separator + catName; // Add <categoryName> to ../file-category/
        File dirRef = new File(fullPath);
        boolean result = dirRef.mkdir();  // Create directory
        if (result) {
            logger.log(Level.INFO, String.format("'%s' directory was created.", dirRef.getName()));
            logger.log(Level.INFO, String.format("Path of directory where file will be stored is %s.", dirRef.getCanonicalPath()));
            return dirRef;
        } else {
            logger.log(Level.INFO, String.format("'%s' directory was NOT created. Maybe it exists.", dirRef.getName()));
            logger.log(Level.WARNING, String.format("Full path to directory is '%s'.", dirRef.getCanonicalPath()));
            return dirRef;
        }
    }

    /**
     * Renders response as XML, with <code>message</code> and <code>success</code> tags inside the
     * <code>result</code> tag.
     *
     * @param response
     * @param msg Message to be written in alert
     * @param affRows Success, can be 1 or 0. Color of alert will be based on this
     * @throws IOException If cannot write response
     */
    private void sendResponse(String msg, int affRows) throws IOException {
        logger.log(Level.INFO, String.format(""));
        request.setAttribute("msg", msg);
        request.setAttribute("success", affRows);
        try {
            response.sendRedirect(request.getContextPath() + "/modules/documents/index.jsp?form="+this.formId);
            logger.log(Level.INFO, String.format("Redirect url is: /modules/documents/index.jsp?form=%s", formId));
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error writing response. "+ex.getMessage(), ex);
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
