package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.FileCategoryBean;
import org.locusdoc.businessLayer.FileCategoryRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.WorkflowBean;
import org.locusdoc.businessLayer.WorkflowHistoryBean;
import org.locusdoc.businessLayer.WorkflowRepository;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
@WebServlet(name = "FileCategoryServlet", urlPatterns = {"/document/category-add"})
public class FileCategoryServlet extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final int NO_WORKFLOW = 0;
    private static final int ONE_APPROVER = 1;
    private static final int TWO_APPROVERS = 2;
    private static final Logger logger = Logger.getLogger(LoginServlet.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FileCategoryServlet</title>");
            out.println("</head>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             *
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        this.response = response;
        this.request = request;
        FileCategoryRepository categoryRepo = new FileCategoryRepository();
        FileCategoryBean category = null;
        String msg = null;
        int upd = Integer.parseInt(request.getParameter("isUpdate"));
        boolean isUpdate = (upd == 1);
        Integer fCatId = null;
        Integer workflow = Integer.parseInt(request.getParameter("workflow"));
        if (isUpdate) {
            fCatId = Integer.parseInt(request.getParameter("catId"));
            try {
                category = categoryRepo.getById(fCatId);
            } catch (ApplicationException ex) {
                renderXmlResponse(ex.getMessage() + fCatId, 0);
            }
            if (category == null) {
                renderXmlResponse("File Category with the specified id does not exist." + category, 0);
            }
            category.setName(request.getParameter("name"));

            try {
                categoryRepo.update(category);
            } catch (ApplicationException ex) {
                renderXmlResponse(ex.getMessage(), 0);
            }
        } else {
            category = new FileCategoryBean(request.getParameter("name"), 0); // Send 0 to editable temporary
            int genId = 0;
            try {
                genId = categoryRepo.insert(category);
            } catch (ApplicationException ex) {
                renderXmlResponse(ex.getMessage(), 0);
            }
            category.setId(genId);
            if (category.getId() == 0) {
                renderXmlResponse("There was an error inserting the new file category.", 0);
            }
            switch (workflow) {
                case NO_WORKFLOW:
                    createWorkflow(category, 1);
                    break;
                case ONE_APPROVER:
                    createWorkflow(category, 3);
                    break;
                case TWO_APPROVERS:
                    createWorkflow(category, 2);
                    break;
            }

        }

        if (isUpdate) {
            msg = "File Category was updated successfully.";
        } else {
            msg = "New File Category was created successfully.";
        }
        renderXmlResponse(msg, 1);
    }

    private void createWorkflow(FileCategoryBean newCategory, Integer sourceCatId) throws IOException {
        logger.log(Level.INFO, String.format(""));
        
        ArrayList<WorkflowBean> sourceFlow = null; // Workflow of the fileCategory the new workflow will be copied from
        WorkflowRepository flowRepo = new WorkflowRepository();
        try {
            sourceFlow = flowRepo.getByFileCategory(sourceCatId);
        } catch (ApplicationException ex) {
            logger.log(Level.SEVERE, String.format("Workflow of source File Category couldn't be fetched from database."), ex);
            renderXmlResponse(ex.getMessage(), 0);
        }
        try {
            for (WorkflowBean flow : sourceFlow) {
                flowRepo.insert(null, newCategory.getId(), flow.getState(), flow.getNextState(), flow.getStatus());
            }
        } catch (ApplicationException ex) {
            logger.log(Level.SEVERE, String.format("Workflow for new fileCategory could not be inserted."), ex);
        }
        logger.log(Level.INFO, String.format("Workflow for new fileCategory inserted succesfully."));
    }

    private void renderXmlResponse(String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "FileCategory registration servlet.";
    }

}
