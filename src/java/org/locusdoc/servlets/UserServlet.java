package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.EmployeeBean;
import org.locusdoc.businessLayer.EmployeeDepartmentBean;
import org.locusdoc.businessLayer.EmployeeDepartmentRepository;
import org.locusdoc.businessLayer.EmployeeRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.UserRepository;
import org.locusdoc.utilities.ApplicationException;
import org.locusdoc.utilities.LocusDocUtil;

/**
 *
 *
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/user/repository"})
public class UserServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();

    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        response.setHeader("Cache-Control", "no-cache");
        UserRepository usrRepo = new UserRepository();
        EmployeeRepository empRepo = new EmployeeRepository();
        EmployeeDepartmentRepository edRepo = new EmployeeDepartmentRepository();
        int affRows = 0;
        int upd = Integer.parseInt(request.getParameter("isUpdate"));
        boolean isUpdate = (upd == 1);
        UserBean user = null;
        EmployeeBean empl = null;
        EmployeeDepartmentBean edb = null;
        if (isUpdate) {
            try {
                user = usrRepo.getByUsername(request.getParameter("username"));
                Logger.getLogger(UserServlet.class.getName()).log(Level.INFO, "Trying to fetch user from db.", user);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (user == null) {
                Logger.getLogger(UserServlet.class.getName()).log(Level.WARNING, "User is null.", user);
                renderXmlResponse(response, "Error retrieving user from database.", 0);
            }

            try {
                edb = edRepo.getActiveDeptOf(user.getEmployee());
                Logger.getLogger(UserServlet.class.getName()).log(Level.INFO, "Trying to get active department association for this employee", edb);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (edb == null) {
                renderXmlResponse(response, "This account has no department associated to it.", 0);
            }

            try {
                empl = empRepo.getById(user.getEmployee());
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (empl == null) {
                renderXmlResponse(response, "Error retrieving employee from database.", 0);
            }
        } else {
            user = new UserBean();
            empl = new EmployeeBean();
        }

        empl = setEmployeeFields(response, empl, request);
        user = setUserFields(user, request);

        if (!isUpdate) {
            int empId = 0;
            // Insert new Employee obj
            try {
                empId = empRepo.insert(empl);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            // Insert Department association for this Employee
            Integer deptId = Integer.parseInt(request.getParameter("department"));
            edb = new EmployeeDepartmentBean(empId, deptId, null, null);
            try {
                edRepo.insert(edb);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            // Set some fields
            user.setEmployee(empId);
            user.setChangedPass(0);
            user.setPassword(LocusDocUtil.generate());
            user.setActive(1);
            // Insert new User obj
            try {
                usrRepo.insert(user);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
        } else {
            // Update Employee obj 
            try {
                empRepo.update(empl);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            // Create new Department association for this Employee
            try {
                edRepo.changeToDept(edb, Integer.parseInt(request.getParameter("department")));
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            // Create new User obj
            try {
                usrRepo.update(user);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
        }
        String msg = "New user account was created successfully.";
        if (isUpdate) {
            msg = "User account details were changed successfully.";
        }
        renderXmlResponse(response, msg, 1);
    }

    private UserBean setUserFields(UserBean user, HttpServletRequest request) {
        user.setUsername(request.getParameter("username"));
        user.setFirstName(request.getParameter("firstName"));
        user.setMiddleName(request.getParameter("middleName"));
        user.setLastName(request.getParameter("lastName"));
        user.setRole(Integer.parseInt(request.getParameter("role")));
        return user;
    }

    private EmployeeBean setEmployeeFields(HttpServletResponse response, EmployeeBean e, HttpServletRequest request) throws IOException {
        e.setEmail(request.getParameter("email"));
        /**
         * @TODO Upload image
         */
        e.setProfilePic("default.png");
        e.setJobTitle(request.getParameter("jobTitle"));
        e.setGender(Integer.parseInt(request.getParameter("gender")));
        java.sql.Date birthdate = null;
        try {
            birthdate = getDateFromStr(request.getParameter("birthdate"));
        } catch (ParseException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.WARNING, null, ex);
            renderXmlResponse(response, "Date format in birthday field is incorrect. It should be month/day/year.", 0);
        }
        java.sql.Date today = new Date(new java.util.Date().getTime());
        if (birthdate.after(today)) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.WARNING, null);
            renderXmlResponse(response, "Birthdate cannot be later than today.", 0);
        }

        e.setBirthdate(birthdate);
        e.setHomeTel(request.getParameter("homeTel"));
        e.setMobileTel(request.getParameter("mobileTel"));
        e.setAddress(request.getParameter("address"));
        e.setCity(request.getParameter("city"));
        e.setZipCode(Integer.parseInt(request.getParameter("zipCode")));
        e.setCountry(request.getParameter("country"));
        return e;
    }

    /**
     * jQuery calendar returns a MM/dd/yyyy format date as String
     *
     * @param dateStr
     * @return
     * @throws ParseException
     */
    public java.sql.Date getDateFromStr(String dateStr) throws ParseException {
        java.text.SimpleDateFormat parser = new java.text.SimpleDateFormat("MM/dd/yyyy");
        java.util.Date date = parser.parse(dateStr);
        java.sql.Date dtSQL = new java.sql.Date(date.getTime());
        return dtSQL;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
