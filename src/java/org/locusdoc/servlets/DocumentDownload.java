package org.locusdoc.servlets;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.FileBean;
import org.locusdoc.businessLayer.FileRepository;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 * @author david
 */
@WebServlet(name = "DocumentDownload", urlPatterns = {"/document/download"})
public class DocumentDownload extends HttpServlet {

    private static final Logger logger = Logger.getLogger(DocumentDownload.class.getName());
    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DocumentDownload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DocumentDownload at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fileIdStr = request.getParameter("file");
        Integer fileId = null;
        if (fileIdStr == null) {
            return;
        }
        try {
            fileId = Integer.parseInt(fileIdStr);
        } catch (NumberFormatException ex) {
            logger.log(Level.WARNING, String.format("Error processing fileId parameter."));
            return;
        }

        FileRepository fileRepo = new FileRepository();
        FileBean file = null;
        try {
            file = fileRepo.getById(fileId);
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when fetching file from DB (id = %s).", fileId));
            return;
        }
        if (file != null) {
            logger.log(Level.INFO, String.format("Successfully fetched file with id = %s.", file.getId()));
        } else {
            return;
        }
        File fileRef = new File(file.getFilePath());
        int length = 0;
        ServletOutputStream outStream = response.getOutputStream();
        ServletContext context = getServletConfig().getServletContext();
        String mimetype = context.getMimeType(file.getFilePath());

        // sets response content type
        if (mimetype == null) {
            mimetype = "application/octet-stream";
        }
        response.setContentType(mimetype);
        response.setContentLength((int) fileRef.length());
        String fileName = file.getFileName();

        // sets HTTP header
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        byte[] byteBuffer = new byte[BUFSIZE];
        DataInputStream in = new DataInputStream(new FileInputStream(fileRef));

        // reads the file's bytes and writes them to the response stream
        while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
            outStream.write(byteBuffer, 0, length);
        }

        in.close();
        outStream.close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
