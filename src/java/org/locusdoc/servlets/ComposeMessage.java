/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.MessageBean;
import org.locusdoc.businessLayer.MessageRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.UserRepository;
import org.locusdoc.utilities.ApplicationException;
import org.locusdoc.utilities.LocusDocUtil;

/**
 *
 *
 */
@WebServlet(name = "ComposeMessage", urlPatterns = {"/message/compose"})
public class ComposeMessage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ComposeMessage</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ComposeMessage at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String receiverStr = request.getParameter("receiver");
        Integer receiverId = null;
        try {
            receiverId = Integer.parseInt(receiverStr);
        } catch (NumberFormatException ex) {
            renderXmlResponse(response, ex.getMessage(), 0);
        }

        UserRepository usrComp = new UserRepository();
        UserBean receiver = null;
        try {
            receiver = usrComp.getById(receiverId);
        } catch (ApplicationException ex) {
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        if (receiver == null) {
            renderXmlResponse(response, "User with specified id does not exist.", 0);
        }
        MessageBean msg = constructMsg(request, receiver);
        MessageRepository msgRepo = new MessageRepository();
        try {
            msgRepo.insert(msg);
        } catch (ApplicationException ex) {
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        String resMsg = "Message was sent succesfully to " + receiver.getFirstName() + " " + receiver.getLastName();
        renderXmlResponse(response, resMsg, 1);
    }

    private MessageBean constructMsg(HttpServletRequest request, UserBean receiver) {
        String subject = request.getParameter("subject");
        String content = request.getParameter("content");
        MessageBean msg = new MessageBean();
        msg.setReceiver(receiver.getId());
        msg.setSubject(LocusDocUtil.cleanString(subject));
        msg.setContent(LocusDocUtil.cleanString(content));
        msg.setRead(0);
        UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
        msg.setSender(sessionUser.getId());
        return msg;
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
