package org.locusdoc.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.locusdoc.businessLayer.*;
import org.locusdoc.utilities.ApplicationException;
import org.locusdoc.utilities.LocusDocUtil;

/**
 *
 */
@WebServlet(value = "/login", name = "login-servlet")
public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("index.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserRepository repo = new UserRepository();
        UserBean sessionUser = null;
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String pass_hash = LocusDocUtil.encrypt(password);
        try {
            sessionUser = repo.getByCredentials(username, pass_hash, true);
        } catch (ApplicationException ex) {
            processError(request, response, ex.getMessage());
        }
        if (sessionUser != null) {
            processSuccess(request, response, sessionUser);

        } else {
            processError(request, response, "Username and password combination seems to be incorrect.");
        }
    }

    private void processError(HttpServletRequest request, HttpServletResponse response, String message)
            throws ServletException {
        request.setAttribute("msg", message);
        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        try {
            rd.forward(request, response);
        } catch (IOException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processSuccess(HttpServletRequest request, HttpServletResponse response,
            UserBean sessionUser) throws ServletException {
        if (sessionUser.getActive() == 0) {
            processError(request, response, "Your user account seems to be disabled.<br>"
                    + " Please contact the IT Administrator.");
        } else {
            HttpSession session = request.getSession(true); // Create new session
            try {
                session.setAttribute("settings", fetchSettings());
            } catch (ApplicationException ex) {
                processError(request, response, "Could not fetch settings from database.");
            }
            try {
                session.setAttribute("permissions", fetchPermissionList(sessionUser.getRole()));
            } catch (ApplicationException ex) {
                processError(request, response, "Could not fetch the permissions for your role.");
            }

            session.setAttribute("sessionUser", sessionUser);

            try {
                response.sendRedirect("main.jsp");
            } catch (IOException ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE,
                        "Could not do redirect operation.", ex);
            }
        }
    }

    private Map<String, String> fetchSettings() throws ApplicationException {
        SettingsRepository setRepo = new SettingsRepository();
        Map<String, String> settings = new HashMap<String, String>();
        ArrayList<SettingsBean> setList = null;
        try {
            setList = setRepo.getAll();
        } catch (ApplicationException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, "Could not fetch settings from database.", ex);
            throw ex;
        }
        Logger.getLogger(LoginServlet.class.getName()).log(Level.INFO, "Settings fetched succesfully.", setList);
        for (SettingsBean setting : setList) {
            settings.put(setting.getKey(), setting.getValue());
        }
        return settings;
    }

    private PermissionHandler fetchPermissionList(Integer roleId) throws ApplicationException {
        PermissionRepository permRepo = new PermissionRepository();
        ArrayList<PermissionBean> permissions = null;
        try {
            permissions = permRepo.getByRole(roleId);
        } catch (ApplicationException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE,
                    "Could not fetch permissions from database for role = " + roleId + ".", ex);
        }
        Logger.getLogger(LoginServlet.class.getName()).log(Level.INFO, "Permissions fetched succesfully.", permissions);

        PermissionHandler handler = null;
        try {
            handler = new PermissionHandler(permissions, roleId);
        } catch (ApplicationException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE,
                    "Could not fetch permissions from database for role = " + roleId + ".", ex);
            throw ex;
        }
        Logger.getLogger(LoginServlet.class.getName()).log(Level.INFO, "Permission handler created succesfully.", handler);

        return handler;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet to manage user authentication.";
    }

}
