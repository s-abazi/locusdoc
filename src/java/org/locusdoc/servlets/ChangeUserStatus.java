package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.UserRepository;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 * @author david
 */
@WebServlet(name = "changeUserStatus", urlPatterns = {"/user/change-status"})
public class ChangeUserStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangeUserStatus</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangeUserStatus at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        processRequest(request, response);
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        UserRepository usrRepo = new UserRepository();
        int affRows = 0;
        String msg = null;
        Integer userId = Integer.parseInt(request.getParameter("id"));
        Integer toStatus = Integer.parseInt(request.getParameter("toStatus"));
        UserBean userObj = null;
        response.setHeader("Cache-Control", "no-cache");

        try {
            userObj = usrRepo.getById(userId);
        } catch (ApplicationException ex) {
            msg = ex.getMessage();
            renderXmlResponse(response, msg, 0);
        }

        if ((userObj != null) && (toStatus == 1 || toStatus == 0)) {
            userObj.setActive(toStatus);
            try {
                affRows = usrRepo.update(userObj);
            } catch (ApplicationException ex) {
                Logger.getLogger(ChangeUserStatus.class.getName()).log(Level.SEVERE, null, ex);
                msg = ex.getMessage();
                renderXmlResponse(response, msg, 0);
            }
            if (affRows == 1) {
                String action = "enabled";
                if (toStatus == 0) {
                    action = "disabled";
                }
                msg = "User account was " + action + " successfuly.";
            } else if (affRows == 0) {
                msg = "There was a problem changing the status of the user account!";
            }
            renderXmlResponse(response, msg, affRows);
        } else {
            renderXmlResponse(response, "Error retrieving user from database. "
                    + "User might not exist anymore or there is a database connection problem.", 0);
        }
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
