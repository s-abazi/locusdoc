package org.locusdoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.EmployeeBean;
import org.locusdoc.businessLayer.EmployeeDepartmentBean;
import org.locusdoc.businessLayer.EmployeeDepartmentRepository;
import org.locusdoc.businessLayer.EmployeeRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.UserRepository;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
@WebServlet(name = "ChangeDepartmentServlet", urlPatterns = {"/department/assignment"})
public class ChangeDepartmentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangeDepartmentServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangeDepartmentServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            UserBean sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        String msg = null; // Message to be send to Ajax request
        String deptIdStr = request.getParameter("department");
        String userIdStr = request.getParameter("user");
        String edIdStr = request.getParameter("edbId");
        Integer deptId = null;
        Integer userId = null;
        Integer edId = null;
        int upd = 0;
        try {
            deptId = Integer.parseInt(deptIdStr);
            userId = Integer.parseInt(userIdStr);
            edId = Integer.parseInt(edIdStr);
            upd = Integer.parseInt(request.getParameter("isUpdate"));
        } catch (NumberFormatException ex) {
            msg = "Parameters are incorrect. Please try again.";
            renderXmlResponse(response, msg, 0);
        }
        boolean isUpdate = (upd == 1);
        UserRepository usrRepo = new UserRepository();
        UserBean user = null;
        try {
            user = usrRepo.getById(userId);
        } catch (ApplicationException ex) {
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        if (user == null) {
            renderXmlResponse(response, "User with specified id does not exist.", 0);
        }

        EmployeeRepository empRepo = new EmployeeRepository();
        EmployeeBean empl = null;
        try {
            empl = empRepo.getById(user.getEmployee());
        } catch (ApplicationException ex) {
            renderXmlResponse(response, ex.getMessage(), 0);
        }
        if (empl == null) {
            renderXmlResponse(response, "This user account has no employee assigned.", 0);
        }

        EmployeeDepartmentRepository edRepo = new EmployeeDepartmentRepository();
        EmployeeDepartmentBean edb = null;
        if (isUpdate) {
            try {
                edb = edRepo.getById(edId);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (edb == null) {
                renderXmlResponse(response, "Specified employee to department association doesn't exist.", 0);
            }
            edb.setDepartment(deptId);
            edb.setEmployee(empl.getId());

            try {
                edRepo.update(edb);
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
        } else {
            try {
                edb = edRepo.getActiveDeptOf(empl.getId());
            } catch (ApplicationException ex) {
                renderXmlResponse(response, ex.getMessage(), 0);
            }
            if (edb != null) {
                EmployeeDepartmentBean newEdb = null;
                try {
                    newEdb = edRepo.changeToDept(edb, deptId);
                } catch (ApplicationException ex) {
                    renderXmlResponse(response, ex.getMessage(), 0);
                }
                if (newEdb == null) {
                    renderXmlResponse(response, "There was a problem assigning the employee to the new department.", 0);
                }
            } else {
                edb = new EmployeeDepartmentBean(empl.getId(), deptId, null, null);
                int insRes = 0;
                try {
                    insRes = edRepo.insert(edb);
                } catch (ApplicationException ex) {
                    renderXmlResponse(response, ex.getMessage(), 0);
                }
                if (insRes == 0) {
                    renderXmlResponse(response, "There was a problem assigning the employee to the new department.", 0);
                }
            }
        }
        if (isUpdate) {
            msg = "Employee assignment was updated successfully.";
        } else {
            msg = "Employee was assigned to the new department successfully.";
        }
        renderXmlResponse(response, msg, 1);
    }

    private void renderXmlResponse(HttpServletResponse response, String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
