/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.locusdoc.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.locusdoc.businessLayer.FileBean;
import org.locusdoc.businessLayer.FileRepository;
import org.locusdoc.businessLayer.SharingBean;
import org.locusdoc.businessLayer.SharingRepository;
import org.locusdoc.businessLayer.UserBean;
import org.locusdoc.businessLayer.WorkflowHistoryBean;
import org.locusdoc.businessLayer.WorkflowHistoryRepository;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 * @author david
 */
@WebServlet(name = "RemoveDocument", urlPatterns = {"/document/remove"})
public class RemoveDocument extends HttpServlet {

    private UserBean sessionUser;
    private static final Logger logger = Logger.getLogger(RemoveDocument.class.getName());
    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RemoveDocument</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RemoveDocument at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public boolean hasSession(HttpServletRequest request) {
        if (request.getSession(false) != null) {
            /**
             * @TODO throw error and friendly message
             */
            this.sessionUser = (UserBean) request.getSession(false).getAttribute("sessionUser");
            if (sessionUser != null && sessionUser.getId() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.log(Level.INFO, String.format(""));

        if (!hasSession(request)) {
            response.sendRedirect("index.jsp");
            return;
        }
        this.request = request;
        this.response = response;

        String fileIdStr = request.getParameter("fileId");
        Integer fileId = null;
        if (fileIdStr == null) {
            renderXmlResponse("To complete this request you must send the file id as POST parameter.", 0);
        }
        try {
            fileId = Integer.parseInt(fileIdStr);
        } catch (NumberFormatException ex) {
            logger.log(Level.WARNING, String.format("Error processing fileId parameter."));
            renderXmlResponse("File id must be a number.", 0);
        }
        FileRepository fileRepo = new FileRepository();
        FileBean file = null;
        try {
            file = fileRepo.getById(fileId);
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when fetching file from DB (id = %s).", fileId));
            renderXmlResponse(ex.getMessage(), 0);
        }
        if (file != null) {
            logger.log(Level.INFO, String.format("Successfully fetched file with id = %s.", file.getId()));
        } else {
            renderXmlResponse("File does not seem to exist in the database.", 0);
        }

        WorkflowHistoryRepository flowRepo = new WorkflowHistoryRepository();
        ArrayList<WorkflowHistoryBean> fileFlow = null;
        try {
            fileFlow = flowRepo.getByFile(file.getId());
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when fetching workflow of file (id = %s).", file.getId()));
            renderXmlResponse(ex.getMessage(), 0);
        }
        logger.log(Level.INFO, String.format("Successfully fetched %s flow/s.", fileFlow.size()));

        //Delete Flow from DB
        for (WorkflowHistoryBean flow : fileFlow) {
            try {
                flowRepo.delete(flow.getId());
            } catch (ApplicationException ex) {
                logger.log(Level.WARNING, String.format("Error thrown when deleting flow with id '%s'.", flow.getId()));
                renderXmlResponse(ex.getMessage(), 0);
            }
            logger.log(Level.INFO, String.format("Flow (%s) deleted successfully.", flow.getId()));
        }
        // Delete from Sharing table
        SharingRepository shareRepo = new SharingRepository();
        ArrayList<SharingBean> shares = null;
        try {
            shares = shareRepo.getByFile(file.getId());
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when fetching shares of file (id = %s).", file.getId()));
            renderXmlResponse(ex.getMessage(), 0);
        }
        logger.log(Level.INFO, String.format("Successfully fetched %s shares.", shares.size()));
        for (SharingBean share : shares) {
            try {
                shareRepo.delete(share.getUser(), share.getFile() );
            } catch (ApplicationException ex) {
                logger.log(Level.WARNING, String.format("Error thrown when deleting share."));
                renderXmlResponse(ex.getMessage(), 0);
            }
        }
        //Delete File from server
        boolean refDel = deleteFile(file);
        //Delete File from DB
        int dbDel = 0;
        try {
            dbDel = fileRepo.delete(fileId);
        } catch (ApplicationException ex) {
            logger.log(Level.WARNING, String.format("Error thrown when deleting file with id '%s'.", fileId));
            renderXmlResponse(ex.getMessage(), 0);
        }

        if (refDel) {
            logger.log(Level.INFO, String.format("File (%s) deleted successfully from server.", fileId));
            if (dbDel == 1) {
                logger.log(Level.INFO, String.format("File (%s) deleted successfully from DB.", fileId));
                renderXmlResponse("File was deleted successfully from the server and database.", 1);
            } else {
                renderXmlResponse("File was deleted fromthe server but not from the database.", 0);
            }

        } else {
            logger.log(Level.INFO, String.format("File (%s) not deleted from server.", fileId));
            if (dbDel == 1) {
                logger.log(Level.INFO, String.format("File (%s) deleted successfully from DB.", fileId));
                renderXmlResponse("File was deleted successfully from the database but not from the server.", 1);
            } else {
                renderXmlResponse("File was not deleted successfully.", 0);
            }
        }
    }

    private boolean deleteFile(FileBean file) {
        logger.log(Level.INFO, String.format(""));
        File fileRef = new File(file.getFilePath());
        return fileRef.delete();
    }

    private void renderXmlResponse(String msg, int affRows) throws IOException {
        if (response.isCommitted()) {
            return; // To avoid IllegalStateException
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        StringBuilder sb = new StringBuilder();
        sb.append("<result>");
        sb.append("<message>");
        sb.append(msg);
        sb.append("</message>");
        sb.append("<success>");
        sb.append(affRows);
        sb.append("</success>");
        sb.append("</result>");
        response.getWriter().write(sb.toString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
