package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
public class FormRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `title`, `path`, `nameId`, `position`, "
            + "`parent`, `order` ";

    /**
     * MAIN position - These forms belong to the Main menu in the header.
     */
    public final static Integer MAIN = 0;
    /**
     * DOCUMENTS position - These forms belong to the Documents form.
     */
    public final static Integer DOCUMENTS = 1;
    /**
     * ADMINISTRATION position - These forms belong to the Administration form.
     */
    public final static Integer ADMINISTRATION = 2;
    /**
     * DEFINITIONS position - These forms belong to the Definitions form.
     */
    public final static Integer DEFINITIONS = 3;
    /**
     * ACCOUNT position - These forms belong to the Account (profile details) form.
     */
    public final static Integer ACCOUNT = 5;

    public FormRepository() {
    }

    public FormBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + FormRepository.column_names + "  FROM `Form` WHERE `id` = ? ";
        FormBean form = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                form = getFormFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            return form;
        } finally {
            closeRsc();
        }
        return form;
    }

    public FormBean getByTitle(String title) throws ApplicationException {
        String query = "SELECT " + FormRepository.column_names + "  FROM `Form` WHERE `title` = ? ";
        FormBean form = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, title);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                form = getFormFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            return form;
        } finally {
            closeRsc();
        }
        return form;
    }

    public FormBean getByPath(String path) throws ApplicationException {
        String query = "SELECT " + FormRepository.column_names + "  FROM `Form` WHERE `path` = ? ";
        FormBean form = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, path);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                form = getFormFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            return form;
        } finally {
            closeRsc();
        }
        return form;
    }

    public FormBean getByNameId(String nameId) throws ApplicationException {
        String query = "SELECT " + FormRepository.column_names + "  FROM `Form` WHERE `nameId` = ? ";
        FormBean form = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, nameId);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                form = getFormFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            return form;
        } finally {
            closeRsc();
        }
        return form;
    }

    public ArrayList<FormBean> getAll() throws ApplicationException {
        String query = "SELECT " + FormRepository.column_names + " FROM `Form` ORDER BY "
                + "`parent` ASC, `order` ASC";
        ArrayList<FormBean> list = new ArrayList<FormBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFormFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FormBean> getByPosition(Integer position) throws ApplicationException {
        String query = "SELECT " + FormRepository.column_names + " FROM `Form` WHERE `position` = ?"
                + " ORDER BY `parent` ASC, `order` ASC";
        ArrayList<FormBean> list = new ArrayList<FormBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, position);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFormFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }
    
    public ArrayList<FormBean> getByPosition(Integer position, Integer parent) throws ApplicationException {
        String condition = (parent == null || parent == 0) ? "IS ? " : "= ? ";
        String query = "SELECT " + FormRepository.column_names + " FROM `Form` WHERE `position` = ? "
                + "AND `parent` "+condition
                + " ORDER BY `parent` ASC, `order` ASC";
        ArrayList<FormBean> list = new ArrayList<FormBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, position);
            if (parent == null || parent == 0) {
                pstmt.setNull(2, Types.INTEGER);
            } else {
                pstmt.setInt(2, parent);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFormFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FormBean> getByParent(Integer parent) throws ApplicationException {
        String condition = (parent == null || parent == 0) ? "IS ? " : "= ? ";
        String query = "SELECT " + FormRepository.column_names + " FROM `Form` WHERE `parent` " + condition
                + "ORDER BY `order` ASC";
        ArrayList<FormBean> list = new ArrayList<FormBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            if (parent == null || parent == 0) {
                pstmt.setNull(1, Types.INTEGER);
            } else {
                pstmt.setInt(1, parent);
            }

        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFormFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FormBean> getByParentNull() throws ApplicationException {
        return getByParent(null);
    }

    public ArrayList<FormBean> getOnlyChildren() {
        String query = "SELECT " + FormRepository.column_names + " FROM `Form` WHERE `parent` IS NOT NULL"
                + " ORDER BY `parent` ASC, `order` ASC";
        ArrayList<FormBean> list = new ArrayList<FormBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFormFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }
    
    public ArrayList<FormBean> getOnlyChildren(Integer position) {
        String query = "SELECT " + FormRepository.column_names + " FROM `Form` WHERE `parent` IS NOT NULL"
                + " AND `position` = ? ORDER BY `parent` ASC, `order` ASC";
        ArrayList<FormBean> list = new ArrayList<FormBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, position);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFormFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String title, String path, String nameId, Integer position,
            Integer parent, Integer order) throws ApplicationException {
        FormBean form = new FormBean(id, title, path, nameId, position, parent, order);
        return insert(form);
    }

    public int insert(FormBean form) throws ApplicationException {
        validate(form);
        if (form.getId() != 0) {
            throw new ApplicationException("Id of the Form should be zero for an insert.");
        }
        String query = "INSERT INTO `Form` ( " + FormRepository.column_names + ") VALUES (NULL, ?, ?, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, form.getTitle());
            pstmt.setString(2, form.getPath());
            pstmt.setString(3, form.getNameId());
            pstmt.setInt(4, form.getPosition());
            if (form.getParent() == null || form.getParent() == 0) {
                pstmt.setNull(5, Types.INTEGER);
            } else {
                pstmt.setInt(5, form.getParent());
            }
            pstmt.setInt(6, form.getOrder());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Form with the same title, path or nameId already exists.", ex);
            }
        }
        form.setId(getPrimaryKey());
        closeRsc();
        return form.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Form object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String title, String path, String nameId, Integer position,
            Integer parent, Integer order) throws ApplicationException {
        FormBean form = new FormBean(id, title, path, nameId, position, parent, order);
        return update(form);
    }

    public int update(FormBean form) throws ApplicationException {
        validate(form);
        if (form.getId() < 1) {
            throw new ApplicationException("Id of the Form is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Form` SET `title`=?, `path`=?, `nameId`=?, `position`=?, `parent`=?,"
                + " `order`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, form.getTitle());
            pstmt.setString(2, form.getPath());
            pstmt.setString(3, form.getNameId());
            pstmt.setInt(4, form.getPosition());
            if (form.getParent() == null || form.getParent() == 0) {
                pstmt.setNull(5, Types.INTEGER);
            } else {
                pstmt.setInt(5, form.getParent());
            }
            pstmt.setInt(6, form.getOrder());
            pstmt.setInt(7, form.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Form with the same title, path or nameId already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Form with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        FormBean form = getById(id);
        if (form == null) {
            throw new ApplicationException("Form with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Form` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, form.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private FormBean getFormFromRSet(ResultSet rset) throws SQLException {
        FormBean form = new FormBean();
        form.setId(rset.getInt("id"));
        form.setTitle(rset.getString("title"));
        form.setPath(rset.getString("path"));
        form.setNameId(rset.getString("nameId"));
        form.setPosition(rset.getInt("position"));
        form.setParent(rset.getInt("parent"));
        form.setOrder(rset.getInt("order"));
        return form;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Method to check if any of the mandatory attributes are <code>null</code>. Do <b>not</b>
     * overwrite this method
     *
     * @param form
     * @return <code>TRUE</code> - if all the mandatory fields are set and <br><code>FALSE</code> -
     * If one or more fields are <code>null</code>
     */
    private void validate(FormBean form) throws ApplicationException {
        if (form == null) {
            throw new ApplicationException("Form object cannot be null.");
        }
        if (form.getId() == null || form.getTitle() == null || form.getPath() == null
                && form.getPosition() == null || form.getOrder() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (form.getTitle().trim().isEmpty() || form.getPath().trim().isEmpty() || form.getNameId().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }

}
