package org.locusdoc.businessLayer;

import java.io.Serializable;

public class AlertBean implements Serializable {

    private Integer id = 0;
    private String title;
    private String content;
    private Integer user;

    /**
     * Empty Constructor
     */
    public AlertBean() {

    }

    /**
     * Constructor with all fields, not with id because it will be auto generated from the database
     * on insert.
     *
     * @param title
     * @param content
     * @param User
     */
    public AlertBean(String title, String content, Integer user) {
        this.title = title;
        this.content = content;
        this.user = user;
    }

    /**
     * Constructor with all fields.
     *
     * @param id
     * @param title
     * @param content
     * @param user
     */
    public AlertBean(Integer id, String title, String content, Integer user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.title);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.content);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.user);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AlertBean)) {
            return false;
        }
        AlertBean temp = (AlertBean) obj;
        if (!(this.getId().equals(temp.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
