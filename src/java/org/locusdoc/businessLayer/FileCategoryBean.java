package org.locusdoc.businessLayer;

import java.io.Serializable;

public class FileCategoryBean implements Serializable {

    private Integer id = 0;
    private String name;
    private Integer editable;

    public FileCategoryBean() {
    }

    public FileCategoryBean(String name, Integer editable) {
        this.name = name;
        this.editable = editable;
    }

    public FileCategoryBean(Integer id, String name, Integer editable) {
        this.id = id;
        this.name = name;
        this.editable = editable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEditable() {
        return editable;
    }

    public void setEditable(Integer editable) {
        this.editable = editable;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        sb.append(this.id);
        sb.append("] ");
        sb.append("[");
        sb.append(this.name);
        sb.append("] ");
        sb.append("[");
        sb.append(this.editable);
        sb.append("]}");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof FileCategoryBean)) {
            return false;
        }
        FileCategoryBean temp = (FileCategoryBean) obj;
        return this.getId().equals(temp.getId());
    }
}
