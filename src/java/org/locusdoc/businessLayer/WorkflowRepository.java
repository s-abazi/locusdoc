package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class WorkflowRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `fileCategory`, `state`, `nextState`, `status` ";

    public WorkflowRepository() {
    }

    public WorkflowBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + WorkflowRepository.column_names + "  FROM `Workflow` WHERE `id` = ? ";
        WorkflowBean wf = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                wf = getWorkflowFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            return wf;
        } finally {
            closeRsc();
        }
        return wf;
    }

    public ArrayList<WorkflowBean> getByState(Integer state) throws ApplicationException {
        String query = "SELECT " + WorkflowRepository.column_names + "  FROM `Workflow` WHERE `state` = ? ";
        ArrayList<WorkflowBean> list = new ArrayList<WorkflowBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, state);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<WorkflowBean> getByStatus(Integer status) throws ApplicationException {

        String query = "SELECT " + WorkflowRepository.column_names + " FROM `Workflow` WHERE `status` = ? ";
        ArrayList<WorkflowBean> list = new ArrayList<WorkflowBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, status);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<WorkflowBean> getByFileCategory(Integer fileCategory) throws ApplicationException {
        if (fileCategory == null || fileCategory < 1) {
            throw new ApplicationException("Message::getByFileCategory() sender parameter is " + fileCategory + ". Must be 1 or larger.");
        }
        String query = "SELECT " + WorkflowRepository.column_names + " FROM `Workflow` WHERE `fileCategory` = ? ";
        ArrayList<WorkflowBean> list = new ArrayList<WorkflowBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, fileCategory);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<WorkflowBean> getAll() throws ApplicationException {
        String query = "SELECT " + WorkflowRepository.column_names + " FROM `Workflow`";
        ArrayList<WorkflowBean> list = new ArrayList<WorkflowBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, Integer fileCategory, Integer state, Integer nextState, Integer status) throws ApplicationException {
        WorkflowBean wf = new WorkflowBean(0, fileCategory, state, nextState, status);
        return insert(wf);
    }

    public int insert(WorkflowBean wf) throws ApplicationException {
        validate(wf);
        if (wf.getId() != 0) {
            throw new ApplicationException("Id of the Workflow should be zero for an insert.");
        }
        String query = "INSERT INTO `Workflow` ( " + WorkflowRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, wf.getFileCategory());
            pstmt.setInt(2, wf.getState());
            pstmt.setInt(3, wf.getNextState());
            pstmt.setInt(4, wf.getStatus());

            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("Error inserting Workflow object into database. SQL state: " + ex.getSQLState() + "\nError Code: " + ex.getErrorCode());
        }
        wf.setId(getPrimaryKey());
        closeRsc();
        return wf.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, Integer fileCategory, Integer state, Integer nextState, Integer status) throws ApplicationException {
        WorkflowBean wf = new WorkflowBean(id, fileCategory, state, nextState, status);
        return update(wf);
    }

    public int update(WorkflowBean wf) throws ApplicationException {
        validate(wf);
        if (wf.getId() < 1) {
            throw new ApplicationException("Id of the Workflow is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Workflow` SET `fileCategory`=?, `state`=?, `nextState`=?, `status`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, wf.getFileCategory());
            pstmt.setInt(2, wf.getState());
            pstmt.setInt(3, wf.getNextState());
            pstmt.setInt(4, wf.getStatus());
            pstmt.setInt(5, wf.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Workflow with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        WorkflowBean wf = getById(id);
        if (wf == null) {
            throw new ApplicationException("Workflow with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Workflow` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, wf.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private WorkflowBean getWorkflowFromRSet(ResultSet rset) throws SQLException {
        WorkflowBean wf = new WorkflowBean();
        wf.setId(rset.getInt("id"));
        wf.setFileCategory(rset.getInt("fileCategory"));
        wf.setState(rset.getInt("state"));
        wf.setNextState(rset.getInt("nextState"));
        wf.setStatus(rset.getInt("status"));
        return wf;

    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(WorkflowBean wf) throws ApplicationException {
        if (wf == null) {
            throw new ApplicationException("Workflow object cannot be null.");
        }
        if (wf.getFileCategory() == null || wf.getState() == null || wf.getNextState() == null
                || wf.getStatus() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }
}
