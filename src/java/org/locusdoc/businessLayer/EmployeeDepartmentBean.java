package org.locusdoc.businessLayer;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class EmployeeDepartmentBean implements Serializable {

    private Integer id = 0;
    private Integer employee;
    private Integer department;
    private Timestamp beginDate;
    private Timestamp endDate;

    public EmployeeDepartmentBean() {
    }

    public EmployeeDepartmentBean(Integer employee, Integer department, Timestamp beginDate, Timestamp endDate) {
        this.employee = employee;
        this.department = department;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public EmployeeDepartmentBean(Integer id, Integer employee, Integer department, Timestamp beginDate, Timestamp endDate) {
        this.id = id;
        this.employee = employee;
        this.department = department;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the employee
     */
    public Integer getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(Integer employee) {
        this.employee = employee;
    }

    /**
     * @return the department
     */
    public Integer getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(Integer department) {
        this.department = department;
    }

    /**
     * @return the beginDate
     */
    public Timestamp getBeginDate() {
        return beginDate;
    }

    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(Timestamp beginDate) {
        this.beginDate = beginDate;
    }

    /**
     * @return the endDate
     */
    public Timestamp getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof EmployeeDepartmentBean)) {
            return false;
        }
        return equals((EmployeeDepartmentBean) obj);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.employee);
        hash = 53 * hash + Objects.hashCode(this.department);
        return hash;
    }

    public boolean equals(EmployeeDepartmentBean edb) {
        if (!edb.getId().equals(this.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.employee);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.department);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.beginDate);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.endDate);
        sb.append("]");
        return sb.toString();
    }

}
