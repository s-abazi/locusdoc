package org.locusdoc.businessLayer;

import java.io.Serializable;

/**
 *
 *
 */
public class PermissionBean implements Serializable {

    private Integer form = 0;
    private Integer role = 0;
    private Integer action = 0;

    public PermissionBean() {
    }

    public PermissionBean(Integer form, Integer role, Integer action) {
        this.form = form;
        this.role = role;
        this.action = action;
    }

    /**
     * @return the form
     */
    public Integer getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(Integer form) {
        this.form = form;
    }

    /**
     * @return the role
     */
    public Integer getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Integer role) {
        this.role = role;
    }

    /**
     * @return the action
     */
    public Integer getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(Integer action) {
        this.action = action;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n [");
        sb.append(this.form);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.role);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.action);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof PermissionBean)) {
            return false;
        }
        PermissionBean perm = (PermissionBean) obj;
        if (!(this.getForm().equals(perm.getForm()))) {
            return false;
        }
        if (!(this.getRole().equals(perm.getRole()))) {
            return false;
        }
        if (!(this.getAction().equals(perm.getAction()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.form != null ? this.form.hashCode() : 0);
        hash = 47 * hash + (this.role != null ? this.role.hashCode() : 0);
        hash = 47 * hash + (this.action != null ? this.action.hashCode() : 0);
        return hash;
    }

}
