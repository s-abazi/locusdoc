package org.locusdoc.businessLayer;

import java.io.Serializable;

public class SettingsBean implements Serializable {

    private Integer id = 0;
    private String key;
    private String value;

    /**
     * Empty Constructor
     */
    public SettingsBean() {

    }

    /**
     * Constructor without id.
     *
     * @param key
     * @param value
     */
    public SettingsBean(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public SettingsBean(Integer id, String key, String value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.key);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.value);
        sb.append("] \n ");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SettingsBean)) {
            return false;
        }
        SettingsBean temp = (SettingsBean) obj;
        if (!(this.getId().equals(temp.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
