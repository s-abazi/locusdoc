package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class UserRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `username`, `password`, `firstName`, "
            + "`middleName`, `lastName`, `active`, `employee`, `role`, `changedPass` ";

    public UserRepository() {
    }

    public UserBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + UserRepository.column_names + "  FROM `User` WHERE `id` = ? ";
        UserBean user = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                user = getUserFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            return user;
        } finally {
            closeRsc();
        }
        return user;
    }

    public UserBean getByUsername(String username) throws ApplicationException {
        String query = "SELECT " + UserRepository.column_names + "  FROM `User` WHERE `username` = ? ";
        UserBean user = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, username);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                user = getUserFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            return user;
        } finally {
            closeRsc();
        }
        return user;
    }

    public UserBean getByCredentials(String username, String password, boolean withInactive)
            throws ApplicationException {
        String cond = "";
        if (!(withInactive)) {
            cond = "AND `active` = 1 ";
        }
        String query = "SELECT " + UserRepository.column_names + "  FROM `User` WHERE "
                + " BINARY `username` = ? AND BINARY `password` = ? " + cond;
        UserBean user = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. \nPlease, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                user = getUserFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            return user;
        } finally {
            closeRsc();
        }
        return user;
    }

    public UserBean getByEmployee(Integer employeeId) throws ApplicationException {
        String query = "SELECT " + UserRepository.column_names + "  FROM `User` WHERE `employee` = ? ";
        UserBean user = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, employeeId);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                user = getUserFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            return user;
        } finally {
            closeRsc();
        }
        return user;
    }

    public ArrayList<UserBean> getByRole(Integer role) throws ApplicationException {
        String query = "SELECT " + UserRepository.column_names + " FROM `User` WHERE `role` = ?";
        ArrayList<UserBean> list = new ArrayList<UserBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, role);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getUserFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<UserBean> getActive() throws ApplicationException {
        return getAll(UserBean.ACTIVE);
    }

    public ArrayList<UserBean> getInactive() throws ApplicationException {
        return getAll(UserBean.INACTIVE);
    }

    private ArrayList<UserBean> getAll(int active) throws ApplicationException {
        String query = "SELECT " + UserRepository.column_names + " FROM `User` WHERE `active` = ?";
        ArrayList<UserBean> list = new ArrayList<UserBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, active);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getUserFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<UserBean> getAll() throws ApplicationException {
        String query = "SELECT " + UserRepository.column_names + " FROM `User`";
        ArrayList<UserBean> list = new ArrayList<UserBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getUserFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String username, String password, String firstName,
            String middleName, String lastName, int active, Integer employee, Integer role,
            int changedPass) throws ApplicationException {

        UserBean user = new UserBean(id, username, password, firstName, middleName, lastName,
                active, employee, role, changedPass);
        return insert(user);
    }

    public int insert(UserBean user) throws ApplicationException {
        validate(user);
        if (user.getId() != 0) {
            throw new ApplicationException("Id of the User should be zero for an insert.");
        }
        String query = "INSERT INTO `User` ( " + UserRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getFirstName());
            pstmt.setString(4, user.getMiddleName());
            pstmt.setString(5, user.getLastName());
            pstmt.setInt(6, user.getActive());
            pstmt.setInt(7, user.getEmployee());
            pstmt.setInt(8, user.getRole());
            pstmt.setInt(9, user.getChangedPass());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("User with the username set to " + user.getUsername()
                        + " already exists.", ex);
            }
        }
        user.setId(getPrimaryKey());
        closeRsc();
        return user.getId();
    }

    public int update(Integer id, String username, String password, String firstName,
            String middleName, String lastName, int active, Integer employee, Integer role,
            int changedPass) throws ApplicationException {
        UserBean user = new UserBean(id, username, password, firstName, middleName, lastName,
                active, employee, role, changedPass);
        return update(user);
    }

    public int update(UserBean user) throws ApplicationException {
        validate(user);
        if (user.getId() < 1) {
            throw new ApplicationException("Id of the User is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `User` SET `username`=?, `password`=?, `firstName`=?, `middleName`=?,"
                + " `lastName`=?, `active`=?, `employee`=?, `role`=?, `changedPass`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getFirstName());
            pstmt.setString(4, user.getMiddleName());
            pstmt.setString(5, user.getLastName());
            pstmt.setInt(6, user.getActive());
            pstmt.setInt(7, user.getEmployee());
            pstmt.setInt(8, user.getRole());
            pstmt.setInt(9, user.getChangedPass());
            pstmt.setInt(10, user.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("User with the username set to " + user.getUsername()
                        + " already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("User with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        UserBean user = getById(id);
        if (user == null) {
            throw new ApplicationException("User with the specified Id does not exist.");
        }
        String query = "DELETE FROM `User` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, user.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the User object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    // " `id`, `username`, `password`, `firstName`, `middleName`, `lastName`, `active`, `employee`, `role`, `changedPass` "
    private UserBean getUserFromRSet(ResultSet rset) throws SQLException {
        UserBean u = new UserBean();
        u.setId(rset.getInt("id"));
        u.setUsername(rset.getString("username"));
        u.setPassword(rset.getString("password"));
        u.setFirstName(rset.getString("firstName"));
        u.setMiddleName(rset.getString("middleName"));
        u.setLastName(rset.getString("lastName"));
        u.setActive(rset.getInt("active"));
        u.setEmployee(rset.getInt("employee"));
        u.setRole(rset.getInt("role"));
        u.setChangedPass(rset.getInt("changedPass"));
        return u;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void validate(UserBean u) throws ApplicationException {
        if (u == null) {
            throw new ApplicationException("Department object cannot be null.");
        }
        // Return false if any of the mandatory fields is null
        if (u.getId() == null || u.getUsername() == null || u.getPassword() == null
                || u.getFirstName() == null || u.getLastName() == null || u.getActive() == null
                || u.getEmployee() == null || u.getRole() == null || u.getChangedPass() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        // Return false if any of the mandatory Strings is empty
        if (u.getUsername().trim().isEmpty() || u.getPassword().trim().isEmpty()
                || u.getFirstName().trim().isEmpty() || u.getLastName().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }

}
