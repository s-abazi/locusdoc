package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 */
public class ActionRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `action`, `description` ";

    public ActionRepository() {
    }

    public ActionBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + ActionRepository.column_names + "  FROM `Action` WHERE `id` = ? ";
        ActionBean action = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                action = getActionFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            return action;
        } finally {
            closeRsc();
        }
        return action;
    }

    public ActionBean getByAction(String action) throws ApplicationException {
        String query = "SELECT " + ActionRepository.column_names + "  FROM `Action` WHERE `action` = ? ";
        ActionBean act = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, action);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                act = getActionFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            return act;
        } finally {
            closeRsc();
        }
        return act;
    }

    public ArrayList<ActionBean> getAll() throws ApplicationException {
        String query = "SELECT " + ActionRepository.column_names + " FROM `Action`";
        ArrayList<ActionBean> list = new ArrayList<ActionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getActionFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String action, String description) throws ApplicationException {
        ActionBean actionObj = new ActionBean(id, action, description);
        return insert(actionObj);
    }

    public int insert(ActionBean actionObj) throws ApplicationException {
        validate(actionObj);
        if (actionObj.getId() != 0) {
            throw new ApplicationException("Id of the Action should be zero for an insert.");
        }
        String query = "INSERT INTO `Action` ( " + ActionRepository.column_names + ") VALUES (NULL, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, actionObj.getAction());
            pstmt.setString(2, actionObj.getDescription());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Action with the action name set to " + actionObj.getAction()
                        + " already exists.", ex);
            }
        }
        actionObj.setId(getPrimaryKey());
        closeRsc();
        return actionObj.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Action object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String action, String description) throws ApplicationException {
        ActionBean actionObj = new ActionBean(id, action, description);
        return update(actionObj);
    }

    public int update(ActionBean actionObj) throws ApplicationException {
        validate(actionObj);
        if (actionObj.getId() < 1) {
            throw new ApplicationException("Id of the Action is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Action` SET `action`=?, `description`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, actionObj.getAction());
            pstmt.setString(2, actionObj.getDescription());
            pstmt.setInt(3, actionObj.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Action with the same action name already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Action with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        ActionBean action = getById(id);
        if (action == null) {
            throw new ApplicationException("Action with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Action` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, action.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private ActionBean getActionFromRSet(ResultSet rset) throws SQLException {
        ActionBean act = new ActionBean();
        act.setId(rset.getInt("id"));
        act.setAction(rset.getString("action"));
        act.setDescription(rset.getString("description"));
        return act;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(ActionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     */
    private void validate(ActionBean action) throws ApplicationException {
        if (action == null) {
            throw new ApplicationException("Action object cannot be null.");
        }
        if (action.getId() == null || action.getAction() == null || action.getDescription() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (action.getAction().trim().isEmpty() || action.getDescription() == null) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
