package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Timestamp;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class FileRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private static final String column_names = " `id`, `fileName`, `requestCategory`, `owner`, `filePath`, `fileType`, `fileSize`, `createdDate`, `locked`, `state`";

    public FileRepository() {
    }

    public FileBean getByFilePath(String filePath) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + " FROM `File` WHERE `filePath` = ? ";
        FileBean file = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, filePath);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                file = getFileFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            return file;
        } finally {
            closeRsc();
        }
        return file;
    }

    public FileBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `id` = ? ";
        FileBean file = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                file = getFileFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            return file;
        } finally {
            closeRsc();
        }
        return file;
    }
    
     public FileBean getByCategory(Integer category) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `requestCategory` = ? ";
        FileBean file = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, category);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                file = getFileFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            return file;
        } finally {
            closeRsc();
        }
        return file;
    }
     
     public ArrayList<FileBean> getByCategoryAndState(Integer category, Integer state) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `requestCategory` = ? "
                + "AND `state` = ? ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, category);
            pstmt.setInt(2, state);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FileBean> getByFileType(String fileType) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `fileType` = ? ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, fileType);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }
    
    public ArrayList<FileBean> getByState(Integer state) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `state` = ? ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, state);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }
    
    public ArrayList<FileBean> getByOwner(Integer owner) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `owner` = ? ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, owner);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }
    
    public ArrayList<FileBean> getApprovedOf(Integer userId) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `owner` = ? AND `state` = 100 ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, userId);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }
    
    /**
     * Return List of Files which don't have the specified state
     * @param state
     * @return
     * @throws ApplicationException 
     */
    public ArrayList<FileBean> getByStateIsNot(Integer state) throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + "  FROM `File` WHERE `state` != ? ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, state);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FileBean> getByFileName(String fileName) throws ApplicationException {
        if (fileName == null) {
            throw new ApplicationException("Message::getByFileName() sender parameter is " + fileName + ". Must not be null.");
        }
        String query = "SELECT " + FileRepository.column_names + " FROM `FIle` WHERE `fileName` = ? ";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, fileName);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FileBean> getAll() throws ApplicationException {
        String query = "SELECT " + FileRepository.column_names + " FROM `File`";
        ArrayList<FileBean> list = new ArrayList<FileBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFileFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String fileName, Integer requestCategory, Integer owner, String filePath,
            String fileType, Integer fileSize, Timestamp createdDate, Integer locked, Integer state) throws ApplicationException {
        FileBean file = new FileBean(id, fileName, requestCategory, owner, filePath, fileType, fileSize,
                createdDate, locked, state);
        return insert(file);
    }

    public int insert(FileBean file) throws ApplicationException {
        validate(file);
        if (file.getId() != 0) {
            throw new ApplicationException("Id of the File should be zero for an insert.");
        }
        String query = "INSERT INTO `File` ( " + FileRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?, ?, ?, ?, NOW(), ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, file.getFileName());
            pstmt.setInt(2, file.getRequestCategory());
            pstmt.setInt(3, file.getOwner());
            pstmt.setString(4, file.getFilePath());
            pstmt.setString(5, file.getFileType());
            pstmt.setInt(6, file.getFileSize());
            pstmt.setInt(7, file.getLocked());
            pstmt.setInt(8, file.getState());

            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("Error inserting Workflow object into database. SQL state: " + ex.getSQLState() + "\nError Code: " + ex.getErrorCode());
        }
        file.setId(getPrimaryKey());
        closeRsc();
        return file.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String fileName, Integer requestCategory, Integer owner, String filePath,
            String fileType, Integer fileSize, Timestamp createdDate, Integer locked, Integer state) throws ApplicationException {
        FileBean file = new FileBean(id, fileName, requestCategory, owner, filePath, fileType, fileSize,
                createdDate, locked, state);
        return update(file);
    }

    public int update(FileBean file) throws ApplicationException {
        validate(file);
        if (file.getId() < 1) {
            throw new ApplicationException("Id of the File is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `File` SET `fileName`= ?, `requestCategory`= ?, `owner`= ?, `filePath`= ?,"
                + " `fileType`= ?, `fileSize`= ?, `createdDate`= ?, `locked`= ?, `state`= ? WHERE `id` = ? ";

        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, file.getFileName());
            pstmt.setInt(2, file.getRequestCategory());
            pstmt.setInt(3, file.getOwner());
            pstmt.setString(4, file.getFilePath());
            pstmt.setString(5, file.getFileType());
            pstmt.setInt(6, file.getFileSize());
            if (file.getCreatedDate() == null) {
                java.util.Date currDt = new java.util.Date();
                file.setCreatedDate(new Timestamp(currDt.getTime()));
            }
            pstmt.setTimestamp(7, file.getCreatedDate());
            pstmt.setInt(8, file.getLocked());
            pstmt.setInt(9, file.getState());
            pstmt.setInt(10, file.getId());

            affRows = pstmt.executeUpdate();

        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("File with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        FileBean file = getById(id);
        if (file == null) {
            throw new ApplicationException("File with the specified Id does not exist.");
        }
        String query = "DELETE FROM `File` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, file.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private FileBean getFileFromRSet(ResultSet rset) throws SQLException {
        FileBean file = new FileBean();
        file.setId(rset.getInt("id"));
        file.setFileName(rset.getString("filename"));
        file.setRequestCategory(rset.getInt("requestCategory"));
        file.setOwner(rset.getInt("owner"));
        file.setFilePath(rset.getString("filePath"));
        file.setFileType(rset.getString("fileType"));
        file.setFileSize(rset.getInt("fileSize"));
        file.setCreatedDate(rset.getTimestamp("createdDate"));
        file.setLocked(rset.getInt("locked"));
        file.setState(rset.getInt("state"));
        return file;

    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(FileBean file) throws ApplicationException {
        if (file == null) {
            throw new ApplicationException("File object cannot be null.");
        }
        if (file.getId() == null || file.getFileName() == null || file.getRequestCategory() == null
                || file.getOwner() == null || file.getFilePath() == null || file.getFileType() == null
                || file.getFileSize() == null || file.getLocked() == null || file.getState() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (file.getFileName().trim().isEmpty() || file.getFilePath().trim().isEmpty()
                || file.getFileType().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
