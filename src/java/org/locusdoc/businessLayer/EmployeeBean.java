package org.locusdoc.businessLayer;

//~--- JDK imports ------------------------------------------------------------
import java.io.Serializable;

import java.sql.Date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * Class to represent the Employee table
 */
public class EmployeeBean implements Serializable {

    private Integer id = 0;
    private Integer gender = -1;
    private String email;
    private String profilePic;
    private String jobTitle;
    private Date birthdate;
    private String homeTel;
    private String mobileTel;
    private String address;
    private String city;
    private Integer zipCode;
    private String country;

    /**
     * Empty constructor
     */
    public EmployeeBean() {
    }

    public EmployeeBean(String email, String profilePic, String jobTitle, Integer gender, Date birthdate,
            String homeTel, String mobileTel, String address, String city, Integer zipCode,
            String country) {
        this.email = email;
        this.profilePic = profilePic;
        this.jobTitle = jobTitle;
        this.gender = gender;
        this.birthdate = birthdate;
        this.homeTel = homeTel;
        this.mobileTel = mobileTel;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
    }

    public EmployeeBean(Integer id, String email, String profilePic, String jobTitle,
            Integer gender, Date birthdate, String homeTel, String mobileTel, String address,
            String city, Integer zipCode, String country) {
        this.id = id;
        this.email = email;
        this.profilePic = profilePic;
        this.jobTitle = jobTitle;
        this.gender = gender;
        this.birthdate = birthdate;
        this.homeTel = homeTel;
        this.mobileTel = mobileTel;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
    }

    /**
     * Method to get id field If the Id is zero (0) this Employee object was just created using the
     * constructor or there was an error returning this object from the database when used any of
     * the static Get methods.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * @param profilePic the profilePic to set
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * @return the jobTitle
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * @param jobTitle the jobTitle to set
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * If the gender is minus one (-1) this Employee object was just created using the constructor
     * or there was an error returning this object from the database when used any of the static Get
     * methods of Employee class.
     *
     * @return the gender
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * Set the gender of this Employee. Valid inputs are 0 (Other), 1 (Male) and 2 (Female)
     *
     * @param gender Gender value
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * @return the birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * @param birthdate the birthdate to set
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String birthDateToStr(String format) {
        DateFormat df = new SimpleDateFormat(format);
        String dateStr = df.format(getBirthdate());

        return dateStr;
    }

    /**
     * @return the homeTel
     */
    public String getHomeTel() {
        return homeTel;
    }

    /**
     * @param homeTel the homeTel to set
     */
    public void setHomeTel(String homeTel) {
        this.homeTel = homeTel;
    }

    /**
     * @return the mobileTel
     */
    public String getMobileTel() {
        return mobileTel;
    }

    /**
     * @param mobileTel the mobileTel to set
     */
    public void setMobileTel(String mobileTel) {
        this.mobileTel = mobileTel;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the zipCode
     */
    public Integer getZipCode() {
        return zipCode;
    }

    /**
     * @param zipCode the zipCode to set
     */
    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    public String genderToStr() {
        String genderStr;
        switch (this.getGender()) {
            case 1:
                genderStr = "Male";
                break;

            case 2:
                genderStr = "Female";
                break;

            default:
                genderStr = "Other";
                break;
        }
        return genderStr;
    }

    @Override
    public String toString() {
        // <editor-fold defaultstate="collapsed" desc="String used to represent this EmployeeBean.">
        StringBuilder sb = new StringBuilder();
        sb.append("\n [");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.email);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.profilePic);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.jobTitle);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.gender);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.birthdate);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.homeTel);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.mobileTel);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.address);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.city);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.zipCode);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.country);
        sb.append("]");
        return sb.toString();
        // </editor-fold>
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj == null) || !(obj instanceof EmployeeBean)) {
            return false;
        }
        EmployeeBean temp = (EmployeeBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + ((this.id != null) ? this.id.hashCode() : 0);
        return hash;
    }
}
