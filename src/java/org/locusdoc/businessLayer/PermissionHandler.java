package org.locusdoc.businessLayer;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.servlets.LoginServlet;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
public class PermissionHandler {

    private ArrayList<PermissionBean> list;
    private RoleBean role;

    public PermissionHandler() {
    }

    /**
     *
     * @param list Permission list for this role.
     * @param roleid
     * @throws ApplicationException
     */
    public PermissionHandler(ArrayList<PermissionBean> list, int roleid) throws ApplicationException {
        if (list == null || list.isEmpty()) {
            throw new ApplicationException("Permission list cannot be null or empty.");
        }
        this.list = list;
        RoleRepository roleRepo = new RoleRepository();
        this.role = roleRepo.getById(roleid);
    }

    /**
     * Re-fetch permissions from database
     *
     * @throws ApplicationException
     */
    public void fetchPermissions() throws ApplicationException {
        PermissionRepository permRepo = new PermissionRepository();
        setList(permRepo.getByRole(role.getId()));
    }
    
    public boolean hasPermission(int form, String actionStr) {
        ActionBean action = null;
        ActionRepository actRepo = new ActionRepository();
        try {
            action = actRepo.getByAction(actionStr);
        } catch (ApplicationException ex) {
            return false;
        }
        if (action == null) {
            Logger.getLogger(PermissionHandler.class.getName()).log(Level.WARNING, "Action is null, returned false.");
            return false;
        }
        PermissionBean perm = new PermissionBean(form, this.role.getId(), action.getId());
        return hasPermission(perm);
    }
    /**
     * 
     * @param form
     * @param actionStr
     * @return true if in list, false otherwise
     */
    public boolean hasPermission(FormBean form, String actionStr) {
        if (form == null){
            Logger.getLogger(PermissionHandler.class.getName()).log(Level.WARNING, "Form is null, returned false.");
            return false;
        }
        ActionBean action = null;
        ActionRepository actRepo = new ActionRepository();
        try {
            action = actRepo.getByAction(actionStr);
        } catch (ApplicationException ex) {
            return false;
        }
        if (action == null) {
            Logger.getLogger(PermissionHandler.class.getName()).log(Level.WARNING, "Action [{0}] is null, returned false.", actionStr);
            return false;
        }
        PermissionBean perm = new PermissionBean(form.getId(), this.role.getId(), action.getId());
        return hasPermission(perm);
    }

    public boolean hasPermission(int form, int action) {
        PermissionBean perm = new PermissionBean(form, this.role.getId(), action);
        return hasPermission(perm);
    }

    public boolean hasPermission(PermissionBean perm) {
        return getList().contains(perm);
    }

    /**
     * @return the list
     */
    public ArrayList<PermissionBean> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<PermissionBean> list) {
        this.list = list;
    }
}
