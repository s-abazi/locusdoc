package org.locusdoc.businessLayer;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Class LogsBean is a JavaBean that represents a Log object from Logs table in database
 */
public class LogsBean implements Serializable {

    private Integer id = 0;
    private Integer action;
    private String description;
    private Timestamp timestamp;
    private Integer user;

    /**
     * Empty Constructor
     */
    public LogsBean() {
    }

    public LogsBean(Integer id, Integer action, String description, Timestamp timestamp, Integer user) {
        this.id = id;
        this.action = action;
        this.description = description;
        this.timestamp = timestamp;
        this.user = user;
    }

    public LogsBean(Integer action, String description, Timestamp timestamp, Integer user) {
        this.action = action;
        this.description = description;
        this.timestamp = timestamp;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.action);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.description);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.timestamp);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.user);
        sb.append("] \n ");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof LogsBean)) {
            return false;
        }
        LogsBean temp = (LogsBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
