package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
public class FileVersionRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `file`, `version`, `fileName`, `fileCategory`, "
            + "`owner`, `filePath`, `fileType`, `fileSize`, `createdDate`, `locked`, `state`, "
            + "`versionUser`, `versionDate`, `versionComment` ";

    public FileVersionBean getByPK(Integer file, Integer version) throws ApplicationException {
        String query = "SELECT " + FileVersionRepository.column_names + "  FROM `FileVersion` WHERE "
                + "`file` = ? AND `version` = ?";
        FileVersionBean fvb = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, file);
            pstmt.setInt(2, version);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                fvb = getVersionFromRset(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            return fvb;
        } finally {
            closeRsc();
        }
        return fvb;
    }

    /**
     *
     * @param file The id of the File object
     * @return fvb FileVersionBean that represents the latest FileVersion of the given file id
     * @throws ApplicationException if there was an error setting up the database connection
     */
    public FileVersionBean getLastVersionOf(Integer file) throws ApplicationException {
        String query = "SELECT " + FileVersionRepository.column_names + " FROM `FileVersion` AS `fv` "
                + "WHERE `version` =  "
                + "(SELECT MAX(`version`) FROM `FileVersion` as `fv2` "
                + "WHERE `fv`.`file` = `fv2`.`file`)"
                + "AND `fv`.file = ?";
        FileVersionBean fvb = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, file);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                fvb = getVersionFromRset(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            return fvb;
        } finally {
            closeRsc();
        }
        return fvb;
    }

    public ArrayList<FileVersionBean> getVersionsOf(Integer file) throws ApplicationException {
        String query = "SELECT " + FileVersionRepository.column_names + " FROM `FileVersion` WHERE `file` = ?";
        ArrayList<FileVersionBean> list = new ArrayList<FileVersionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, file);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getVersionFromRset(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FileVersionBean> getAll(int locked) throws ApplicationException {
        String lockedCond = "";
        if (locked == 1) {
            lockedCond = " WHERE `locked` = 1";
        } else if (locked == 0) {
            lockedCond = " WHERE `locked` = 0";
        }
        String query = "SELECT " + FileVersionRepository.column_names + " FROM `FileVersion`" + lockedCond;
        ArrayList<FileVersionBean> list = new ArrayList<FileVersionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getVersionFromRset(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FileVersionBean> getLatestOfAll() throws ApplicationException {
        String query = "SELECT " + FileVersionRepository.column_names + " FROM `FileVersion` AS `fv` "
                + "WHERE `version` =  "
                + "(SELECT MAX(`version`) FROM `FileVersion` as `fv2` "
                + "WHERE `fv`.`file` = `fv2`.`file`)";
        ArrayList<FileVersionBean> list = new ArrayList<FileVersionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getVersionFromRset(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer file, Integer version, String fileName, Integer fileCategory, Integer owner, String filePath,
            String fileType, Integer fileSize, Timestamp createdDate, Integer locked,
            Integer state, Integer versionUser, Timestamp versionDate, String versionComment)
            throws ApplicationException {

        FileVersionBean fVersion = new FileVersionBean(file, version, fileName, fileCategory, owner, filePath,
                fileType, fileSize, createdDate, locked, state, versionUser, versionDate,
                versionComment);
        return insert(fVersion);
    }

    public int insert(FileVersionBean fVersion) throws ApplicationException {
        validate(fVersion);
        String query = "INSERT INTO `FileVersion` ( " + FileVersionRepository.column_names + ") "
                + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        int affRows = 0;
        try {
            pstmt.setInt(1, fVersion.getFile());
            pstmt.setInt(2, fVersion.getVersion());
            pstmt.setString(3, fVersion.getFileName());
            pstmt.setInt(4, fVersion.getFileCategory());
            pstmt.setInt(5, fVersion.getOwner());
            pstmt.setString(6, fVersion.getFilePath());
            pstmt.setString(7, fVersion.getFileType());
            pstmt.setInt(8, fVersion.getFileSize());
            pstmt.setTimestamp(9, fVersion.getCreatedDate());
            pstmt.setInt(10, fVersion.getLocked());
            pstmt.setInt(11, fVersion.getState());
            pstmt.setInt(12, fVersion.getVersionUser());
            if (fVersion.getVersionDate() == null) {
                java.util.Date today = new java.util.Date();
                Timestamp sqlToday = new Timestamp(today.getTime());
                fVersion.setVersionDate(sqlToday);
            }
            pstmt.setTimestamp(13, fVersion.getVersionDate());
            pstmt.setString(14, fVersion.getVersionComment());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            throw new ApplicationException("There was an error creating the FileVersion. SQL State Code: " + ex.getSQLState(), ex);
        }
        closeRsc();
        return affRows;
    }

    public int update(Integer file, Integer version, String fileName, Integer fileCategory, Integer owner, String filePath,
            String fileType, Integer fileSize, Timestamp createdDate, Integer locked,
            Integer state, Integer versionUser, Timestamp versionDate, String versionComment)
            throws ApplicationException {
        FileVersionBean fVersion = new FileVersionBean(file, version, fileName, fileCategory, owner, filePath,
                fileType, fileSize, createdDate, locked, state, versionUser, versionDate,
                versionComment);
        return update(fVersion);
    }

    public int update(FileVersionBean fvb) throws ApplicationException {
        validate(fvb);
        /**
         * " `file`, `version`, `fileName`, `fileCategory`, " + "`owner`, `filePath`, `fileType`,
         * `fileSize`, `createdDate`, `locked`, `state`, " + "`versionUser`, `versionDate`,
         * `versionComment` "
         */
        String query = "UPDATE `FileVersion` SET `fileName`=?, `fileCategory`=?, `owner`=?, "
                + "`filePath` = ?, `fileType`=?, `fileSize`=?, `createdDate`=?, `locked`=?, "
                + "`state`=?, `versionUser`=?, `versionDate`=?, `versionComment`=? WHERE `file`=? AND `version` = ?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, fvb.getFileName());
            pstmt.setInt(2, fvb.getFileCategory());
            pstmt.setInt(3, fvb.getOwner());
            pstmt.setString(4, fvb.getFilePath());
            pstmt.setString(5, fvb.getFileType());
            pstmt.setInt(6, fvb.getFileSize());
            pstmt.setTimestamp(7, fvb.getCreatedDate());
            pstmt.setInt(8, fvb.getLocked());
            pstmt.setInt(9, fvb.getState());
            pstmt.setInt(10, fvb.getVersionUser());
            pstmt.setTimestamp(11, fvb.getVersionDate());
            pstmt.setString(12, fvb.getVersionComment());
            pstmt.setInt(13, fvb.getFile());
            pstmt.setInt(14, fvb.getVersion());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            throw new ApplicationException("Erro executing update. SQL State: " + sqlError, ex);
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("FileVersion with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer file, Integer version) throws ApplicationException {
        FileVersionBean fvb = getByPK(file, version);
        if (fvb == null) {
            throw new ApplicationException("FileVersion with the specified file andversion combination does not exist.");
        }
        String query = "DELETE FROM `FileVersion` WHERE `file` = ? AND `version` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, fvb.getFile());
            pstmt.setInt(2, fvb.getVersion());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the delete query on FileVersion table.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private FileVersionBean getVersionFromRset(ResultSet rset) {
        FileVersionBean fVersion = null;
        try {
            fVersion = new FileVersionBean();
            fVersion.setFile(rset.getInt("file"));
            fVersion.setVersion(rset.getInt("version"));
            fVersion.setFileName(rset.getString("fileName"));
            fVersion.setFileCategory(rset.getInt("fileCategory"));
            fVersion.setOwner(rset.getInt("owner"));
            fVersion.setFilePath(rset.getString("filePath"));
            fVersion.setFileType(rset.getString("fileType"));
            fVersion.setFileSize(rset.getInt("fileSize"));
            fVersion.setCreatedDate(rset.getTimestamp("createdDate"));
            fVersion.setLocked(rset.getInt("locked"));
            fVersion.setState(rset.getInt("state"));
            fVersion.setVersionUser(rset.getInt("versionUser"));
            fVersion.setVersionDate(rset.getTimestamp("versionDate"));
            fVersion.setVersionComment(rset.getString("versionComment"));
        } catch (SQLException sqlExc) {
            Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, sqlExc);
        }
        return fVersion;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileVersionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     */
    private void validate(FileVersionBean fv) throws ApplicationException {
        if (fv == null) {
            throw new ApplicationException("FileVersion object cannot be null.");
        }
        if (fv.getFile() == null || fv.getVersion() == null || fv.getFileName() == null
                || fv.getOwner() == null || fv.getFilePath() == null || fv.getFileSize() == null
                || fv.getCreatedDate() == null || fv.getLocked() == null || fv.getState() == null
                || fv.getVersionUser() == null || fv.getVersionComment() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (fv.getFileName().trim().isEmpty() || fv.getFilePath().trim().isEmpty()
                || fv.getVersionComment().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
