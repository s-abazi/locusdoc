package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 * A repository class with CRUD functions for EmployeeBean.
 */
public class EmployeeRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `email`, `profilePic`, `jobTitle`, `gender`,"
            + " `birthdate`, `homeTel`, `mobileTel`, `address`, `city`, `zipCode`, `country` ";

    /**
     * Empty constructor
     */
    public EmployeeRepository() {
    }

    public EmployeeBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + EmployeeRepository.column_names + "  FROM `Employee` WHERE `id`"
                + " = ? ";
        EmployeeBean empl = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                empl = getEmployeeFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            return empl;
        } finally {
            closeRsc();
        }
        return empl;
    }

    public ArrayList<EmployeeBean> getAll() throws ApplicationException {
        String query = "SELECT " + EmployeeRepository.column_names + " FROM `Employee`";
        ArrayList<EmployeeBean> list = new ArrayList<EmployeeBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getEmployeeFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<EmployeeBean> getByDept(Integer dept) throws ApplicationException {
        throw new UnsupportedOperationException("Not yet implemented!");
    }

    public int insert(String email, String profilePic, String jobTitle, Integer gender,
            Date birthdate, String homeTel, String mobileTel, String address, String city,
            Integer zipCode, String country) throws ApplicationException {
        EmployeeBean emp = new EmployeeBean(email, profilePic, jobTitle, gender, birthdate, homeTel,
                mobileTel, address, city, zipCode, country);
        return insert(emp);
    }

    public int insert(EmployeeBean empl) throws ApplicationException {
        validate(empl);
        if (empl.getId() != 0) {
            throw new ApplicationException("Id of the Employee should be zero for an insert.");
        }
        String query = "INSERT INTO `Employee` ( " + EmployeeRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, empl.getEmail());
            pstmt.setString(2, empl.getProfilePic());
            pstmt.setString(3, empl.getJobTitle());
            pstmt.setInt(4, empl.getGender());
            pstmt.setDate(5, empl.getBirthdate());
            pstmt.setString(6, empl.getHomeTel());
            pstmt.setString(7, empl.getMobileTel());
            pstmt.setString(8, empl.getAddress());
            pstmt.setString(9, empl.getCity());
            pstmt.setInt(10, empl.getZipCode());
            pstmt.setString(11, empl.getCountry());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Employee with the email set to " + empl.getEmail()
                        + " already exists.", ex);
            }
        }
        empl.setId(getPrimaryKey());
        closeRsc();
        return empl.getId();
    }

    public int update(Integer id, String email, String profilePic, String jobTitle, Integer gender,
            Date birthdate, String homeTel, String mobileTel, String address, String city,
            Integer zipCode, String country) throws ApplicationException {
        EmployeeBean empl = new EmployeeBean(id, email, profilePic, jobTitle, gender, birthdate,
                homeTel, mobileTel, address, city, zipCode, country);
        return update(empl);
    }

    public int update(EmployeeBean empl) throws ApplicationException {
        validate(empl);
        if (empl.getId() < 1) {
            throw new ApplicationException("Id of the Employee is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Employee` SET `email`=?, `profilePic`=?, "
                + "`jobTitle`=?, `gender`=?, `birthdate`=?, `homeTel`=?, `mobileTel`=?, `address`=?,"
                + " `city`=?, `zipCode`=?, `country`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, empl.getEmail());
            pstmt.setString(2, empl.getProfilePic());
            pstmt.setString(3, empl.getJobTitle());
            pstmt.setInt(4, empl.getGender());
            pstmt.setDate(5, empl.getBirthdate());
            pstmt.setString(6, empl.getHomeTel());
            pstmt.setString(7, empl.getMobileTel());
            pstmt.setString(8, empl.getAddress());
            pstmt.setString(9, empl.getCity());
            pstmt.setInt(10, empl.getZipCode());
            pstmt.setString(11, empl.getCountry());
            pstmt.setInt(12, empl.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Employee with the email set to " + empl.getEmail()
                        + " already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Employee with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        EmployeeBean empl = getById(id);
        if (empl == null) {
            throw new ApplicationException("Employee with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Employee` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, empl.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private EmployeeBean getEmployeeFromRSet(ResultSet rset) throws SQLException {
        EmployeeBean e = new EmployeeBean();
        e.setId(rset.getInt("id"));
        e.setEmail(rset.getString("email"));
        e.setProfilePic(rset.getString("profilePic"));
        e.setJobTitle(rset.getString("jobTitle"));
        e.setGender(rset.getInt("gender"));
        e.setBirthdate(rset.getDate("birthdate"));
        e.setHomeTel(rset.getString("homeTel"));
        e.setMobileTel(rset.getString("mobileTel"));
        e.setAddress(rset.getString("address"));
        e.setCity(rset.getString("city"));
        e.setZipCode(rset.getInt("zipCode"));
        e.setCountry(rset.getString("country"));
        return e;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Employee object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    private void validate(EmployeeBean e) throws ApplicationException {
        if (e == null) {
            throw new ApplicationException("Department object cannot be null.");
        }
        // Return false if any of the mandatory fields is null
        if (e.getId() == null || e.getEmail() == null || e.getProfilePic() == null
                || e.getJobTitle() == null || e.getGender() == null || e.getBirthdate() == null
                || e.getHomeTel() == null || e.getAddress() == null || e.getCity() == null
                || e.getZipCode() == null || e.getCountry() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        // Return false if any of the mandatory Strings is empty
        if (e.getEmail().trim().isEmpty() || e.getProfilePic().trim().isEmpty()
                || e.getJobTitle().trim().isEmpty() || e.getHomeTel().trim().isEmpty()
                || e.getAddress().trim().isEmpty() || e.getCity().trim().isEmpty()
                || e.getCountry().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
