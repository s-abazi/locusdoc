/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 */
public class EmployeeDepartmentRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `employee`, `department`, `begin_date`, "
            + "`end_date` ";

    public EmployeeDepartmentBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM "
                + "`Employee_Department` WHERE `id` = ?";
        EmployeeDepartmentBean edb = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                edb = getFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            return edb;
        } finally {
            closeRsc();
        }
        return edb;
    }

    public ArrayList<EmployeeDepartmentBean> getByEmplAndDept(Integer employee, Integer dept)
            throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM "
                + "`Employee_Department` WHERE `employee` = ? AND `department` = ? ";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, employee);
            pstmt.setInt(2, dept);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<EmployeeDepartmentBean> getDeptHistoryOf(Integer employee)
            throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM "
                + "`Employee_Department` WHERE `employee` = ? ORDER BY `end_date` ASC ";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, employee);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public EmployeeDepartmentBean getActiveDeptOf(Integer employee)
            throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM "
                + "`Employee_Department` WHERE `employee` = ? AND `end_date` IS NULL ";
        EmployeeDepartmentBean edb = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, employee);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                edb = (getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return edb;
    }

    public ArrayList<EmployeeDepartmentBean> getEmplHistoryOf(Integer dept)
            throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM "
                + "`Employee_Department` WHERE `department` = ? ORDER BY `employee` ASC ";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, dept);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<EmployeeDepartmentBean> getActiveEmplOf(Integer dept)
            throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM"
                + " `Employee_Department` WHERE `department` = ? AND `end_date` IS NULL"
                + " ORDER BY `employee` ASC ";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, dept);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<EmployeeDepartmentBean> getAllActive()
            throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM"
                + " `Employee_Department` WHERE `end_date` IS NULL";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FormRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<EmployeeDepartmentBean> getAll() throws ApplicationException {
        String query = "SELECT " + EmployeeDepartmentRepository.column_names + " FROM `Employee_Department`";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<EmployeeDepartmentBean> getAllNotDisabled() throws ApplicationException {
        String query = "SELECT `ED`.* FROM `Employee_Department` AS `ED`"
                + " INNER JOIN `User` AS `U`"
                + " ON `U`.`employee` = `ED`.`employee`"
                + " WHERE `U`.`active` = 1";
        ArrayList<EmployeeDepartmentBean> list = new ArrayList<EmployeeDepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, Integer employee, Integer department, Timestamp beginDate, Timestamp endDate) throws ApplicationException {
        EmployeeDepartmentBean edb = new EmployeeDepartmentBean(id, employee, department, beginDate, endDate);
        return insert(edb);
    }

    public int insert(EmployeeDepartmentBean edb) throws ApplicationException {
        validate(edb);
        if (edb.getId() != 0) {
            throw new ApplicationException("Id of the Department should be zero for an insert.");
        }
        String query = "INSERT INTO `Employee_Department` ( " + EmployeeDepartmentRepository.column_names + ") VALUES (NULL, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, edb.getEmployee());
            pstmt.setInt(2, edb.getDepartment());
            if (edb.getBeginDate() == null) {
                java.util.Date today = new java.util.Date();
                Timestamp sqlToday = new Timestamp(today.getTime());
                edb.setBeginDate(sqlToday);
            }
            pstmt.setTimestamp(3, edb.getBeginDate());
            pstmt.setTimestamp(4, edb.getEndDate());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            throw new ApplicationException("There was an error creating the Employee to "
                    + "Department association. SQL State Code: " + ex.getSQLState(), ex);
        }
        edb.setId(getPrimaryKey());
        closeRsc();
        return edb.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Employee to Department association. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, Integer employee, Integer department, Timestamp beginDate, Timestamp endDate) throws ApplicationException {
        EmployeeDepartmentBean edb = new EmployeeDepartmentBean(id, employee, department, beginDate, endDate);
        return update(edb);
    }

    public int update(EmployeeDepartmentBean edb) throws ApplicationException {
        validate(edb);
        if (edb.getId() < 1) {
            throw new ApplicationException("Id of the EmployeeDepartment is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Employee_Department` SET `employee`=?, `department`=?, `begin_date`=?, `end_date`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, edb.getEmployee());
            pstmt.setInt(2, edb.getDepartment());
            pstmt.setTimestamp(3, edb.getBeginDate());
            pstmt.setTimestamp(4, edb.getEndDate());
            pstmt.setInt(5, edb.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error updating the Employee to "
                    + "Department association. SQL State Code: " + ex.getSQLState(), ex);
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Employee to Department with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        EmployeeDepartmentBean edb = getById(id);
        if (edb == null) {
            throw new ApplicationException("EmployeeDepartment with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Employee_Department` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, edb.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    /**
     * Sets end_date of this EmployeeDepartment to current Datetime and creates new
     * EmployeeDepartment object in database, where the end_date is null
     *
     * @param edb
     * @param newDept
     * @return newEdb The newly created EmployeeDepartment
     * @throws ApplicationException if there was an error ending the last EmployeeDepartment
     * association or an error was thrown when creating the new EmloyeeDepartment association
     */
    public EmployeeDepartmentBean changeToDept(EmployeeDepartmentBean edb, Integer newDept) throws ApplicationException {
        java.util.Date today = new java.util.Date();
        Timestamp sqlToday = new Timestamp(today.getTime());
        edb.setEndDate(sqlToday);
        try {
            update(edb);
        } catch (ApplicationException ex) {
            throw new ApplicationException("There was an error ending the Employee to Department association.", ex);
        }
        EmployeeDepartmentBean newEdb = new EmployeeDepartmentBean();
        newEdb.setEmployee(edb.getEmployee());
        newEdb.setDepartment(newDept);
        newEdb.setEndDate(null);
        try {
            insert(newEdb);
        } catch (ApplicationException ex) {
            throw new ApplicationException("There was an error creating the new Employee to Department association.", ex);
        } finally {
            closeRsc();
        }
        return newEdb;
    }

    private EmployeeDepartmentBean getFromRSet(ResultSet rset) {
        EmployeeDepartmentBean edb = null;
        try {
            edb = new EmployeeDepartmentBean();
            edb.setId(rset.getInt("id"));
            edb.setEmployee(rset.getInt("employee"));
            edb.setDepartment(rset.getInt("department"));
            edb.setBeginDate(rset.getTimestamp("begin_date"));
            edb.setEndDate(rset.getTimestamp("end_date"));
        } catch (SQLException sqlExc) {
            Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, sqlExc);
        }
        return edb;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeDepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     */
    private void validate(EmployeeDepartmentBean edb) throws ApplicationException {
        if (edb == null) {
            throw new ApplicationException("EmployeeDepartment object cannot be null.");
        }
        if (edb.getId() == null || edb.getEmployee() == null || edb.getDepartment() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }
}
