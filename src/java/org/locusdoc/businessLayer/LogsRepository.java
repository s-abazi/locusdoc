package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class LogsRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `action`, `description`, `timestamp`, `user` ";

    public LogsRepository() {
    }

    public LogsBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + LogsRepository.column_names + "  FROM `Logs` WHERE `id` = ? ";
        LogsBean logs = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                logs = getLogsFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            return logs;
        } finally {
            closeRsc();
        }
        return logs;
    }

    /**
     * Get all logs from a specific date.
     *
     * @param timestamp
     * @return
     * @throws ApplicationException
     */
    public LogsBean getByDate(Timestamp timestamp) throws ApplicationException {
        String query = "SELECT" + LogsRepository.column_names + "FROM `Logs` WHERE `timestamp` = " + timestamp;
        LogsBean logs = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setTimestamp(1, timestamp);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                logs = getLogsFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            return logs;
        } finally {
            closeRsc();
        }
        return logs;
    }

    /**
     * Get all logs for this User.
     *
     * @param user
     * @return
     * @throws ApplicationException
     */
    public ArrayList<LogsBean> getByUser(Integer user) throws ApplicationException {

        String query = "SELECT " + LogsRepository.column_names + " FROM `Logs` WHERE user = ? ";
        ArrayList<LogsBean> list = new ArrayList<LogsBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, user);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getLogsFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<LogsBean> getAll() throws ApplicationException {
        String query = "SELECT " + LogsRepository.column_names + " FROM `Logs`";
        ArrayList<LogsBean> list = new ArrayList<LogsBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getLogsFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, Integer action, String description, Timestamp timestamp, Integer user) throws ApplicationException {
        LogsBean logs = new LogsBean(id, action, description, timestamp, user);
        return insert(logs);
    }

    public int insert(LogsBean logs) throws ApplicationException {
        validate(logs);
        if (logs.getId() != 0) {
            throw new ApplicationException("Id of the Log should be zero for an insert.");
        }
        String query = "INSERT INTO `Logs` ( " + LogsRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, NOW(), ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, logs.getAction());
            pstmt.setString(2, logs.getDescription());
            pstmt.setInt(3, logs.getUser());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        }
        logs.setId(getPrimaryKey());
        closeRsc();
        return logs.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, Integer action, String description, Timestamp timestamp, Integer user) throws ApplicationException {
        LogsBean logs = new LogsBean(id, action, description, timestamp, user);
        return update(logs);
    }

    public int update(LogsBean logs) throws ApplicationException {
        validate(logs);
        if (logs.getId() < 1) {
            throw new ApplicationException("Id of the Log is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Logs` SET `action`=?, `description`=?, `timestamp`=?, `user`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, logs.getAction());
            pstmt.setString(2, logs.getDescription());
            pstmt.setTimestamp(3, logs.getTimestamp());
            pstmt.setInt(4, logs.getUser());
            pstmt.setInt(5, logs.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Log with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        LogsBean logs = getById(id);
        if (logs == null) {
            throw new ApplicationException("Log with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Logs` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, logs.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private LogsBean getLogsFromRSet(ResultSet rset) throws SQLException {
        LogsBean logs = new LogsBean();
        logs.setId(rset.getInt("id"));
        logs.setAction(rset.getInt("action"));
        logs.setDescription(rset.getString("description"));
        logs.setTimestamp(rset.getTimestamp("timestamp"));
        logs.setUser(rset.getInt("user"));
        return logs;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(LogsRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(LogsBean logs) throws ApplicationException {
        if (logs == null) {
            throw new ApplicationException("Logs object cannot be null.");
        }
        if (logs.getId() == null || logs.getDescription() == null || logs.getUser() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (logs.getDescription().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
