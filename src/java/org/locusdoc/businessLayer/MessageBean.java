package org.locusdoc.businessLayer;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Class MessageBean is a JavaBean that represents a Message object from Message table in database
 *
 */
public class MessageBean implements Serializable {

    private Integer id = 0;
    private String subject;
    private String content;
    private Timestamp timestamp;
    private Integer read;
    private Integer sender;
    private Integer receiver;

    /**
     * Empty Constructor
     */
    public MessageBean() {

    }

    public MessageBean(Integer id, String subject, String content, Timestamp timestamp,
            Integer read, Integer sender, Integer receiver) {
        this.id = id;
        this.subject = subject;
        this.content = content;
        this.timestamp = timestamp;
        this.read = read;
        this.sender = sender;
        this.receiver = receiver;
    }

    public MessageBean(String subject, String content, Timestamp timestamp,
            Integer read, Integer sender, Integer receiver) {
        this.subject = subject;
        this.content = content;
        this.timestamp = timestamp;
        this.read = read;
        this.sender = sender;
        this.receiver = receiver;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getRead() {
        return read;
    }

    public void setRead(Integer read) {
        this.read = read;
    }

    public Integer getSender() {
        return sender;
    }

    public void setSender(Integer sender) {
        this.sender = sender;
    }

    public Integer getReceiver() {
        return receiver;
    }

    public void setReceiver(Integer receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n [");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.subject);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.content);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.timestamp);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.read);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.sender);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.receiver);
        sb.append("]");

        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MessageBean)) {
            return false;
        }
        MessageBean temp = (MessageBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
