package org.locusdoc.businessLayer;

import java.io.Serializable;

/**
 *
 * @author Majlindë Jahiu
 */
public class ActionBean implements Serializable {

    private Integer id = 0;
    private String action;
    private String description;

    public ActionBean() {
    }

    public ActionBean(Integer id, String action, String description) {
        this.id = id;
        this.action = action;
        this.description = description;
    }

    public ActionBean(String action, String description) {
        this.action = action;
        this.description = description;
    }

    /**
     * Method to get id field If the Id is zero (0) this Action object was just created using the
     * constructor or there was an error returning this object from the database when used any of
     * the static Get methods.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n [");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.action);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.description);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ActionBean)) {
            return false;
        }
        ActionBean temp = (ActionBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
