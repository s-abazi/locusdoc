package org.locusdoc.businessLayer;

import java.io.Serializable;

public class UserStateApproverBean implements Serializable {

    private Integer user = 0;
    private Integer fileCategory = 0;
    private Integer state = 0;

    /**
     * Empty Constructor
     */
    public UserStateApproverBean() {

    }

    /**
     * Constructor with all fields
     *
     * @param user
     * @param fileCategory
     * @param state
     */
    public UserStateApproverBean(Integer user, Integer fileCategory, Integer state) {
        this.user = user;
        this.fileCategory = fileCategory;
        this.state = state;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getFileCategory() {
        return fileCategory;
    }

    public void setFileCategory(Integer fileCategory) {
        this.fileCategory = fileCategory;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.user);
        sb.append("] - ");
        sb.append("[");
        sb.append(this.fileCategory);
        sb.append("]-  ");
        sb.append("[");
        sb.append(this.state);
        sb.append("]\n");
        return sb.toString();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof UserStateApproverBean)) {
            return false;
        }
        UserStateApproverBean temp = (UserStateApproverBean) obj;
        if (!(this.getUser().equals(temp.getUser()))) {
            return false;
        }
        if (!(this.getFileCategory().equals(temp.getFileCategory()))) {
            return false;
        }
        if (!(this.getState().equals(temp.getState()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 41 * hash + (this.fileCategory != null ? this.fileCategory.hashCode() : 0);
        hash = 41 * hash + (this.state != null ? this.state.hashCode() : 0);

        return hash;
    }
}
