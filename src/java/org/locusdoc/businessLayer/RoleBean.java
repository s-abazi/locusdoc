package org.locusdoc.businessLayer;

import java.io.Serializable;

/**
 *
 * Class to represent the Role table
 */
public class RoleBean implements Serializable {

    private Integer id = 0;
    private String name;
    private String description;

    public RoleBean() {
    }

    public RoleBean(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public RoleBean(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n[");
        sb.append(this.id);
        sb.append("] \n");
        sb.append("[");
        sb.append(this.name);
        sb.append("] \n");
        sb.append("[");
        sb.append(this.description);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof RoleBean)) {
            return false;
        }
        RoleBean temp = (RoleBean) obj;
        return temp.getId().equals(getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
