package org.locusdoc.businessLayer;

import java.io.Serializable;
import java.sql.Timestamp;

public class FileBean implements Serializable {

    private Integer id = 0;
    private String fileName;
    private Integer requestCategory;
    private Integer owner;
    private String filePath;
    private String fileType;
    private Integer fileSize;
    private Timestamp createdDate;
    private Integer locked;
    private Integer state;

    public FileBean() {
    }

    public FileBean(int id, String fileName, int requestCategory, int owner, String filePath, String fileType,
            int fileSize, Timestamp createdDate, int locked, int state) {
        this.id = id;
        this.fileName = fileName;
        this.requestCategory = requestCategory;
        this.filePath = filePath;
        this.owner = owner;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.createdDate = createdDate;
        this.locked = locked;
        this.state = state;

    }

    public FileBean(String fileName, int requestCategory, int owner, String filePath, String fileType, int fileSize,
            Timestamp createdDate, int locked, int state) {
        this.fileName = fileName;
        this.requestCategory = requestCategory;
        this.filePath = filePath;
        this.owner = owner;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.createdDate = createdDate;
        this.locked = locked;
        this.state = state;

    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getRequestCategory() {
        return requestCategory;
    }

    public void setRequestCategory(int requestCategory) {
        this.requestCategory = requestCategory;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getLocked() {
        return locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }

    public Integer getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("],  ");
        sb.append("[");
        sb.append(this.fileName);
        sb.append("],  ");
        sb.append("[");
        sb.append(this.requestCategory);
        sb.append("],  ");
        sb.append("[");
        sb.append(this.owner);
        sb.append("], ");
        sb.append("[");
        sb.append(this.filePath);
        sb.append("], ");
        sb.append("[");
        sb.append(this.fileType);
        sb.append("], ");
        sb.append("[");
        sb.append(this.fileSize);
        sb.append("], ");
        sb.append("[");
        sb.append(this.createdDate);
        sb.append("], ");
        sb.append("[");
        sb.append(this.locked);
        sb.append("], ");
        sb.append("[");
        sb.append(this.state);
        sb.append("] \n\n");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof FileBean)) {
            return false;
        }
        FileBean f = (FileBean) obj;
        return this.getId().equals(f.getId());
    }
}
