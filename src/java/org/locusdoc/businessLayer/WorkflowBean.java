package org.locusdoc.businessLayer;

import java.io.Serializable;

public class WorkflowBean implements Serializable {

    private Integer id = 0;
    private Integer fileCategory;
    private Integer state;
    private Integer nextState;
    private Integer status;

    /**
     * Empty Constructor
     */
    public WorkflowBean() {
    }

    /**
     * Constructor with all fields, except id because it will be auto generated from the database on
     * insert.
     *
     * @param fileCategory
     * @param state
     * @param nextState
     * @param status
     *
     *
     */
    public WorkflowBean(Integer fileCategory, Integer state, Integer nextState, Integer status) {
        this.fileCategory = fileCategory;
        this.state = state;
        this.nextState = nextState;
        this.status = status;

    }

    /**
     * Constructor with all fields, including id.
     *
     * @param id
     * @param fileCategory
     * @param state
     * @param nextState
     * @param status
     *
     *
     */
    public WorkflowBean(Integer id, Integer fileCategory, Integer state,
            Integer nextState, Integer status) {
        this.id = id;
        this.fileCategory = fileCategory;
        this.state = state;
        this.nextState = nextState;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFileCategory() {
        return fileCategory;
    }

    public void setFileCategory(Integer fileCategory) {
        this.fileCategory = fileCategory;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getNextState() {
        return nextState;
    }

    public void setNextState(Integer nextState) {
        this.nextState = nextState;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("] - ");
        sb.append("[");
        sb.append(this.fileCategory);
        sb.append("]-  ");
        sb.append("[");
        sb.append(this.state);
        sb.append("] -  ");
        sb.append("[");
        sb.append(this.nextState);
        sb.append("]-  ");
        sb.append("[");
        sb.append(this.status);
        sb.append("]\n");
        return sb.toString();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WorkflowBean)) {
            return false;
        }
        WorkflowBean temp = (WorkflowBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
