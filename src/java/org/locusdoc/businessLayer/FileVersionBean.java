package org.locusdoc.businessLayer;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author david
 */
public class FileVersionBean {
    /* file, version, fileName, fileCategory, owner, filePath, fileType, fileSize, createdDate,
     locked, state, versionUser, versionDate, versionComment */

    private Integer file = 0; // Part of super key, References File
    private Integer version = 0; // Part of super key
    private String fileName;
    private Integer fileCategory; // References FileCategory
    private Integer owner; // References User, that is the owner of this file
    private String filePath;
    private String fileType;
    private Integer fileSize;
    private Timestamp createdDate;
    private Integer locked;
    private Integer state; // References State
    private Integer versionUser;// Refrences User, who created this version by changing this file
    private Timestamp versionDate; // Generated on insert if null
    private String versionComment;

    public FileVersionBean() {
    }

    public FileVersionBean(Integer file, Integer version, String fileName, Integer fileCategory, Integer owner, String filePath,
            String fileType, Integer fileSize, Timestamp createdDate, Integer locked,
            Integer state, Integer versionUser, Timestamp versionDate, String versionComment) {
        this.file = file;
        this.version = version;
        this.fileName = fileName;
        this.fileCategory = fileCategory;
        this.owner = owner;
        this.filePath = filePath;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.createdDate = createdDate;
        this.locked = locked;
        this.state = state;
        this.versionUser = versionUser;
        this.versionDate = versionDate;
        this.versionComment = versionComment;
    }

    /**
     * @return the file
     */
    public Integer getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Integer file) {
        this.file = file;
    }

    /**
     * @return the version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the fileCategory
     */
    public Integer getFileCategory() {
        return fileCategory;
    }

    /**
     * @param fileCategory the fileCategory to set
     */
    public void setFileCategory(Integer fileCategory) {
        this.fileCategory = fileCategory;
    }

    /**
     * @return the owner
     */
    public Integer getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * @param fileType the fileType to set
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * @return the fileSize
     */
    public Integer getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize the fileSize to set
     */
    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return the createdDate
     */
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the locked
     */
    public Integer getLocked() {
        return locked;
    }

    /**
     * @param locked the locked to set
     */
    public void setLocked(Integer locked) {
        this.locked = locked;
    }

    /**
     * @return the state
     */
    public Integer getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * @return the versionUser
     */
    public Integer getVersionUser() {
        return versionUser;
    }

    /**
     * @param versionUser the versionUser to set
     */
    public void setVersionUser(Integer versionUser) {
        this.versionUser = versionUser;
    }

    /**
     * @return the versionDate
     */
    public Timestamp getVersionDate() {
        return versionDate;
    }

    /**
     * @param versionDate the versionDate to set
     */
    public void setVersionDate(Timestamp versionDate) {
        this.versionDate = versionDate;
    }

    /**
     * @return the versionComment
     */
    public String getVersionComment() {
        return versionComment;
    }

    /**
     * @param versionComment the versionComment to set
     */
    public void setVersionComment(String versionComment) {
        this.versionComment = versionComment;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.file);
        hash = 67 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileVersionBean other = (FileVersionBean) obj;
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileVersion{" + "file=" + file + ", version=" + version + ", fileName=" + fileName + ", fileCategory=" + fileCategory + ", owner=" + owner + ", filePath=" + filePath + ", fileType=" + fileType + ", fileSize=" + fileSize + ", createdDate=" + createdDate + ", locked=" + locked + ", state=" + state + ", versionUser=" + versionUser + ", versionDate=" + versionDate + ", versionComment=" + versionComment + '}';
    }

}
