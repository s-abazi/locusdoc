package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class SharingRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `user`, `file`, `view`, `edit`, `remove`";

    public SharingRepository() {

    }

    public ArrayList<SharingBean> getByFile(Integer file) throws ApplicationException {
        String query = "SELECT " + SharingRepository.column_names + "  FROM `Sharing` WHERE `file` = ? ";
        ArrayList<SharingBean> list = new ArrayList<SharingBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, file);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getSharingFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public SharingBean getByUserFile(Integer user, Integer file) throws ApplicationException {
        String query = "SELECT " + SharingRepository.column_names + "  FROM `Sharing` WHERE `user` = ? AND `file` = ? ";
        SharingBean sh = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, user);
            pstmt.setInt(2, file);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                sh = getSharingFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            return sh;
        } finally {
            closeRsc();
        }
        return sh;
    }

    public ArrayList<SharingBean> getByUser(Integer user) throws ApplicationException {
        if (user == null || user < 1) {
            throw new ApplicationException("Message::getByUser() sender parameter is " + user + ". Must be 1 or larger.");
        }
        String query = "SELECT " + SharingRepository.column_names + " FROM `Sharing` WHERE `user` = ? ";
        ArrayList<SharingBean> list = new ArrayList<SharingBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, user);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getSharingFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<SharingBean> getAll() throws ApplicationException {
        String query = "SELECT " + SharingRepository.column_names + " FROM `Sharing`";
        ArrayList<SharingBean> list = new ArrayList<SharingBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getSharingFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer user, Integer file, Integer view, Integer edit, Integer remove) throws ApplicationException {
        SharingBean sh = new SharingBean(user, file, view, edit, remove);
        return insert(sh);
    }

    public int insert(SharingBean sh) throws ApplicationException {
        validate(sh);

        String query = "INSERT INTO `Sharing` ( " + SharingRepository.column_names + ") VALUES "
                + "(?, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        int affRows = 0;
        try {
            pstmt.setInt(1, sh.getUser());
            pstmt.setInt(2, sh.getFile());
            pstmt.setInt(3, sh.getView());
            pstmt.setInt(4, sh.getEdit());
            pstmt.setInt(5, sh.getRemove());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("Error inserting Sharing object into database. SQL state: " + ex.getSQLState() + "\nError Code: " + ex.getErrorCode());
        } finally {
            closeRsc();
        }
        return affRows;
    }

    public int update(Integer user, Integer file, Integer view, Integer edit, Integer remove) throws ApplicationException {
        SharingBean sh = new SharingBean(user, file, view, edit, remove);
        return update(sh);
    }

    public int update(SharingBean sh) throws ApplicationException {
        validate(sh);
        String query = "UPDATE `Sharing` SET `view` = ?, `edit` = ?, `remove` = ?"
                + " WHERE `user`= ? AND `file` = ? ";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, sh.getView());
            pstmt.setInt(2, sh.getEdit());
            pstmt.setInt(3, sh.getRemove());
            pstmt.setInt(4, sh.getUser());
            pstmt.setInt(5, sh.getFile());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Sharing with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer user, Integer file) throws ApplicationException {
        SharingBean sh = getByUserFile(user, file);
        if (sh == null) {
            throw new ApplicationException("File to User association does not exist.");
        }
        String query = "DELETE FROM `Sharing` WHERE `user` = ? AND `file` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, sh.getUser());
            pstmt.setInt(2, sh.getFile());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private SharingBean getSharingFromRSet(ResultSet rset) throws SQLException {
        SharingBean sh = new SharingBean();
        sh.setUser(rset.getInt("user"));
        sh.setFile(rset.getInt("file"));
        sh.setView(rset.getInt("view"));
        sh.setEdit(rset.getInt("edit"));
        sh.setRemove(rset.getInt("remove"));
        return sh;

    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(SharingRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(SharingBean sh) throws ApplicationException {
        if (sh == null) {
            throw new ApplicationException("Sharing object cannot be null.");
        }
        if (sh.getUser() == null || sh.getFile() == null || sh.getView() == null || sh.getEdit() == null
                || sh.getRemove() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }
}
