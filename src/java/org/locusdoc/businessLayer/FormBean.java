package org.locusdoc.businessLayer;

import java.io.Serializable;

/**
 *
 */
public class FormBean implements Serializable {

    private Integer id = 0;
    private String title;
    private String path;
    private String nameId;
    private Integer position;
    private Integer parent = 0;
    private Integer order;

    public FormBean() {
    }

    public FormBean(String title, String path, String nameId, Integer position, Integer parent,
            Integer order) {
        this.title = title;
        this.path = path;
        this.nameId = nameId;
        this.position = position;
        this.parent = parent;
        this.order = order;
    }

    public FormBean(Integer id, String title, String path, String nameId, Integer position, Integer parent,
            Integer order) {
        this.id = id;
        this.title = title;
        this.path = path;
        this.nameId = nameId;
        this.position = position;
        this.parent = parent;
        this.order = order;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * The id or name of the HTML DIV this form belongs to
     *
     * @return the nameId
     */
    public String getNameId() {
        return nameId;
    }

    /**
     * The id or name of the HTML DIV this form belongs to
     *
     * @param nameId the nameId to set
     */
    public void setNameId(String nameId) {
        this.nameId = nameId;
    }

    /**
     * Positions define where this forms will be shown, they can be a DIV or view file.
     *
     * @return the position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * Positions define where this forms will be shown, they can be a DIV or view file.
     *
     * @param position the position to set
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     * @return the parent
     */
    public Integer getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Integer parent) {
        this.parent = parent;
    }

    /**
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n [");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.title);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.path);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.nameId);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.position);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.parent);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.order);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof FormBean)) {
            return false;
        }
        FormBean temp = (FormBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
