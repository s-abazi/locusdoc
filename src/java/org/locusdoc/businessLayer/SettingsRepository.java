package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class SettingsRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `option_key`, `option_value`";

    public SettingsRepository() {

    }

    public SettingsBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + SettingsRepository.column_names + "  FROM `Settings` WHERE `id` = ?";
        SettingsBean sh = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                sh = getSettingsFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            return sh;
        } finally {
            closeRsc();
        }
        return sh;
    }

    public SettingsBean getByKey(String key) throws ApplicationException {
        String query = "SELECT " + SettingsRepository.column_names + "  FROM `Settings` WHERE `option_key` = ?";
        SettingsBean sh = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, key);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                sh = getSettingsFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            return sh;
        } finally {
            closeRsc();
        }
        return sh;
    }

    public ArrayList<SettingsBean> getAll() throws ApplicationException {
        String query = "SELECT " + SettingsRepository.column_names + " FROM `Settings`";
        ArrayList<SettingsBean> list = new ArrayList<SettingsBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getSettingsFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String key, String value) throws ApplicationException {
        SettingsBean sh = new SettingsBean(id, key, value);
        return insert(sh);
    }

    public int insert(SettingsBean sh) throws ApplicationException {
        validate(sh);

        String query = "INSERT INTO `Settings` ( " + SettingsRepository.column_names + ") VALUES "
                + "(NULL, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, sh.getKey());
            pstmt.setString(2, sh.getValue());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("Settings object could not be inserted.");
        }
        sh.setId(getPrimaryKey());
        closeRsc();
        return sh.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Settings object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String key, String value) throws ApplicationException {
        SettingsBean sh = new SettingsBean(id, key, value);
        return update(sh);
    }

    public int update(SettingsBean sh) throws ApplicationException {
        validate(sh);
        String query = "UPDATE `Settings` SET `option_key` = ?, `option_value` = ?"
                + " WHERE `id` = ? ";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, sh.getKey());
            pstmt.setString(2, sh.getValue());
            pstmt.setInt(3, sh.getId());

            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Settings with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        SettingsBean sh = getById(id);
        if (sh == null) {
            throw new ApplicationException("Id association does not exist.");
        }
        String query = "DELETE FROM `Settings` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, sh.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private SettingsBean getSettingsFromRSet(ResultSet rset) throws SQLException {
        SettingsBean sh = new SettingsBean();
        sh.setId(rset.getInt("id"));
        sh.setKey(rset.getString("option_key"));
        sh.setValue(rset.getString("option_value"));
        return sh;

    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(SettingsRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(SettingsBean sh) throws ApplicationException {
        if (sh == null) {
            throw new ApplicationException("Settings object cannot be null.");
        }
        if (sh.getId() == null || sh.getKey() == null || sh.getValue() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }
}
