package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
public class DepartmentRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `name` ";

    public DepartmentRepository() {
    }

    public DepartmentBean getById(Integer id) throws ApplicationException {
        if (id < 1) {
            throw new ApplicationException("Id [" + id + "] is not a valid Department id");
        }
        String query = "SELECT " + DepartmentRepository.column_names + "  FROM `Department` WHERE `id` = ? ";
        DepartmentBean dept = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                dept = getDeptFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            return dept;
        } finally {
            closeRsc();
        }
        return dept;
    }

    public DepartmentBean getByName(String name) throws ApplicationException {
        String query = "SELECT " + DepartmentRepository.column_names + "  FROM `Department` WHERE `name` = ? ";
        DepartmentBean dept = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, name);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                dept = getDeptFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            return dept;
        } finally {
            closeRsc();
        }
        return dept;
    }

    public ArrayList<DepartmentBean> getAll() throws ApplicationException {
        String query = "SELECT " + DepartmentRepository.column_names + " FROM `Department`";
        ArrayList<DepartmentBean> list = new ArrayList<DepartmentBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getDeptFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String name) throws ApplicationException {
        DepartmentBean dept = new DepartmentBean(id, name);
        return insert(dept);
    }

    public int insert(DepartmentBean dept) throws ApplicationException {
        validate(dept);
        if (dept.getId() != 0) {
            throw new ApplicationException("Id of the Department should be zero for an insert.");
        }
        String query = "INSERT INTO `Department` ( " + DepartmentRepository.column_names + ") VALUES (NULL, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, dept.getName());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Department with the name set to " + dept.getName()
                        + " already exists.", ex);
            }
        }
        dept.setId(getPrimaryKey());
        closeRsc();
        return dept.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Department object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String name) throws ApplicationException {
        DepartmentBean dept = new DepartmentBean(id, name);
        return update(dept);
    }

    public int update(DepartmentBean dept) throws ApplicationException {
        validate(dept);
        if (dept.getId() < 1) {
            throw new ApplicationException("Id of the Department is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Department` SET `name`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, dept.getName());
            pstmt.setInt(2, dept.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Department with the same name already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Department with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        DepartmentBean dept = getById(id);
        if (dept == null) {
            throw new ApplicationException("Department with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Department` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, dept.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private DepartmentBean getDeptFromRSet(ResultSet rset) throws SQLException {
        DepartmentBean dept = new DepartmentBean();
        dept.setId(rset.getInt("id"));
        dept.setName(rset.getString("name"));
        return dept;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     */
    private void validate(DepartmentBean dept) throws ApplicationException {
        if (dept == null) {
            throw new ApplicationException("Department object cannot be null.");
        }
        if (dept.getId() == null || dept.getName() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (dept.getName().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}