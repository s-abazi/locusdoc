package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class UserStateApproverRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `user`, `fileCategory`, `state` ";

    public UserStateApproverRepository() {
    }

    public ArrayList<UserStateApproverBean> getByUser(Integer user) throws ApplicationException {
        if (user == null || user < 1) {
            throw new ApplicationException("Message::getByUser() sender parameter is " + user + ". Must be 1 or larger.");
        }
        String query = "SELECT " + UserStateApproverRepository.column_names + "  FROM `User_State_Approver` WHERE `user` = ? ";
        ArrayList<UserStateApproverBean> list = new ArrayList<UserStateApproverBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, user);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getUserStateApproverFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<UserStateApproverBean> getByFileCategory(Integer fileCategory) throws ApplicationException {
        if (fileCategory == null || fileCategory < 1) {
            throw new ApplicationException("Message::getByFileCategory() sender parameter is " + fileCategory + ". Must be 1 or larger.");
        }
        String query = "SELECT " + UserStateApproverRepository.column_names + " FROM `User_State_Approver` WHERE `fileCategory` = ? ";
        ArrayList<UserStateApproverBean> list = new ArrayList<UserStateApproverBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, fileCategory);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getUserStateApproverFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<UserStateApproverBean> getByState(Integer state) throws ApplicationException {
        if (state == null || state < 1) {
            throw new ApplicationException("Message::getByState() sender parameter is " + state + ". Must be 1 or larger.");
        }
        String query = "SELECT " + UserStateApproverRepository.column_names + " FROM `User_State_Approver` WHERE `state` = ? ";
        ArrayList<UserStateApproverBean> list = new ArrayList<UserStateApproverBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, state);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getUserStateApproverFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }
    
    public ArrayList<UserStateApproverBean> getByStateAndCategory(Integer state, Integer category) throws ApplicationException {
        if (state == null || state < 1) {
            throw new ApplicationException("Message::getByState() sender parameter is " + state + ". Must be 1 or larger.");
        }
        String query = "SELECT " + UserStateApproverRepository.column_names + " FROM `User_State_Approver` WHERE `state` = ? "
                + "AND `fileCategory` = ?";
        ArrayList<UserStateApproverBean> list = new ArrayList<UserStateApproverBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, state);
            pstmt.setInt(2, category);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getUserStateApproverFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<UserStateApproverBean> getAll() throws ApplicationException {
        String query = "SELECT " + UserStateApproverRepository.column_names + " FROM `User_State_Approver`";
        ArrayList<UserStateApproverBean> list = new ArrayList<UserStateApproverBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getUserStateApproverFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public UserStateApproverBean getByPK(Integer user, Integer fileCategory, Integer state) throws ApplicationException {
        String query = "SELECT " + UserStateApproverRepository.column_names + "  FROM `User_State_Approver`"
                + " WHERE `user` = ? AND `fileCategory` = ? AND `state` = ? ";
        UserStateApproverBean uApprover = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, user);
            pstmt.setInt(2, fileCategory);
            pstmt.setInt(3, state);

        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                uApprover = getUserStateApproverFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            return uApprover;
        } finally {
            closeRsc();
        }
        return uApprover;
    }

    public int insert(Integer user, Integer fileCategory, Integer state) throws ApplicationException {
        UserStateApproverBean uApprover = new UserStateApproverBean(user, fileCategory, state);
        return insert(uApprover);
    }

    public int insert(UserStateApproverBean uApprover) throws ApplicationException {
        validate(uApprover);
        String query = "INSERT INTO `User_State_Approver` ( " + UserStateApproverRepository.column_names + ") VALUES "
                + "(?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        int affRows = 0;
        try {
            pstmt.setInt(1, uApprover.getUser());
            pstmt.setInt(2, uApprover.getFileCategory());
            pstmt.setInt(3, uApprover.getState());

            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("Error inserting User_State_Approver object into database. SQL state: " + ex.getSQLState() + "\nError Code: " + ex.getErrorCode());
        }
        closeRsc();
        return affRows;
    }

    public int delete(Integer user, Integer fileCategory, Integer state) throws ApplicationException {
        UserStateApproverBean uApprover = getByPK(user, fileCategory, state);
        if (uApprover == null) {
            throw new ApplicationException("User_FileCategory_State association does not exist.");
        }

        String query = "DELETE FROM `User_State_Approver` WHERE `user` = ? AND `fileCategory` = ? AND `state` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, uApprover.getUser());
            pstmt.setInt(2, uApprover.getFileCategory());
            pstmt.setInt(3, uApprover.getState());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private UserStateApproverBean getUserStateApproverFromRSet(ResultSet rset) throws SQLException {
        UserStateApproverBean uApprover = new UserStateApproverBean();
        uApprover.setUser(rset.getInt("user"));
        uApprover.setFileCategory(rset.getInt("fileCategory"));
        uApprover.setState(rset.getInt("state"));
        return uApprover;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserStateApproverRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(UserStateApproverBean uApprover) throws ApplicationException {
        if (uApprover == null) {
            throw new ApplicationException("UserStateApprover object cannot be null.");
        }
        if (uApprover.getUser() == null || uApprover.getFileCategory() == null || uApprover.getState() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }
}
