package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class WorkflowHistoryRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `file`, `state`, `nextState`, `status` ";

    public WorkflowHistoryRepository() {
    }

    public WorkflowHistoryBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + WorkflowHistoryRepository.column_names + "  FROM `WorkflowHistory` WHERE `id` = ? ";
        WorkflowHistoryBean wh = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                wh = getWorkflowFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            return wh;
        } finally {
            closeRsc();
        }
        return wh;
    }
    
    public WorkflowHistoryBean getByFileAndState(Integer fileId, Integer stateId) throws ApplicationException {
        String query = "SELECT " + WorkflowHistoryRepository.column_names + "  FROM `WorkflowHistory` "
                + "WHERE `file` = ? AND `state` = ? ";
        WorkflowHistoryBean wh = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, fileId);
            pstmt.setInt(2, stateId);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                wh = getWorkflowFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            return wh;
        } finally {
            closeRsc();
        }
        return wh;
    }

    public ArrayList<WorkflowHistoryBean> getByState(Integer state) throws ApplicationException {
        String query = "SELECT " + WorkflowHistoryRepository.column_names + "  FROM `WorkflowHistory` WHERE `state` = ? ";
        ArrayList<WorkflowHistoryBean> list = new ArrayList<WorkflowHistoryBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, state);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<WorkflowHistoryBean> getByStatus(Integer status) throws ApplicationException {

        String query = "SELECT " + WorkflowHistoryRepository.column_names + " FROM `WorkflowHistory` WHERE `status` = ? ";
        ArrayList<WorkflowHistoryBean> list = new ArrayList<WorkflowHistoryBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, status);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<WorkflowHistoryBean> getByFile(Integer file) throws ApplicationException {
        if (file == null || file < 1) {
            throw new ApplicationException("Message::getByFileCategory() sender parameter is " + file + ". Must be 1 or larger.");
        }
        String query = "SELECT " + WorkflowHistoryRepository.column_names + " FROM `WorkflowHistory` WHERE `file` = ? ";
        ArrayList<WorkflowHistoryBean> list = new ArrayList<WorkflowHistoryBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, file);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<WorkflowHistoryBean> getAll() throws ApplicationException {
        String query = "SELECT " + WorkflowHistoryRepository.column_names + " FROM `WorkflowHistory`";
        ArrayList<WorkflowHistoryBean> list = new ArrayList<WorkflowHistoryBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getWorkflowFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, Integer file, Integer state, Integer nextState, Integer status) throws ApplicationException {
        WorkflowHistoryBean wh = new WorkflowHistoryBean(id, file, state, nextState, status);
        return insert(wh);
    }

    /**
     * 
     * @param wh
     * @return Id of inserted object
     * @throws ApplicationException 
     */
    public int insert(WorkflowHistoryBean wh) throws ApplicationException {
        validate(wh);
        if (wh.getId() != 0) {
            throw new ApplicationException("Id of the WorkflowHistory should be zero for an insert.");
        }
        String query = "INSERT INTO `WorkflowHistory` ( " + WorkflowHistoryRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, wh.getFile());
            pstmt.setInt(2, wh.getState());
            pstmt.setInt(3, wh.getNextState());
            pstmt.setInt(4, wh.getStatus());

            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("Error inserting WorkflowHistory object into database. SQL state: " + ex.getSQLState() + "\nError Code: " + ex.getErrorCode());
        }
        wh.setId(getPrimaryKey());
        closeRsc();
        return wh.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, Integer file, Integer state, Integer nextState, Integer status) throws ApplicationException {
        WorkflowHistoryBean wh = new WorkflowHistoryBean(id, file, state, nextState, status);
        return update(wh);
    }

    public int update(WorkflowHistoryBean wh) throws ApplicationException {
        validate(wh);
        if (wh.getId() < 1) {
            throw new ApplicationException("Id of the WorkflowHistory is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `WorkflowHistory` SET `file`=?, `state`=?, `nextState`=?, `status`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, wh.getFile());
            pstmt.setInt(2, wh.getState());
            pstmt.setInt(3, wh.getNextState());
            pstmt.setInt(4, wh.getStatus());
            pstmt.setInt(5, wh.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("WorkflowHistory with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        WorkflowHistoryBean wh = getById(id);
        if (wh == null) {
            throw new ApplicationException("WorkflowHistory with the specified Id does not exist.");
        }
        String query = "DELETE FROM `WorkflowHistory` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, wh.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private WorkflowHistoryBean getWorkflowFromRSet(ResultSet rset) throws SQLException {
        WorkflowHistoryBean wh = new WorkflowHistoryBean();
        wh.setId(rset.getInt("id"));
        wh.setFile(rset.getInt("file"));
        wh.setState(rset.getInt("state"));
        wh.setNextState(rset.getInt("nextState"));
        wh.setStatus(rset.getInt("status"));
        return wh;

    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(WorkflowHistoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(WorkflowHistoryBean wh) throws ApplicationException {
        if (wh == null) {
            throw new ApplicationException("WorkflowHistory object cannot be null.");
        }
        if (wh.getId() == null || wh.getFile() == null || wh.getState() == null || wh.getNextState() == null
                || wh.getStatus() == null) {
            throw new ApplicationException("WorkflowHistory fields cannot be null.");
        }
    }
}
