package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class FileCategoryRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `name`, `editable`";

    public FileCategoryRepository() {
    }

    public FileCategoryBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + FileCategoryRepository.column_names + "  FROM `FileCategory` WHERE `id` = ? ";
        FileCategoryBean file = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                file = getFileCategoryFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            return file;
        } finally {
            closeRsc();
        }
        return file;
    }

    public FileCategoryBean getByName(String name) throws ApplicationException {
        String query = "SELECT " + FileCategoryRepository.column_names + "  FROM `FileCategory` WHERE `name` = ? ";
        FileCategoryBean file = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, name);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                file = getFileCategoryFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            return file;
        } finally {
            closeRsc();
        }
        return file;
    }

    public ArrayList<FileCategoryBean> getAllEditable() throws ApplicationException {
        String query = "SELECT " + FileCategoryRepository.column_names + " FROM `FileCategory` "
                + "WHERE `editable` = 1";
        ArrayList<FileCategoryBean> list = new ArrayList<FileCategoryBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFileCategoryFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<FileCategoryBean> getAll() throws ApplicationException {
        String query = "SELECT " + FileCategoryRepository.column_names + " FROM `FileCategory`";
        ArrayList<FileCategoryBean> list = new ArrayList<FileCategoryBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getFileCategoryFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;

    }

    public int insert(Integer id, String name, Integer editable) throws ApplicationException {
        FileCategoryBean fcb = new FileCategoryBean(id, name, editable);
        return insert(fcb);
    }

    public int insert(FileCategoryBean file) throws ApplicationException {
        validate(file);
        if (file.getId() != 0) {
            throw new ApplicationException("Id of the FileCategory should be zero for an insert.");
        }
        String query = "INSERT INTO `FileCategory` ( " + FileCategoryRepository.column_names + ") VALUES (NULL, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, file.getName());
            pstmt.setInt(2, file.getEditable());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("File Category with the name set to " + file.getName()
                        + " already exists.", ex);
            }
        }
        file.setId(getPrimaryKey());
        closeRsc();
        return file.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have been an error inserting the FileCategory object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }
    
    public int update(Integer id, String name, Integer editable) throws ApplicationException {
        FileCategoryBean fcb = new FileCategoryBean(id, name, editable);
        return update(fcb);
    }

    public int update(FileCategoryBean file) throws ApplicationException {
        validate(file);
        if (file.getId() < 1) {
            throw new ApplicationException("Id of the FileCategory is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `FileCategory` SET `name`=?, `editable`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, file.getName());
            pstmt.setInt(2, file.getEditable());
            pstmt.setInt(3, file.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("FileCategory with the same name already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("FileCategory with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        FileCategoryBean file = getById(id);
        if (file == null) {
            throw new ApplicationException("Form with the specified Id does not exist.");
        }
        String query = "DELETE FROM `FileCategory` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, file.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private FileCategoryBean getFileCategoryFromRSet(ResultSet rset) throws SQLException {
        FileCategoryBean file = new FileCategoryBean();
        file.setId(rset.getInt("id"));
        file.setName(rset.getString("name"));
        file.setEditable(rset.getInt("editable"));
        return file;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileCategoryRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void validate(FileCategoryBean fcb) throws ApplicationException {
        if (fcb == null) {
            throw new ApplicationException("FileCategory object cannot be null.");
        }
        if (fcb.getId() == null || fcb.getName() == null || fcb.getEditable() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (fcb.getName().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
        if (!(fcb.getEditable().equals(1) || fcb.getEditable().equals(0))) {
            throw new ApplicationException("Value of editable is invalid. Editable field can be zero (0) for No and one (1) for Yes.");
        }
    }

}
