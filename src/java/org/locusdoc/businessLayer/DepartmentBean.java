package org.locusdoc.businessLayer;

import java.io.Serializable;

/**
 * Class DepartmentBean is a JavaBean that represents a Department object from Department table in
 * database
 *
 * @author Majlindë Jahiu
 */
public class DepartmentBean implements Serializable {

    private Integer id = 0;
    private String name;

    /**
     * Empty Constructor
     */
    public DepartmentBean() {
    }

    /**
     * Constructor with all fields, except from id because it will be generated automatically from
     * the database.
     *
     * @param name
     */
    public DepartmentBean(String name) {
        this.name = name;
    }

    /**
     * Constructor with all fields
     *
     * @param id
     * @param name
     */
    public DepartmentBean(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            name = "";
        }
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.id);
        sb.append("] - ");
        sb.append("[");
        sb.append(this.name);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof DepartmentBean)) {
            return false;
        }
        DepartmentBean temp = (DepartmentBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
