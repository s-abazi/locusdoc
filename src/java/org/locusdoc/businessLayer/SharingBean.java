package org.locusdoc.businessLayer;

import java.io.Serializable;

public class SharingBean implements Serializable {

    private Integer user = 0;
    private Integer file = 0;
    private Integer view;
    private Integer edit;
    private Integer remove;

    public SharingBean() {
    }

    public SharingBean(Integer user, Integer file, Integer view, Integer edit, Integer remove) {

        this.user = user;
        this.file = file;
        this.view = view;
        this.edit = edit;
        this.remove = remove;

    }

    public SharingBean(Integer view, Integer edit, Integer remove) {
        this.view = view;
        this.edit = edit;
        this.remove = remove;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getFile() {
        return file;
    }

    public void setFile(Integer file) {
        this.file = file;
    }

    public Integer getView() {
        return view;
    }

    public void setView(Integer view) {
        this.view = view;
    }

    public Integer getEdit() {
        return edit;
    }

    public void setEdit(Integer edit) {
        this.edit = edit;
    }

    public Integer getRemove() {
        return remove;
    }

    public void setRemove(Integer remove) {
        this.remove = remove;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.user);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.file);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.view);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.edit);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.remove);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SharingBean)) {
            return false;
        }
        SharingBean temp = (SharingBean) obj;
        if (!(this.getUser().equals(temp.getUser()))) {
            return false;
        }
        if (!(this.getFile().equals(temp.getFile()))) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 41 * hash + (this.file != null ? this.file.hashCode() : 0);
        return hash;
    }

}
