package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
public class RoleRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `name`, `description` ";

    public RoleBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + RoleRepository.column_names + "  FROM `Role` WHERE `id` = ? ";
        RoleBean role = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                role = getRoleFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            return role;
        } finally {
            closeRsc();
        }
        return role;
    }

    public RoleBean getByName(String name) throws ApplicationException {
        String query = "SELECT " + RoleRepository.column_names + "  FROM `Role` WHERE `name` = ? ";
        RoleBean role = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, name);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                role = getRoleFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            return role;
        } finally {
            closeRsc();
        }
        return role;
    }

    private RoleBean getRoleFromRSet(ResultSet rset) throws SQLException {
        RoleBean role = new RoleBean();
        role.setId(rset.getInt("id"));
        role.setName(rset.getString("name"));
        role.setDescription(rset.getString("description"));
        return role;
    }

    public ArrayList<RoleBean> getAll() throws ApplicationException {
        String query = "SELECT " + RoleRepository.column_names + " FROM `Role`";
        ArrayList<RoleBean> list = new ArrayList<RoleBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getRoleFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String name, String description) throws ApplicationException {
        RoleBean role = new RoleBean(id, name, description);
        return insert(role);
    }

    public int insert(RoleBean role) throws ApplicationException {
        validate(role);
        if (role.getId() != 0) {
            throw new ApplicationException("Id of the Role should be zero for an insert.");
        }
        String query = "INSERT INTO `Role` ( " + RoleRepository.column_names + ") VALUES (NULL, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, role.getName());
            pstmt.setString(2, role.getDescription());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Role with the same name already exists.", ex);
            }
        }
        role.setId(getPrimaryKey());
        closeRsc();
        return role.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Form object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String name, String description) throws ApplicationException {
        RoleBean role = new RoleBean(id, name, description);
        return update(role);
    }

    public int update(RoleBean role) throws ApplicationException {
        validate(role);
        if (role.getId() < 1) {
            throw new ApplicationException("Id of the Role is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Role` SET `name`=?, `description`=?  WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, role.getName());
            pstmt.setString(2, role.getDescription());
            pstmt.setInt(3, role.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Role with the same name already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Role with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        RoleBean role = getById(id);
        if (role == null) {
            throw new ApplicationException("Role with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Role` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, role.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Method to check if any of the mandatory attributes are <code>null</code>. Do <b>not</b>
     * overwrite this method
     *
     * @param role
     * @return <code>TRUE</code> - if all the mandatory fields are set and <br><code>FALSE</code> -
     * If one or more fields are <code>null</code>
     */
    private void validate(RoleBean role) throws ApplicationException {
        if (role == null) {
            throw new ApplicationException("Role object cannot be null.");
        }
        if (role.getId() == null || role.getName() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (role.getName().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
