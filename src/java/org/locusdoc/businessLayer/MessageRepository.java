package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class MessageRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `subject`, `content`, `timestamp`, "
            + "`read`, `sender`, `receiver` ";

    public MessageRepository() {
    }

    public MessageBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + MessageRepository.column_names + "  FROM `Message` WHERE `id` = ? ";
        MessageBean msg = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                msg = getMessageFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            return msg;
        } finally {
            closeRsc();
        }
        return msg;
    }

    public ArrayList<MessageBean> getByReceiver(Integer reciever) throws ApplicationException {
        String query = "SELECT " + MessageRepository.column_names + "  FROM `Message` WHERE `receiver` = ? "
                + "ORDER BY `timestamp` DESC";
        ArrayList<MessageBean> list = new ArrayList<MessageBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, reciever);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getMessageFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<MessageBean> getByReceiver(Integer receiver, Integer read)
            throws ApplicationException {
        if (receiver == null || receiver < 1) {
            throw new ApplicationException("Message::getByReceiver() receiver parameter is " + receiver
                    + ". Must be 1 or larger.");
        }
        if (read == null || !(read == 1 || read == 0)) {
            throw new ApplicationException("Message::getByReceiver() read parameter is " + read
                    + ". Must be 1 or 0.");
        }
        String query = "SELECT " + MessageRepository.column_names + " FROM `Message` WHERE `receiver` = ?"
                + " AND `read` = ? ORDER BY `timestamp` DESC ";
        ArrayList<MessageBean> list = new ArrayList<MessageBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, receiver);
            pstmt.setInt(2, read);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getMessageFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    /**
     * Get all messages for this Sender. Returned messages can be marked as read or not read.
     *
     * @param sender
     * @return
     * @throws ApplicationException
     */
    public ArrayList<MessageBean> getBySender(Integer sender) throws ApplicationException {
        if (sender == null || sender < 1) {
            throw new ApplicationException("Message::getBySender() sender parameter is " + sender + ". Must be 1 or larger.");
        }
        String query = "SELECT " + MessageRepository.column_names + " FROM `Message` WHERE `sender` = ? "
                + "ORDER BY `timestamp` DESC";
        ArrayList<MessageBean> list = new ArrayList<MessageBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, sender);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getMessageFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<MessageBean> getBySender(Integer sender, Integer read)
            throws ApplicationException {
        if (sender == null || sender < 1) {
            throw new ApplicationException("Message::getBySender() sender parameter is " + sender
                    + ". Must be 1 or larger.");
        }
        if (read == null || !(read == 1 || read == 0)) {
            throw new ApplicationException("Message::getBySender() read parameter is " + read
                    + ". Must be 1 or 0.");
        }
        String query = "SELECT " + MessageRepository.column_names + " FROM `Message` WHERE `sender` = ? "
                + "AND `read` = ? ORDER BY `timestamp` DESC";
        ArrayList<MessageBean> list = new ArrayList<MessageBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, sender);
            pstmt.setInt(2, read);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getMessageFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<MessageBean> getAll() throws ApplicationException {
        String query = "SELECT " + MessageRepository.column_names + " FROM `Message` "
                + "ORDER BY `timestamp` DESC";
        ArrayList<MessageBean> list = new ArrayList<MessageBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getMessageFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String subject, String content, Timestamp timestamp, Integer read, Integer sender, Integer receiver) throws ApplicationException {
        MessageBean msg = new MessageBean(id, subject, content, timestamp, read, sender, receiver);
        return insert(msg);
    }

    public int insert(MessageBean msg) throws ApplicationException {
        validate(msg);
        if (msg.getId() != 0) {
            throw new ApplicationException("Id of the Message should be zero for an insert.");
        }
        String query = "INSERT INTO `Message` ( " + MessageRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, msg.getSubject());
            pstmt.setString(2, msg.getContent());
            pstmt.setTimestamp(3, msg.getTimestamp());
            pstmt.setInt(4, msg.getRead());
            pstmt.setInt(5, msg.getSender());
            pstmt.setInt(6, msg.getReceiver());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        }
        msg.setId(getPrimaryKey());
        closeRsc();
        return msg.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String subject, String content, Timestamp timestamp, Integer read, Integer sender,
            Integer receiver) throws ApplicationException {
        MessageBean msg = new MessageBean(id, subject, content, timestamp, read, sender, receiver);
        return update(msg);
    }

    public int update(MessageBean msg) throws ApplicationException {
        validate(msg);
        if (msg.getId() < 1) {
            throw new ApplicationException("Id of the Message is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `Message` SET `subject`=?, `content`=?, `timestamp`=?, `read`=?,"
                + " `sender`=?, `receiver`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, msg.getSubject());
            pstmt.setString(2, msg.getContent());
            pstmt.setTimestamp(3, msg.getTimestamp());
            pstmt.setInt(4, msg.getRead());
            pstmt.setInt(5, msg.getSender());
            pstmt.setInt(6, msg.getReceiver());
            pstmt.setInt(7, msg.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Message with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        MessageBean msg = getById(id);
        if (msg == null) {
            throw new ApplicationException("Message with the specified Id does not exist.");
        }
        String query = "DELETE FROM `Message` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, msg.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }
    
    public int deleteAll(Integer receiver) throws ApplicationException {
        String query = "DELETE FROM `Message` WHERE `receiver` = ?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, receiver);
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private MessageBean getMessageFromRSet(ResultSet rset) throws SQLException {
        MessageBean msg = new MessageBean();
        msg.setId(rset.getInt("id"));
        msg.setSubject(rset.getString("subject"));
        msg.setContent(rset.getString("content"));
        msg.setTimestamp(rset.getTimestamp("timestamp"));
        msg.setRead(rset.getInt("read"));
        msg.setSender(rset.getInt("sender"));
        msg.setReceiver(rset.getInt("receiver"));
        return msg;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(MessageRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(MessageBean msg) throws ApplicationException {
        if (msg == null) {
            throw new ApplicationException("Messages object cannot be null.");
        }
        if (msg.getId() == null || msg.getSubject() == null || msg.getContent() == null || msg.getRead() == null
                || msg.getSender() == null || msg.getReceiver() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (msg.getSubject().trim().isEmpty() || msg.getContent().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
