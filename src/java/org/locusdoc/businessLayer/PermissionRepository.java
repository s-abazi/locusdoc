/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

/**
 *
 *
 */
public class PermissionRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `form`, `role`, `action` ";

    public PermissionRepository() {
    }

    public PermissionBean getExatcPermission(Integer form, Integer role, Integer action) throws ApplicationException {
        String query = "SELECT " + PermissionRepository.column_names + "  FROM `Permission` WHERE "
                + "`form` = ? AND `role` = ? AND `action` = ?";
        PermissionBean perm = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, form);
            pstmt.setInt(2, role);
            pstmt.setInt(3, action);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                perm = getPermissionFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            return perm;
        } finally {
            closeRsc();
        }
        return perm;
    }

    public ArrayList<PermissionBean> getAll() throws ApplicationException {
        String query = "SELECT " + PermissionRepository.column_names + " FROM `Permission`";
        ArrayList<PermissionBean> list = new ArrayList<PermissionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getPermissionFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<PermissionBean> getByForm(Integer form) throws ApplicationException {
        String query = "SELECT " + PermissionRepository.column_names + " FROM `Permission` WHERE"
                + " `form` = ?";
        ArrayList<PermissionBean> list = new ArrayList<PermissionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, form);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getPermissionFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<PermissionBean> getByRole(Integer role) throws ApplicationException {
        String query = "SELECT " + PermissionRepository.column_names + " FROM `Permission` WHERE"
                + " `role` = ?";
        ArrayList<PermissionBean> list = new ArrayList<PermissionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, role);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getPermissionFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public ArrayList<PermissionBean> getByAction(Integer action) throws ApplicationException {
        String query = "SELECT " + PermissionRepository.column_names + " FROM `Permission` WHERE"
                + " `action` = ?";
        ArrayList<PermissionBean> list = new ArrayList<PermissionBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, action);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getPermissionFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer form, Integer role, Integer action) throws ApplicationException {
        PermissionBean perm = new PermissionBean(form, role, action);
        return insert(perm);
    }

    public int insert(PermissionBean perm) throws ApplicationException {
        validate(perm);
        String query = "INSERT INTO `Permission` ( " + PermissionRepository.column_names + ") "
                + "VALUES (?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(DepartmentRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        int affRows = 0;
        try {
            pstmt.setInt(1, perm.getForm());
            pstmt.setInt(2, perm.getRole());
            pstmt.setInt(3, perm.getAction());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("Permission with the same form, role and action"
                        + " already exists.", ex);
            }
        }
        closeRsc();
        return affRows;
    }

    public int delete(Integer form, Integer role, Integer action) throws ApplicationException {
        PermissionBean perm = getExatcPermission(form, role, action);
        if (perm == null) {
            throw new ApplicationException("Permission with the specified attributes does not exist.");
        }
        String query = "DELETE FROM `Permission` WHERE `form` = ? AND `role` =? AND `action` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(RoleRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, perm.getForm());
            pstmt.setInt(2, perm.getRole());
            pstmt.setInt(3, perm.getAction());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private void validate(PermissionBean perm) throws ApplicationException {
        if (perm == null) {
            throw new ApplicationException("Permission object cannot be null.");
        }
        if (perm.getForm() == null || perm.getAction() == null || perm.getRole() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }

    private static PermissionBean getPermissionFromRSet(ResultSet rset) throws SQLException {
        PermissionBean perm = new PermissionBean();
        perm.setForm(rset.getInt("form"));
        perm.setRole(rset.getInt("role"));
        perm.setAction(rset.getInt("action"));
        return perm;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(PermissionRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
