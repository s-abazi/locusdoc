package org.locusdoc.businessLayer;

import java.io.Serializable;

public class StateBean implements Serializable {

    private Integer id = 0;
    private String description;

    public StateBean() {
    }

    public StateBean(String description) {
        this.description = description;
    }

    public StateBean(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        sb.append(this.id);
        sb.append("] ");
        sb.append("[");
        sb.append(this.description);
        sb.append("]} ");

        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof StateBean)) {
            return false;
        }
        StateBean temp = (StateBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
