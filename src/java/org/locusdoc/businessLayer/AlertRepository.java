package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class AlertRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `title`, `content`, `user`";

    public AlertRepository() {

    }

    public ArrayList<AlertBean> getByUser(Integer user) throws ApplicationException {
        String query = "SELECT " + AlertRepository.column_names + "  FROM `Alert` WHERE `user` = ? ";
        ArrayList<AlertBean> list = new ArrayList<AlertBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, user);
            rset = pstmt.executeQuery();
            while (rset.next()) {
                list.add(getAlertFromRSet(rset));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            return list;
        } finally {
            closeRsc();
        }
        return list;
    }

    public AlertBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + AlertRepository.column_names + "  FROM `Alert` WHERE `id` = ?";
        AlertBean ab = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                ab = getAlertFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            return ab;
        } finally {
            closeRsc();
        }
        return ab;
    }

    public ArrayList<AlertBean> getAll() throws ApplicationException {
        String query = "SELECT " + AlertRepository.column_names + " FROM `Alert`";
        ArrayList<AlertBean> list = new ArrayList<AlertBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getAlertFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String title, String content, Integer user) throws ApplicationException {
        AlertBean ab = new AlertBean(id, title, content, user);
        return insert(ab);
    }

    public int insert(AlertBean ab) throws ApplicationException {
        validate(ab);

        String query = "INSERT INTO `Alert` ( " + AlertRepository.column_names + ") VALUES "
                + "(NULL, ?, ?, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        int affRows = 0;
        try {
            pstmt.setString(1, ab.getTitle());
            pstmt.setString(2, ab.getContent());
            pstmt.setInt(3, ab.getUser());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error inserting Alert object.", ex);
        }
        ab.setId(getPrimaryKey());
        closeRsc();
        return ab.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the Alert object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String title, String content, Integer user) throws ApplicationException {
        AlertBean ab = new AlertBean(id, title, content, user);
        return update(ab);
    }

    public int update(AlertBean ab) throws ApplicationException {
        validate(ab);
        String query = "UPDATE `Alert` SET `title` = ?, `content` = ?, `user` = ?"
                + " WHERE `id`= ?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, ab.getTitle());
            pstmt.setString(2, ab.getContent());
            pstmt.setInt(3, ab.getUser());
            pstmt.setInt(4, ab.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("Alert with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        AlertBean ab = getById(id);
        if (ab == null) {
            throw new ApplicationException("This id does not exist.");
        }
        String query = "DELETE FROM `Alert` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, ab.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    public int deleteAll(Integer user) throws ApplicationException {
        String query = "DELETE FROM `Alert` WHERE `user` = ? ";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setInt(1, user);
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private AlertBean getAlertFromRSet(ResultSet rset) throws SQLException {
        AlertBean ab = new AlertBean();
        ab.setId(rset.getInt("id"));
        ab.setTitle(rset.getString("title"));
        ab.setContent(rset.getString("content"));
        ab.setUser(rset.getInt("user"));
        return ab;

    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AlertRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Checks if all fields are set.
     *
     * @return false If any ore more fields are null or any String is empty, true if all fields are
     * not equal to null and all Strings are not empty.
     *
     *
     */
    private void validate(AlertBean ab) throws ApplicationException {
        if (ab == null) {
            throw new ApplicationException("Alert object cannot be null.");
        }
        if (ab.getId() == null || ab.getTitle() == null || ab.getContent() == null || ab.getUser() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
    }
}
