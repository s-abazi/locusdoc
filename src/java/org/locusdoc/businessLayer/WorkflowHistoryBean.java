package org.locusdoc.businessLayer;

import java.io.Serializable;

public class WorkflowHistoryBean implements Serializable {

    private Integer id = 0;
    private Integer file;
    private Integer state;
    private Integer nextState;
    private Integer status;

    /**
     * Empty Constructor
     */
    public WorkflowHistoryBean() {
    }

    /**
     * Constructor with all fields, except id because it will be auto generated from the database on
     * insert.
     *
     * @param file
     * @param state
     * @param nextState
     * @param status
     *
     */
    public WorkflowHistoryBean(Integer file, Integer state, Integer nextState, Integer status) {
        this.file = file;
        this.state = state;
        this.nextState = nextState;
        this.status = status;

    }

    /**
     * Constructor with all fields, including id.
     *
     * @param id
     * @param file
     * @param state
     * @param nextState
     * @param status
     *
     */
    public WorkflowHistoryBean(Integer id, Integer file, Integer state,
            Integer nextState, Integer status) {
        this.id = id;
        this.file = file;
        this.state = state;
        this.nextState = nextState;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFile() {
        return file;
    }

    public void setFile(Integer file) {
        this.file = file;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getNextState() {
        return nextState;
    }

    public void setNextState(Integer nextState) {
        this.nextState = nextState;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WorkflowHistoryBean{" + "id=" + id + ", file=" + file + ", state=" + state + ", nextState=" + nextState + ", status=" + status + '}';
    }

    

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WorkflowHistoryBean)) {
            return false;
        }
        WorkflowHistoryBean temp = (WorkflowHistoryBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
