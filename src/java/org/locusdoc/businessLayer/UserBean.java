package org.locusdoc.businessLayer;

import java.io.Serializable;
import org.locusdoc.utilities.ApplicationException;

/**
 * Class UserBean is a JavaBean that represents a User object from the User table in database
 *
 * @author Shkumbin Abazi <shk.abazi@gmail.com>
 */
public class UserBean implements Serializable {

    private Integer id = 0;
    private String username;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private Integer active;
    private Integer employee;
    private Integer role;
    private Integer changedPass;

    // Static Constants
    public static final int ACTIVE = 1;
    public static final int INACTIVE = 0;

    /**
     * Empty constructor
     */
    public UserBean() {
    }

    /**
     * Constructor with all fields, not with id because it will be auto generated from the database
     * on insert.
     *
     * @param username
     * @param password
     * @param firstName
     * @param middleName
     * @param lastName
     * @param active
     * @param employee
     * @param role
     * @param changedPass
     */
    public UserBean(String username, String password, String firstName, String middleName,
            String lastName, Integer active, Integer employee, Integer role, Integer changedPass) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.active = active;
        this.employee = employee;
        this.role = role;
        this.changedPass = changedPass;
    }

    /**
     * Constructor with all fields, including id
     *
     * @param id
     * @param username
     * @param password
     * @param firstName
     * @param middleName
     * @param lastName
     * @param active
     * @param role
     * @param employee
     * @param changedPass
     */
    public UserBean(Integer id, String username, String password, String firstName,
            String middleName, String lastName, Integer active, Integer employee, Integer role,
            Integer changedPass) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.active = active;
        this.employee = employee;
        this.role = role;
        this.changedPass = changedPass;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if (username == null) {
            this.username = "";
        }
        this.username = username;

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password == null) {
            password = "";
        }
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null) {
            firstName = "";
        }
        this.firstName = firstName;
    }

    /**
     * @return The middle name of this User
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        if (middleName == null) {
            middleName = "";
        }
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null) {
            lastName = "";
        }
        this.lastName = lastName;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the employee
     */
    public Integer getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(Integer employee) {
        this.employee = employee;
    }

    /**
     * @return the role_id
     */
    public Integer getRole() {
        return role;
    }

    /**
     * @param role the role_id to set
     */
    public void setRole(Integer role) {
        this.role = role;
    }

    /**
     * @return the changedPass
     */
    public Integer getChangedPass() {
        return changedPass;
    }

    /**
     * @param changedPass the changedPass to set
     */
    public void setChangedPass(Integer changedPass) {
        this.changedPass = changedPass;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" \n [");
        sb.append(this.id);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.username);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.password);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.firstName);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.middleName);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.lastName);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.active);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.employee);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.role);
        sb.append("] \n ");
        sb.append("[");
        sb.append(this.changedPass);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof UserBean)) {
            return false;
        }
        UserBean temp = (UserBean) obj;
        return this.getId().equals(temp.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
