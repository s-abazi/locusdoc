package org.locusdoc.businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.locusdoc.databaseLayer.DbSource;
import org.locusdoc.utilities.ApplicationException;

public class StateRepository {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;
    private static ResultSet rset;
    private final static String column_names = " `id`, `description` ";

    public StateRepository() {
    }

    public StateBean getById(Integer id) throws ApplicationException {
        String query = "SELECT " + StateRepository.column_names + "  FROM `State` WHERE `id` = ? ";
        StateBean state = null;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = pstmt.executeQuery();
            while (rset.next()) {
                state = getStateFromRSet(rset);
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            return state;
        } finally {
            closeRsc();
        }
        return state;
    }

    public ArrayList<StateBean> getAll() throws ApplicationException {
        String query = "SELECT " + StateRepository.column_names + " FROM `State` ";
        ArrayList<StateBean> list = new ArrayList<StateBean>();
        try {
            conn = DbSource.setupDataSource().getConnection();
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            rset = stmt.executeQuery(query);
            while (rset.next()) {
                list.add(getStateFromRSet(rset));
            }
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeRsc();
        }
        return list;
    }

    public int insert(Integer id, String description) throws ApplicationException {
        StateBean state = new StateBean(id, description);
        return insert(state);
    }

    public int insert(StateBean state) throws ApplicationException {
        validate(state);
        if (state.getId() != 0) {
            throw new ApplicationException("Id of the State should be zero for an insert.");
        }
        String query = "INSERT INTO `State` ( " + StateRepository.column_names + ") VALUES (NULL, ?)";
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, state.getDescription());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
        }
        state.setId(getPrimaryKey());
        closeRsc();
        return state.getId();
    }

    private Integer getPrimaryKey() throws ApplicationException {
        try {
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                return (rs.getInt(1));
            }
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There might have bene an error inserting the State object. "
                    + "Cannot return the generated primary key.", ex);
        }
        return 0;
    }

    public int update(Integer id, String description) throws ApplicationException {
        StateBean state = new StateBean(id, description);
        return update(state);
    }

    public int update(StateBean state) throws ApplicationException {
        validate(state);
        if (state.getId() < 1) {
            throw new ApplicationException("Id of the State is invalid. It cant be zero or negative.");
        }
        String query = "UPDATE `State` SET `description`=? WHERE `id`=?";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {
            pstmt.setString(1, state.getDescription());
            pstmt.setInt(2, state.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            String sqlError = ex.getSQLState();
            if (sqlError.contains("23000")) {
                throw new ApplicationException("State with the same title, path or nameId already exists.", ex);
            }
        } finally {
            closeRsc();
        }
        if (affRows == 0) {
            throw new ApplicationException("State with specified id does not exist.");
        }
        return affRows;
    }

    public int delete(Integer id) throws ApplicationException {
        StateBean state = getById(id);
        if (state == null) {
            throw new ApplicationException("State with the specified Id does not exist.");
        }
        String query = "DELETE FROM `State` WHERE `id` = ? LIMIT 1";
        int affRows = 0;
        try {
            conn = DbSource.setupDataSource().getConnection();
            pstmt = conn.prepareStatement(query);
        } catch (SQLException ex) {
            closeRsc();
            Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("Error connecting to database. Please, try again.", ex);
        }
        try {

            pstmt.setInt(1, state.getId());
            affRows = pstmt.executeUpdate();
        } catch (SQLException ex) {
            closeRsc();
            throw new ApplicationException("There was an error executing the query.", ex);
        } finally {
            closeRsc();
        }
        return affRows;
    }

    private StateBean getStateFromRSet(ResultSet rset) throws SQLException {
        StateBean state = new StateBean();
        state.setId(rset.getInt("id"));
        state.setDescription(rset.getString("description"));
        return state;
    }

    private void closeRsc() {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(StateRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void validate(StateBean state) throws ApplicationException {
        if (state == null) {
            throw new ApplicationException("State object cannot be null.");
        }
        if (state.getId() == null || state.getDescription() == null) {
            throw new ApplicationException("Mandatory fields cannot be null.");
        }
        if (state.getDescription().trim().isEmpty()) {
            throw new ApplicationException("Mandatory fields cannot be empty.");
        }
    }
}
