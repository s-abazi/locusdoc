package org.locusdoc.utilities;

/**
 *
 * Class for Employee specific Exceptions
 */
public class ApplicationException extends Exception {

    /**
     * Creates a new instance of <code>ApplicationException</code> without detail
     * message.
     */
    public ApplicationException() {
    }

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ApplicationException(String msg) {
        super(msg);
    }
    
    public ApplicationException(String msg, Throwable rootCause) {
        super(msg, rootCause);
    }

    public ApplicationException(Throwable rootCause) {
        super(rootCause);
    }
}
