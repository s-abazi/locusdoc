/*
 * Class used to encrypt Strings with the SHA-256 encryption algorithm
 */
package org.locusdoc.utilities;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 *
 * @author Shkumbin Abazi <shk.abazi@gmail.com>
 */
public class LocusDocUtil {

    public static MessageDigest md;

    /**
     * This class cannot be instantiated
     */
    private LocusDocUtil() {
    }

    public static String encrypt(String passTxt) {
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException algoExc) {
            algoExc.printStackTrace();
            Logger.getLogger(LocusDocUtil.class.getName()).log(Level.SEVERE, null, algoExc);
            return null;
        }
        byte[] passBytes;
        try {
            passBytes = passTxt.getBytes("UTF-8");
        } catch (UnsupportedEncodingException wrongEncEx) {
            Logger.getLogger(LocusDocUtil.class.getName()).log(Level.SEVERE, null, wrongEncEx);
            return null;
        }
        
        md.reset();
        byte[] digested = md.digest(passBytes);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digested.length; i++) {
            String hex = Integer.toHexString(0xff & digested[i]);
            if(hex.length() == 1) sb.append(0);
            sb.append(hex);
        }
        return sb.toString();
    }
    
    public static String generate() {
        return encrypt("112233");
    }
    
    
    public static String cleanString(String unsafe) {
        String clean = Jsoup.clean(unsafe, Whitelist.basic());
        return clean;
    }
}
