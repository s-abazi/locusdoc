/*
 * Database access class
 */
package org.locusdoc.databaseLayer;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author Shkumbin Abazi <shk.abazi@gmail.com>
 */
public class DbSource {
    private static DataSource ds;
    public static DataSource setupDataSource() {
        Context envContext = null;
        Context initContext = null;
        if (ds == null){
            try {
                envContext = new InitialContext();
                initContext = (Context) envContext.lookup("java:/comp/env");
                ds = (DataSource) initContext.lookup("jdbc/dms");
            } catch (NamingException ex) {
                Logger.getLogger(DbSource.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ds;
    }

    public static void printDataSourceStats(DataSource ds) {
        BasicDataSource bds = (BasicDataSource) ds;
        System.out.println("NumActive: " + bds.getNumActive());
        System.out.println("NumIdle: " + bds.getNumIdle());
    }

    public static void shutdownDataSource(DataSource ds) throws SQLException {
        BasicDataSource bds = (BasicDataSource) ds;
        bds.close();
    }
}
